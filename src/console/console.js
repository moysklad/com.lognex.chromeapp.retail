function error (msg) { console.log(msg); }

const testreceipt = {
		"paidSum": 130000,
		"type": "cash",
		//"discount": 500,
		"cashier": "cashier for test",
		"positions": [
			{
				"name": "Трусы",
				"price": 10050,
				"quantity": 2.0,
				"vat": 18.0
			},
			{
				"name": "Носки",
				"price": 5010,
				"quantity": 0.5,
				//"discount": 500,
				"vat": 10.0
			},
			{
				"name": "Крестик",
				"price": 100020,
				"quantity": 1.0,
				"vat": 0
			}
		],
		// "egais1": {
		// 	"head": "OOO \"Логнекс\"",
		// 	"inn": "7736570901",
		// 	"kpp": "773601001",
		// 	"url": "http://check.egais.ru/?id=86a22086-edb1-4e6b-9b04-e719cf5f817a&dt=0211161528&cn=030000227789",
		// 	"sign": "6753391F2CECF4CD26D05AA73BBF63B3EDC44DCB3A55B83DE04590B251D9040B5346FF5A15FA969466B51407B825AA9686A082E6917EEEF2973D1EEA8F287433",
		// 	"doc": 1,
		// 	"shift": 1
		// }
	};

	const testcorrreceipt = {
			"op": 1,
			"type": "1",
			"cashier": "cashier for test",
			"cashsum":120*100,
			"nocashsum": 50*100
		};

const logtext = document.getElementById("logtext");

coninput.focus();
document.getElementById("log").onclick = function() { coninput.focus(); };

function cmd(l) {
	return l.substring(0, l.indexOf(' ')+1 ? l.indexOf(' '): l.length);
}

function prm(l) {
	return l.indexOf(' ')+1 ? l.substr(l.indexOf(' ')+1) : "";
}

function log(msg, input) {
	if (logtext.innerText)
		logtext.innerText += "\n";
	const line = "..."+("" + new Date().getTime()).substr(-5,5)+ " "+(input ? "# " : "> ")+msg;
	logtext.innerText += line;
	console.log(line);
}

var vendor = "shtrikh", port = "COM5"; kkm.config.default.bitrate=115200;

history(coninput, function (e) {
	log(coninput.value, true);
	if (commands[cmd(coninput.value)])
		try {
			commands[cmd(coninput.value)](coninput.value);
		} catch (e) { log("[ERROR]::"+e.message); }
	coninput.value = "";
});


function conf() {
	var c = kkm.config.default()[vendor]();
	c.id = port;
	c.comprefs.bitrate = 115200;
	return c;
}

function exec() {
    return function (logging, chain) {
        kkm.serial.request(logging, conf(), function (conn, send1, receive1, close, flush) {
            kkm[conf().vendor].command(conf(), chain, logging, conn, send1, receive1, close, flush);
        }, function () { log(conf().id+" not available"); });
    }
}

const logging = function(errtable) {
    return {
        log: log,
        err: error,
        assert: kkm.util.assertmaker(errtable, error)
    };
};

var commands = {};

commands.help = function () {
	commands.clear();
	log("Тестовая консоль для работы с ФР через chrome.serial");
	log("----------------------------------------------------");
	log("Доступные команды:");
	log("");
	log("help - вывод этого сообщения");
	log("clear - очистка окна");
	log("lsp - перечисление доступных портов");
	log("sv <имя вендора, например mock> - выбор вендора");
	log("gv - показать выбранного вендора");
	log("sp <путь к порту> - выбор COM-порта");
	log("gp - показать выбранный порт");
	log("beep - звуковой сигнал");
	log("demo - тестовый прогон печати");
	log("info - запрос информации об устройстве");
	log("type - запрос типа устройства");
	log("check - запрос состояния");
	log("print - печать тестового чека");
	log("corrprint - печать тестового чека коррекции");
	log("z - печать отчета с закрытием смены");
	log("exit - закрыть приложение");
	log("история команд доступна по стрелкам ↑ и ↓");
	log("----------------------------------------------------");
}

commands.clear = function () {
	logtext.innerText = "";
};

commands.lsp = function () {
	log("enumerating serial ports");
	kkm.serial.enum(
		logging,
		null,
		function (portsnum) {  log("found ports: "+portsnum, false); },
		function  (fill) {
			var d = fill(function() { return {}; });
			log("path "+d.id+", name "+d.name+", vendor "+d.vendor+", product "+d.product);
		}
	);
};

commands.sp = function (l) {
	port = prm(l);
}

commands.gp = function () {
	log(port, false);
}

commands.sv = function(l) {
	vendor = prm(l);
}

commands.gv = function() {
	log(vendor, false);
}

function chk() {
	if (!vendor) throw Error("vendor not set");
	if (!port) throw Error("port not set");
}

commands.beep = function () {
	chk();
	kkm[vendor].ext(logging, conf(), exec(), function (b) {
		b();
	});
};

commands.demo = function () {
	chk();
	kkm[vendor].ext(logging, conf(), exec(), function (b, d) {
		d();
	});
};

commands.info = function () {
	const p = {
		mock: function(data) {
			log("тестовая информация получена");
		},
		shtrikh: function (data) {
			log("[ШТРИХ] info: " + JSON.stringify(data));
		}
	};

	chk();
	kkm[vendor].ext(logging, conf(), exec(), function(b, d, i) {
		i(p[vendor]);
	});
};


commands.type = function () {
	var proc = (vendor, data) => {
		return (data) => log("[" + vendor + "] type data received: " + kkm.util.bytes2str(data));
	};

	chk();
	kkm[vendor].ext(logging, conf(), exec(), function(b, d, i, t) {
		t(proc(vendor));
	});
};

commands.check = () => {
	chk();
	kkm[vendor].ext(logging, conf(), exec(), (b, d, i, t, check) => {
		check({}, () => {});
	})
};

commands.print = function () {
	chk();
	kkm[vendor].ext(logging, conf(), exec(), function(b, d, i, t, c, p) {
		testreceipt.return = false;
		p(testreceipt);
	});
}

commands.corrprint = function () {
	chk();
	kkm[vendor].ext(logging, conf(), exec(), function(b, d, i, t, c, p,z,x,ct, cn, cut, ofd, cp) {
		cp(testcorrreceipt,function(){log("corr print finish");});
	});
}

commands.return = function () {
	chk();
	kkm[vendor].ext(logging, conf(), exec(), function(b, d, i, t, c, p) {
		testreceipt.return = true;
		r(testreceipt);
	});
}

commands.x = function () {
	chk();
	kkm[vendor].ext(logging, conf(), exec(), function(b, d, i, t, c, p, z, x) {
		x();
	});
}

commands.z = function () {
	chk();
	kkm[vendor].ext(logging, conf(), exec(), function(b, d, i, t, c, p, z) {
		z();
	});
}

commands.exit = function () {
	window.close();
};

commands.findv = function () {
    kkm.find.forvendor(
        vendor,
        logging,
        function(msg) { log("findlog -> "+msg); },
        function(fr) { log("found "+fr.model); },
        function (id) { log("not found on "+id); },
        function () { log("complete"); }
    );
}

commands.help();

function history(input, handle) {
	var cmdlog = [];
	var cursor = 0;

	function up() {
		if (0 == cursor) return;
		cursor--;
		input.value = cmdlog[cursor];
	}

	function down() {
		if (cursor == cmdlog.length) return;
		cursor++;
		input.value= cmdlog.length == cursor ? "" : cmdlog[cursor];
	}

	input.onkeyup = function(e) {
		switch (e.keyCode) {
			case 38: //up arrow
				up();
				break;
			case 40: //down arrow
				down();
				break;
			case 13: // enter
				cmdlog.push(input.value);
				cursor = cmdlog.length;
				handle(e);
			default:
				return;
		}
	};
}
