kkm.mock.api = function (util, config, executor, proc) {
	function app(chain) {
		executor(util, chain);
	}

	util(function (log, err, crc, bytecheck, empty, assert, map, ofd) {
		function beep() {
			app(
				function (send, receive, close) {
					log("beeeeep!");
					send([1,2,3,4,5], function() {
						close();
					});
				}
			);
		}

		function demo(onfinish) {
			log("тестовый прогон печати");
			app(
				function (send, receive, close) {
					receive(assert("ошибка при получении отчета о тестовой печати", close, function (data) {
						log("тестовая печать завершена");
					}));

					log("запускаем тестовую печать");
					send([1,2,3,4,5], function () {
						log("команда тестовой печати отправлена")
					});
				}
			);
		}

		function info(proc) {
			log("получение информации об устройстве"); 
			app(
				function (send, receive, close) {
					receive(function (data) {
						log("информация об устройстве получена"); 
						close(function () { proc(data); });
					});
					log("запрашиваем информацию об устройстве"); 
					send([1,2,3,4,5], function() {
						log("команда запроса информации отправлена"); 
					});
				}
			);
		}

		function type(proc) {
			log("получение информации о типе устройства"); 
			app(
				function (send, receive, close) {
					receive(function (data) {
						log("информация о типе устройства получена"); 
						close(function () { proc(data); });
					});
					log("запрашиваем информацию о типе устройстве"); 
					send([1,2,3,4,5], function() {
						log("команда запроса типа устройства отправлена"); 
					});
				}
			);
		}

		function check(type, info) {
			log("проверка состояния устройства"); 
			app(
				function (send, receive, close) {
					receive(function (data) {
						log("информация о типе устройства получена"); 
						type(data);
						receive(function (data) {
							log("информация об устройстве получена"); 
							close(function () { info(data); });
						});
						log("запрашиваем информацию об устройстве"); 
						send([1,2,3,4,5], function() {
							log("команда запроса информации отправлена"); 
						});

					});
					log("запрашиваем информацию о типе устройстве"); 
					send([1,2,3,4,5], function() {
						log("команда запроса типа устройства отправлена"); 
					});
				}
			);
		}

		function print(receipt, onfinish) {
			app(
				function (send, receive, close) {
					function openshift(next) {
						receive(assert("Ошибка открытия смены", close, function (data) {
							log("смена открыта"); 
							next();
						}));
						log("открываем смену"); 
						send([1,2,3,4,5], function() {
							log("команда открытия смены отправлена"); 
						});
					}

					function openreceipt(next) {
						receive(assert("Ошибка открытия чека", close, function (data) {
							log("чек открыт");
							next();
						}));
						log("открываем чек"); 
						send([1,2,3,4,5], function() {
							log("команда открытия чека отправлена"); 
						});
					}

					function posdata(pos) {
						// преобразуем позицию чека их json в понятную устройству команду в байтовом массиве
						return [1,2,3,4,5];
					}

					function position(pos, next) {
						receive(assert("Ошибка при добавлении позиции", close, function (data) {
							log("позиция добавлена"); 
							next();
						}));
						log("добавляем позицию в чек"); 
						send(posdata(pos), function() {
							log("команда добавления позиции отправлена"); 
						});
					}

					function closereceipt() {
						receive(assert("Ошибка закрытия чека", close, function (data) {
							log("чек закрыт"); 
							close(onfinish);
						}));
						log("закрываем чек"); 
						send([1,2,3,4,5], function() {
							log("команда закрытия чека отправлена"); 
						});
					}

					openshift(function () {
					openreceipt(function () {
					map(receipt.positions, position, closereceipt)
					})});
				}
			);
		}

		function z(onfinish) {
			log("запрос Z-отчета"); 
			app(
				function (send, receive, close) {
					receive(assert("Ошибка при печати Z-отчета", close, function (data) {
						log("отчет напечатан"); 
						close(function () { proc(data); });
					}));
					log("запрашиваем печать z-отчета"); 
					send([1,2,3,4,5], function() {
						log("команда печати z-отчета отправлена"); 
					});
				}
			);
		}

		proc(beep, demo, info, type, check, print, z);
	}, config.errtable);
}