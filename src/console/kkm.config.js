kkm.config.default = function () {
	function comprefs() {
		return {
			"persistent":false, 
			"name":"KKM", 
			"bufferSize": 512, 
			"bitrate": 57600, 
			"dataBits": "eight", 
			"parityBit": "no", 
			"stopBits": "one", 
			"ctsFlowControl": false, 
			"sendTimeout": 500
		};
	}

	function mock() {
		return {
			"vendor": "mock",
			"id": this.id,
			"comprefs": this.comprefs,
			"password": [0,1,2,3,4,5],
			"operpass": [0,1,2,3,4,5],
			"adminpass": [0,1,2,3,4,5],
			"timeout": this.timeout,
			"cmdtimeout": this.cmdtimeout,
			"porttimeout": this.porttimeout,
                        "receiveTimeout": 500,
                        "sendTimeout": 100,
			"errtable": kkm.mock.errors()
		};
	
	}


	return {
		"id": false,
		"comprefs": comprefs,
		"timeout": 60000,
		"cmdtimeout":10000,
		"porttimeout":500,
		"retries":7,
		"mock":mock,
	};
};