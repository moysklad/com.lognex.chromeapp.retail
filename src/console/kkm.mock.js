kkm.mock.command = function (config, client, util, conn, send1, receive1, close, flush) {
	util(function (l, err, crc, bytechecker) {
		const log = function (msg) { l("..."+msg) };

		function send (data, sentlink) {
			log("sending data "+data+" to virtual fr");
			sentlink();
		};

		function receive(receivedlink) {
			log("receiving fake data from virtual fr");
			receivedlink([1,0,2,3,4,5]);
		}

		client(send, receive, close);
	});
}