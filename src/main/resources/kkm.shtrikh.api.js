kkm.shtrikh.api = function (logging, config, executor, proc) {
	const U = kkm.util;

	const L = U.modlog(logging(), "...");

	const log = L.log;
	const err = L.err;

	const assert = U.assertmaker(config.errtable, err);
    const assert0 = U.assertmaker(config.errtable, function(msg) { err(msg, 001); });

	function app(chain) {
		executor(logging, chain);
	}

	function beep(data, onfinish) {
		app(
			function (send, receive, close) {
				receive(assert("Ошибка при звуковом сигнале", close, function (data) {
					log("beep confirm sais: "+(0 == data[1] ? "beeeeep!" : "error "+data[1]));
					close();
				}));
				send([0x13].concat(config.operpass), function() {
					log("beep sent");
				});
			}
		);
	}

	function demo(data, onfinish) {
		app(
			function (send, receive, close) {
				receive(assert("Ошибка при тестовом прогоне", close, function (data) {
					close();
					if (onfinish && onfinish instanceof Function)
						onfinish();
				}));
				send([0x19].concat(config.operpass).concat([0x01]), function () {
					log("test pass request sent");
				});
			}
		);
	}

function demotext(data, onfinish) {
		app(
			function (send, receive, close) {
				const stalk = U.safetalkmaker(send, receive, close, log, assert);
				const mas = [kkm.config.default.demotext,"","","","","","","","",""];
				const text = mas.map(s => cutbytes(win1251encode(s),20));
				function printline(prev, line) {
					return prev.then(function () {
						return stalk([0x17].concat(config.operpass).concat([3]).concat(cutbytes(line, 40)), "Ошибка при печати строки (" + line + ")");
					});
				}
				text.reduce(printline, Promise.resolve()).then(
					function ()
					{
						close();
						if (onfinish && onfinish instanceof Function)
							onfinish();
					}
				);
			}
		);
	}

	function info(proc) {
		app(
			function(send, receive, close) {
				receive(assert("Ошибка при получении информации об устройстве", close, function (data) {
					close(function () { proc(data); });
				}));
				log("запрос состояния ФР Штрих");
				send([0x11].concat(config.operpass), function() {
					log("long info request sent");
				});
			}
		);
	}

	function type(proc) {
		app(
			function(send, receive, close) {
				receive(assert("Ошибка при получении типа устройства", close, function (data) {
					log("type data received");
					close(function () { proc(data); });
				}));
				send([0xfc], function() {
					log("type request sent");
				});
			}
		);
	}

	function check(type, info) {
		app(
			function (send, receive, close) {
				function t(f) {
					receive(function (data) {
						log(data[1] ? "error "+conf.errtable[data[1]] : "type data received");
						kkm.shtrikh.typedata = data;
						f();
					});
					send([0xfc], function() {
						log("type request sent!");
					});
				}

				function i() {
					type(kkm.shtrikh.typedata);
					receive(function (data) {
                        log("info data received!");
                        close(function () { info(data); });
					});
					send([0x11].concat(config.operpass), function() {
						log("info request sent!");
					});						
				}
				(!!kkm.shtrikh.typedata ? U.empty : t)(i);
			}
		);
	}

	function wait4finish(talk) {
		log("ожидаем окончания процесса");
		return U.waiter(function() {
			return talk([0x10].concat(config.operpass)).
			then(function (infodata) { return new Promise(function (r) {
				const ready = (5 < infodata.length && 0 == infodata[6]);
				r(ready);
			}); });
		}, 2000);
	}

	// отрезка чека
	function cutreceipt(stalk) {
		return  stalk([0xfc], "Ошибка при получении типа устройства").
				then(function(data) {
					const model = data[6];
					return config.nocutmodels.includes(model) ?
						Promise.resolve() :
						stalk([0x25].concat(config.operpass).concat([1]), "Ошибка отрезки чека");
				});
	}

	function print(receipt, onfinish) {
		const linelengths = {
			250:57, // Штрих-М-ФР-К
			7:50, // Штрих-Мини-ФР-К
			4:36, // Штрих-ФР-К
			252:32, // Штрих-Лайт-ФР-К
			240:32, // Штрих-Лайт-ПТК

		};

		var model;
		function check(data, fail, drop) {
			const modes = {
				1: "Выдача данных",
				5: "Блокировка по неправильному паролю налогового инспектора",
				6: "Ожидание подтверждения ввода даты",
				7: "Разрешение изменения положения десятичной точки",
				9: "Режим разрешения технологического обнуления",
				10: "Тестовый прогон.",
				11: "Печать полного фис. отчета",
				12: "Печать отчёта ЭКЛЗ",
				13: "Работа с фискальным подкладным документом",
				14: "Печать подкладного документа"
			};

			switch (true) {
/*				case (0 != data[6]):
					return fail("Закончилась бумага");*/
				case (8 == data[5]): // открыт чек, закрываем 
					return drop();
				case (3 == data[5]):
					return fail("Ошибка при печати кассового чека. Смена превысила 24 часа, для продолжения работы необходимо снять Z-отчет");
				case (0 == data[5] || 4 == data[5] || 2 == data[5] || 15 == data[5]):
					return Promise.resolve();
				default: return fail("Печать чека невозможна в режиме: "+(modes[data[5]] || +data[5]));
			}
		}

		const ll = function () { return (linelengths[model] || 40); };

		const ofdblock = U.ofd(
			win1251encode, 
			fillarr(0x2d, ll()), 
			[0x2a, 0x2a, 0x2a, 0x20, 0xd4, 0xd0, 0x20, 0x2a, 0x2a, 0x2a],
			[0x3a, 0x20]
		)

		log("печатаем чек "+(receipt.return?"возврата":"продажи"));
		const rtype = receipt.return ? [0x02] : [0x00];
		const stype = receipt.return ? [0x82] : [0x80];

		app(
			function (send, receive, close, state) {
				const talk = U.talkmaker(send, receive, log);
				const stalk = U.safetalkmaker(send, receive, close, log, assert);
				const stalk0 = U.safetalkmaker(send, receive, close, log, assert0);

				function printline(prev, line) {
					return prev.then(function () {
						return stalk([0x17].concat(config.operpass).concat([3]).concat(cutbytes(line, 40)), "Ошибка при печати строки (" + line + ")");
					});
				}

				function sell(prev, rl) {
					log("печать строки "+JSON.stringify(rl));

					const quantity = int2bytes(Math.round(rl.quantity*1000), [0,0,0,0,0]).reverse();
					const amount = int2bytes(rl.price, [0,0,0,0,0]).reverse();
					const name = splitbytes(win1251encode(rl.name), ll());

					log("данные подготовлены, amount "+amount.join()+", quantity: "+quantity.join()+", name: "+name.join());

					return prev.then(function () {
						return name.reduce(printline, Promise.resolve()).
						then(function () { return stalk(stype.concat(config.operpass).concat(quantity).concat(amount).concat(zeroarr(45)), "Ошибка печати строки чека") });
					});
				}

				function fillmodel(withmodel) {
					return stalk([0xfc], "Ошибка при получении типа устройства").
					then(function (data) {
						model = data[6];
						log("получили модель Штриха ("+model+")");
						return Promise.resolve();
					});
				}

				function drop(succ) {
					return stalk([0x88].concat(config.operpass), "Ошибка аннулирования незакрытого чека").
					then(function () {
						log("предыдущий не закрытый чек аннулирован");
						return new Promise(function(r) {
							setTimeout(r, 200);
						});
					});
				}

				function fail(msg) {
					return new Promise(function(res) {
						close(function () {
							err(msg);
							res();
						});
					});
				}

				function upload(data) {
					function upload(data, block) {
						log("Uploading " + data + " into block " + block);
						return stalk(
							[0xdd].							// загрузка данных
							concat(config.operpass).		// пароль
							concat([0x00]).					// тип данных (0 для двухмерного штрихкода)
							concat(block).					// порядковый номер блока данных
							concat(data)					// данные (64 байта)
							, "Ошибка загрузки данных")
					}

					const blocks = splitbytes(data, 64);
					return blocks.reduce(
						(p, block, index) => {
							log("Uploading " + JSON.stringify(block) + " into block " + index);
							return p.then(() => upload(block, index));
						}, Promise.resolve());
				}

				function egaisprint(receipt) {
					function qr() {
						if (kkm.shtrikh.qr_supported) {
                            log("Printing QR code");
                            const data = win1251encode(receipt.url);
                            return upload(data).then(() => stalk(
                                [0xde].// печать многомерного штрих-кода
                                concat(config.operpass).// пароль
                                concat([
                                    0x03,						// тип штрихкода (QR)
                                    data.length,				// длина данных
                                    0x00,						// номер начального блока данных
                                    0x00,						// версия (авто)
                                    0x00,
                                    0x00,
                                    0x06,						// размер точки
                                    0x00,
                                    0x02,						// уровень коррекции ошибок
                                    0x01						// выравнивание (по центру)
                                ])
                            ));
                        } else {
							log("Skipping QR code print");
							return Promise.resolve();
						}
					}

					const head = kkm.egais.head(win1251encode, receipt, kkm.shtrikh.cashbox, ll());
					const tail = kkm.egais.tail(win1251encode, receipt, ll());

					return head.reduce(printline, Promise.resolve())
						.then(qr)
						.then(() => tail.reduce(printline, Promise.resolve()));
				}

				fillmodel(). // выясняем с какой моделью ФР работаем
				then(function () { return talk([0x10].concat(config.operpass)); }). // запрашиваем информацию о состоянии ФР
				then(function (data) { return check(data, fail, drop); }). // проверяем, готов ли ФР к печати
				then(function () { return stalk0([0x8d].concat(config.operpass).concat(rtype), "Ошибка открытия чека")}). // открываем чек
				then(function () { // печатаем блок онлайновой фискализации, если такой есть
					log("чек открыт");
					const block = ofdblock(receipt.ofdCode, receipt.ofdKKT, receipt.ofdTime, false);

					return block.reduce(printline, Promise.resolve());
				}).
				then(function () { return receipt.positions.reduce(sell, Promise.resolve()); }). // печатаем строки чека
				then(function () { // задаем скиндку на чек, если указана
					return receipt.discount ?
						stalk([0x86].concat(config.operpass).concat(int2bytes(receipt.discount, [0,0,0,0,0]).reverse()).concat(zeroarr(44)), "Ошибка регистрации скидки"): 
						Promise.resolve();
				}).
				then(function () { return stalk([0x89].concat(config.operpass), "Ошибка печати подытога чека"); }). // печатаем подытог чека
				then(function () { // закрываем чек
					const cash = int2bytes(receipt.paidSum, [0,0,0,0,0]).reverse();
					const nocash = int2bytes(receipt.noCashSum, [0,0,0,0,0]).reverse();
					const summ =  cash.concat(zeroarr(10)).concat(nocash.concat(zeroarr(46)));

					return stalk([0x85].concat(config.operpass).concat(summ), "Ошибка закрытия чека");
				}).
				then(function() { state.stagecritical = false; }). // с этого момента разрыв подключения к ФР не приводит к ошибке в продаже
				then(function () { return wait4finish(talk).then(function () { return new Promise(function (r) { setTimeout(r, 200); }) }); }). // ждем окончания закрытия чека
				then(() => receipt.egais1 ? egaisprint(receipt.egais1) : Promise.resolve()).										// печать чека УТМ, state[8] - номер в зале
				then(function () { return cutreceipt(stalk) }). // отрезаем чек
				then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
				then(onfinish); // передаем управление наружу
			}
		);
	}

	function z(data, onfinish) {
		app(function(send, receive, close) {
			const talk = U.talkmaker(send, receive, log);
			const stalk = U.safetalkmaker(send, receive, close, log, assert);

			stalk([0x41].concat(config.adminpass), "Ошибка печати Z-отчета"). // печатаем отчет
			then(function () { return wait4finish(talk).then(function () { return new Promise(function (r) { setTimeout(r, 200); }) }); }). // ждем окончания печати
			then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
			then(onfinish); // передаем управление наружу
		});
	}

	function x(data, onfinish) {
		app(function(send, receive, close) {
			const talk = U.talkmaker(send, receive, log);
			const stalk = U.safetalkmaker(send, receive, close, log, assert);

			stalk([0x40].concat(config.adminpass), "Ошибка печати X-отчета"). // печатаем отчет
			then(function () { return wait4finish(talk).then(function () { return new Promise(function (r) { setTimeout(r, 200); }) }); }). // ждем окончания печати
			then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
			then(onfinish); // передаем управление наружу
		});
	}

	function change(data, onfinish) {
		if (onfinish && onfinish instanceof Function) onfinish();
	}

	function cashin(data, onfinish) {
		app(function(send, receive, close) {
			const summ = data.sum || data;
			const talk = U.talkmaker(send, receive, log);
			const stalk = U.safetalkmaker(send, receive, close, log, assert);

			stalk([0x50].concat(config.operpass).concat(int2bytes(summ, [0,0,0,0,0]).reverse()), "Ошибка при внесении наличных").
			then(function() { return wait4finish(talk) }). // ждем окончания печати внесения
			then(function () { return cutreceipt(stalk); }). // отрезаем чек
			then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
			then(onfinish);
		});
	}

	function cashout(data, onfinish) {
		app(function(send, receive, close) {
			const summ = data.sum || data;
			const talk = U.talkmaker(send, receive, log);
			const stalk = U.safetalkmaker(send, receive, close, log, assert);

			stalk([0x51].concat(config.operpass).concat(int2bytes(summ, [0,0,0,0,0]).reverse()), "Ошибка при выплате наличных").
			then(function() { return wait4finish(talk); }). // ждем окончания печати выплаты
			then(function () { return cutreceipt(stalk); }). // отрезаем чек
			then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
			then(onfinish);
		});
	}

	proc(beep, /*demo*/demotext, info, type, check, print, z, x, change, cashin, cashout, wait4finish);
}
