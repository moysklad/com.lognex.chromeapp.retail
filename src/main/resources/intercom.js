const logbuf = [];
function log(msg) {
    logbuf.push(msg);
}
function flushlog() {
    const all = logbuf.join("\n");
    console.log(all);
    post({"kkm":"log", "log":all});
    logbuf.length = 0;
}

function disable (what) {
    post({"kkm":"disable", "what":what});
}

function error2retail(msg, code) {
    flushlog();
    log("sending error message ["+msg+"] to retail");
    post({"kkm":"error", "error": msg, "fatal":true, "code":(code || 999)});
    disable("print");
    disable("z");
}

function headersmap(s) {
    m = {};

    function parseline(l) {
        i = l.indexOf(':');
        m[l.substr(0, i)] = l.substr(i+1, l.length);
    }
    s.split("\n").map(parseline);

    return m;
};

window.addEventListener("message", (function (event) {
    const logging = function() {
        return {
            log: log,
            err: error2retail
        };
    };

    const U = kkm.util;
    const findlogging = U.fslogging(logging, U.id);

    var vendor = false, comm = false, config = false, executor = false, ext = false;

    function selected() {
        return (vendor && comm && config && executor && ext);
    }

    function app(proc) {
        if (selected())
            ext(logging, config, executor, proc);
        else
            error2retail("no kkm selected");
    }

    var findid = 0;

    function oncomplete (resp) {
        const res = {"kkm": "complete"};
        if (resp) {
            res.data = resp;
            log("got response data: "+JSON.stringify(resp));
        }
        post(res);
        flushlog();
    }

    const commands = {
        "beep": function (data, onfinish) { app(function (beep, demo, info, type, check, print, z) { beep(data, onfinish); }); },
        "demo": function (data, onfinish) { app(function (beep, demo, info, type, check, print, z) { demo(data, onfinish); }); },
        "print": function (data, onfinish) { log("...printing..."); app(function (beep, demo, info, type, check, print, z) { print(data, onfinish); }); },
        "corrprint": function (data, onfinish) { log("...corr check printing..."); app(function (beep, demo, info, type, check, print, z, x, change, cashin, cashout, ofdreport, corrprint) { corrprint(data, onfinish); }); },
        "z": function (data, onfinish) { log("...z..."); app(function (beep, demo, info, type, check, print, z) { z(data, function() { disable("z"); onfinish(); }); });},
        "x": function (data, onfinish) { log("...x..."); app(function (beep, demo, info, type, check, print, z, x) { x(data, onfinish); });},
        "change": function(data, onfinish) { log("...opening change..."); app(function(beep, demo, info, type, check, print, z, x, change) { change(data, onfinish); }); },
        "cashin": function(data, onfinish) { log("...cash in..."); app(function(beep, demo, info, type, check, print, z, x, change, cashin) { cashin(data, onfinish); });},
        "cashout": function(data, onfinish) { log("...cash out..."); app(function(beep, demo, info, type, check, print, z, x, change, cashin, cashout) { cashout(data, onfinish); });},
        "ofdreport": function(data, onfinish) { log("...ofd report..."); app(function(b, d, i, t, c, p, z, x, ch, cin, cout, ofdreport) { ofdreport(data, onfinish); }); },
        "check": function (data, onfinish) {
            log("checking...?");
            if (! selected()) {
                post({"kkm":"noconn"});
                log("port not specified");
                return;
            }

            log("...checking for "+vendor+"...");
            const res = {"kkm": "check"};

            app(function (beep, demo, info, type, check, print, z) {
                check(res, function (res, datas) {
                    log("got fr properties "+res);
                    log("got fr vendor "+res.vendor);
                    kkm[res.vendor].qr_supported = res.qrsupported; // флаг поддержки проверенным ФР печати QR-кодов
                    kkm[res.vendor].ffd = res.ffd; // версия Формата Фискальных Данных подключенного устройства
                    kkm[res.vendor].ffd105vat = res.ffd105vat; // для какой версии Формата Фискальных Данных сформирована таблица ставок НДС в ФР

                    log("check complete");
                    log("sending to app device info: ");
                    log(JSON.stringify(res));
                    // не зовем onfinish с complete'ом, вместо него отвечаем check
                    post(res);
                    flushlog();
                });
            });
        },
        "printerprint": function(data, onfinish) {
            const fr = document.querySelector("iframe#__printingFrame");
            fr.srcdoc = data;
            fr.onload = function () { fr.contentWindow.print(); };
            onfinish();
        },
        "refresh": function (data, onfinish) {
            if (findid==100) findid = 0;
            findid++;
            const myfindid=findid;

            function findlog(msg) {
              if(findid==myfindid)
                post({"kkm":"findlog", "msg": msg});
            }

            function found(fr) {
                log("found fr "+fr[1]+" ("+fr[0]+")");
                if(findid==myfindid)
                post({"kkm":"found", "fr": fr});
            }

            function notfound(id) {
                log("not found anything on "+id);
                if(findid==myfindid)
                post({"kkm": "notfound","id": id});
            }

            function complete(ids) {
              if(findid==myfindid)
              {
                post({"kkm": "ids", "ids": ids});
                setTimeout(onfinish, 500);
              }
            }

            if (data && data.vendor) kkm.find.forvendor(data.vendor, findlogging, findlog, found, notfound, complete);
            else kkm.find.all(findlogging, findlog, found, notfound, complete);
        },
        "break": function (data, onfinish) {
          if(findid==100)
          {
            findid=0;
          }
          findid++;
            const execs = {};
            function checkcompleter (val) {
                return function () {
                    log("checking for break completed");
                    execs[val] = true;

                    var count = 0;
                    var complete = true;
                    for (k in execs) {
                        if (!execs[k]) {
                            complete = false;
                            count++;
                        }
                    }

                    log("break is "+(complete ? "complete": "incomplete, "+count+" conns more"));

                    if (complete)
                        onfinish();
                }
            }

            log("closing "+kkm.request.break.size);
            kkm.request.break.forEach(function (val, _) {
                execs[val] = false;
                val(checkcompleter(val));
            });
        },
        "select": function (data, onfinish) {
            const vc = data[2].split("_");
            vendor = vc[0];
            log("selected vendor "+vendor);
            comm = (1 < vc.length ? vc[1] : "serial");
            log("selected comm type "+comm);
            kkm.config.default.bitrate = parseInt(data[3]);
            log("speed set to "+data[3])
            ext = kkm[vendor].ext;
            config = kkm.config.default();
            config = config[vendor]();
            config.id = data[0];

            executor = function (logging, chain) {
                kkm[comm].request(logging, config, function (conn, send1, receive1, close, flush, state) {
                    kkm[vendor].command(config, chain, logging, conn, send1, receive1, close, flush, state);
                }, function () { log(config.id+" not available"); }, oncomplete);
            }
            log("fr selected on port "+data[0]);
            app(function(b, d, i, t, c, p, z, x, ch, cin, cout, ofd, corr, hackfr) { hackfr(data, onfinish); });
        },
        "setspeed": function(data, onfinish) {
            kkm.config.default.bitrate = parseInt(data);
            onfinish();
        },
        "testprint": function (data, onfinish) {
            app(function (beep, demo, info, type, check, print, z) { log("performing testprint"); demo(data, onfinish); });
        },
        "version": function (data, onfinish) {
            post({"kkm":"version", "version": chrome.runtime.getManifest().version});
            onfinish();
        },
        "relog": function (data, onfinish) {
            log(data);
        },
        "httpreq": function(data, onfinish) {
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (4 == xhr.readyState) {
                    onfinish({
                        "status": xhr.status,
                        "body": xhr.responseText,
                        "headers": headersmap(xhr.getAllResponseHeaders())
                    });
                }
            };
            xhr.open(data.method, data.url);
            for (h in data.headers) {
                xhr.setRequestHeader(h, data.headers[h]);
            }
            xhr.send(data.body);
        }
    };

    function command(name, data) {
        if (!commands[name]) return;
        commands[name](data, oncomplete);
    }

    return function (event) {
        // MAGIC - это маркер для отличия исходящих сообщений от входящих
        if (event.data.lastIndexOf("MAGIC", 0) != 0) {
            log("got message " + event.data);
            try {
                const req = JSON.parse(event.data);
                log("processing with " + req.msg);
                command(req.msg, req.data);
            }
            catch (e) {
                error2retail(e.message, false);
                flushlog();
            }
        }
    }
})(), false);

chrome.app.runtime.onLaunched.addListener(function (launchData) {
    log("app launched from <"+launchData.url+">");
});
