kkm.atol.ext = function (logging, config, executor, proc) {
	kkm.atol.api(logging, config, executor, function (beep, demo, info, type, check, print, z, x, change, cashin, cashout) {
		const U = kkm.util;
		const L = logging();
		const log = L.log;
		const err = L.err;
		const assert = U.assertmaker(config.errtable, err);

		function p(cmd) {
			return ((config.password || [0,0]).concat(cmd));
		}

		function talkmaker(send, receive, log) {
			const talk = U.talkmaker(send, receive, log);
			return function(data) {
				return talk(p(data));
			}
		};

		function safetalkmaker(send, receive, close, log, assert) {
			const stalk = U.safetalkmaker(send, receive, close, log, assert);
			return function(data, assertmsg ){
				return stalk(p(data), assertmsg);
			}
		};


		function app(chain) {
			executor(logging, chain);
		}

		function checkext(res, checked) {
			const datas = [];

			function fsapp(chain) {
				const fslogging = U.fslogging(logging, function(l) { return function (e) { if (e) l(e.message || e); checked(res, datas); }; });
				return executor(fslogging, chain);
			}

			res.vendor = "atol";
			app(function (send, receive, close) {
					receive(function (data) {
						log(data[0] ? "error "+data[0] : "type data received");
	                    datas.push(data);
						res.model = cp866decode(data.slice(11, data.length));
						receive(function (data) {
						    log("info data received! ["+data+"]");
							datas.push(data);
							if(data[15] && data[16]) res.ver = " v. "+cp866decode([data[15],data[16]]).split("").join(".");
							else res.ver = "";
							res.fiscal = (data[9] & 1);
					        res.doc = 0;
					        res.receipt = fromBCD([0x00, 0x00, data[18], data[19]]);
					        res.change = fromBCD([0x00, 0x00, data[20], data[21]]);
					        res.serial = fromBCD([data[13], data[12], data[11], data[10]]);
							res.qrsupported = false;
                			close(function () { 
                				fsapp(function (fsend, fsreceive, fsclose) {
                					var read = false;
                					setTimeout(function() {
                						if (read) return;
                						log("ffd version read timed out");
                						fsclose(function() { checked(res, datas); });
                					}, 500);
									fsreceive(function(data) {
										log("ffd version received");
										res.ffd = data[2];
										log("ffd version is "+res.ffd);
										fsreceive(function(data) {
											const ffd105vat = data[2] & 8;
											log("vat table ffd version is "+(ffd105vat ? "1.05" : "1.0"));
											res.ffd105vat = ffd105vat;
											fsclose(function() { checked(res, datas); });
										});
										fsend([0x91, 0x38, 0x00, 0x00], function () { // чтение регистра 38h 
											log("fr flags request sent!");
										});
	                				});
									fsend(p([0x91, 0x36, 0x00, 0x00]), function() { // чтение регистра 36h (версия ФФД)
										log("ffd request sent!");
									});
                				});
							});
						});
						send(p([0x3f]), function() {
							log("info request sent!");
						});
					});
					send(p([0xa5]), function() {
						log("type request sent!");
					});
				}
			);
		}

		proc(beep, demo, info, type, checkext, print, z, x, change, cashin, cashout, /*ofdreport*/U.cbpass, /*corrprint*/U.cbpass, /*hackfr*/U.cbpass);
	});
}
