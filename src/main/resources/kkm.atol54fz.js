const STXv3 = 0xFE;
const ESCv3 =  0xFD;

kkm.atol.commandv3util = {
	esc: function(data) {
		var i = -1;
		while (++i < data.length)
			if (STXv3 == data[i] || ESCv3 == data[i]) {
				data.splice(i, 1, ESCv3, data[i] - 16);
				i++;
			}
		return data;
	},
	unesc: function(data) {
		var i = 0;
		while(i++<data.length-1)
			if (data[i] == ESCv3)
				data.splice(i, 2, data[i+1] + 16);
		return data;
	},
	crc8table: (function(poly) {
		const crc8val = (c) => { for (var i=0;i<8;++i) { var p = (c & 0x80) ? poly: 0x00; c = ((c << 1) ^ p) % 256; }; return c; };
		const res = [];
		for (var i=0;i<256;++i) res[i] = crc8val(i);
		return res;
	})(0x31),
	crc8: function(data, table) { var res = 0xff; for (var i=0;i<data.length;i++) res = table[(res^data[i]) % 256]; return res;	},
	len:function(l) {
		return [
			l & 0x7f,
			l >> 7
		];
	},
	unlen:function(data) {
		return data[0] + (data[1] << 7);
	},
	pack: function(data, id, esc, crc8, crc8table, len) {
		const lbytes = len(data.length);
		const crc = crc8([id].concat(data), crc8table);
		const escdata = esc(data);

		return [STXv3].
		concat(lbytes).
		concat(id).
		concat(escdata).
		concat(esc([crc]));
	},
	packcompletemaker: function(unlen, unesc, cmdids) {
		return function(b, l) {
			var shft = -1;
			for (var i=0; i<l; i++)
				if (STXv3 == b[i]) shft = i;

			if (-1 == shft) return false;
			if (shft+3 > l) return false;

			const il = unlen([b[shft+1], b[shft+2]]);
			if (!il) return false;
			const data = [];
			for (var i=4; i<l-shft; i++) {
				data[i-4] = b[shft+i];
			}
			if (0xa1 == data[0] || 0xa2 == data[0] || 0xa8 == data[0])
				return false;

			const id = 0xf0 == b[shft+3] ? b[shft+5] : b[shft+3]
			if (!cmdids.includes(id)) return false;

			return (unesc(data).length == il+1);
		};
	},
	newid: function(U, err) {
		return new Promise(function(res) {
			U.fromstorage().
			then(function(items) {
				log("loaded id from storage "+items.id);
				const id = parseInt(items.id || "0x00");
				const newid = id < 0xdf ? id+1: 0x00;
				return U.tostorage({"id": ""+newid}).
				then(function() { return Promise.resolve(newid); });
			}).
			then(function (newid) {
					log("saved to storage id "+newid);
					res(newid);
			}).
			catch(err);
		});
	},
	unpack: function(rawdata, unlen, unesc) {
		const start = rawdata.lastIndexOf(STXv3);
		const data = -1 == start ? rawdata : rawdata.slice(start);
		const l = unlen(data.slice(1, 3));
		const clean = unesc(data.slice(4, data.length - 1));
		if (clean.length < l) throw "packed incomplete";

		return clean;
	},
	tobuf:function(data, tid) {
		return [0xc1, // команда добавления задания в буфер
				0x01, // флаг NeedResult взведен
				tid, // следующий tid - следующей команде
		].concat(data);
	},
	frombuf:function(data, err) {
		const errs = ["buffer full", "duplicate id", "no such id", "illegal value"];
		switch(data[0]) { // результат обработки команды
			// ошибки формата пакета команды
			case 0xb1:
			case 0xb2:
			case 0xb3:
			case 0xb4:
				throw "error: "+errs[data[0] - 0xb1];
			// обработка команды не завершена (должно отсекаться предпроверкой)
			case 0xa1:
			case 0xa2:
			case 0xa8:
				throw "command processing incomplete ["+data+"]";
			// ошибка при выполнении команды, пробрасываем выше
			case 0xa4:
			// обработка завершена, возвращаем результат
			case 0xa3:
				return data.slice(1);
			// асинхронная ошибка
			case 0xa7:
			// асинхронный успешный результат
			case 0xa6:
				return data.slice(2);
			// обработка команды была прекращена из-за предыдущих ошибок
			case 0xa5:
				throw "command processing stopped ["+data+"]";
			// ответ не по протоколу
			default:
				throw "receive data unknown";
		}
	},
	cmdids: new Array()
};

kkm.atol54fz.command = function (config, client, logging, conn, send1, receive1, close, flush) {
	const U = kkm.util;
	const L = U.modlog(logging(), "...");
	const V3 = kkm.atol.commandv3util;

	const log = L.log;
	const err = L.err;

	function senddata(id, data) {
		return new Promise(function(res) {
			log("sending command ["+data.map(n=>(n).toString(16))+"] to fr (v3)");
			send1(conn, V3.pack(V3.tobuf(data, id), id, V3.esc, V3.crc8, V3.crc8table, V3.len), function (si) {
				V3.cmdids.push(id);
	            log((!!si && 0 < si.bytesSent && !!si.error) ?
	                "failed sending sending command to fr (v3) "+chrome.runtime.lastError.message :
	                "command ["+data.map(hx)+"] sent to fr (v3)"
	            );
	            res();
			});
		});
	}

	function sendabort(id) {
		return new Promise(function(res) {
			log("flushing all previous commands in command buffer");
			send1(conn, V3.pack([0xc4], id, V3.esc, V3.crc8, V3.crc8table, V3.len), function(si) {
	            log((!!si && 0 < si.bytesSent && !!si.error) ?
	                "failed sending abort command to fr (v3) "+chrome.runtime.lastError.message :
	                "abort command sent to fr (v3)"
	            );
	            res();
			});
		});
	}

	function sendreq(id, cid) {
		return new Promise(function (res) {
			log("rerequesting result for last command sent to FR for id "+id+", cid "+cid+" (v3)");
			send1(conn, V3.pack([0xC3, cid], id, V3.esc, V3.crc8, V3.crc8table, V3.len), function(si) {
				V3.cmdids.push(id);
				log("last command result rerequest sent (v3)");
				log("cmdids buffer is ["+V3.cmdids.map(hx)+"]");
				res();
			});
		});
	}

	function pflush(conn) {
		return new Promise(function(res) {
			flush(conn, res);
		});
	}

	function send (sdata, sentlink) { // отправка команды АТОЛ
		pflush(conn).
		then(function() { return V3.newid(U, err); }).
		then(function (id) { log("sendabort from send"); return sendabort(id); }).
		then(function() { return new Promise(function(res) { setTimeout(res, 200); }); }).
		then(function() { return V3.newid(U, err); }).
		then(function (id) { return senddata(id, sdata); }).
		then(sentlink).
		catch(err);
	};

	function receive(receivedlink) { // получение сообщение АТОЛ
		var timer = false;
		function rereq(cd) {
			timer = setTimeout(function() {
				if (6 == cd) return err("requesting command result from FR failed");
				
				return pflush(conn).
				then(function() { return V3.newid(U, err); }).
				then(function(id) {
					if (!V3.cmdids.length) {
						log("no command to rerequest");
						return U.pjam();
					}
					
					const cid = V3.cmdids[0];
					log("Rereq started for " + cid + " on attempt " + cd);
					return sendreq(id, cid); }).
				then(function () { rereq(cd + 1); });
			}, 2000);
		}

		log("receiving command response (v3)");
		receive1(conn, V3.packcompletemaker(V3.unlen, V3.unesc, V3.cmdids), function(d) {
			clearTimeout(timer);
			log("cmdids buffer before flushing ["+V3.cmdids.map(hx)+"]")
			try {
				const res = V3.unpack(d, V3.unlen, V3.unesc);
				log("command response received (v3) ["+res.map(hx)+"]");
				const clean = V3.frombuf(res, err);
				log("received response data ["+clean.map(hx)+"] (v3)");
				V3.cmdids.length = 0;
				receivedlink(clean);
			} catch(e) {
				close(function () { err(e); });
			}
		});
		rereq(1);
	}

	client(send, receive, close);
}