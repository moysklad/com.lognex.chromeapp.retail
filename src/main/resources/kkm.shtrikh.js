kkm.shtrikh.command = function (config, client, logging, conn, send1, receive1, close, flush, state) {
	const U = kkm.util;

	const crc = U.crc;
	const bytechecker = U.bytecheck;

	const L = U.modlog(logging(), "......");

	const log = L.log;
	const err = L.err;

	function pack(data) { // запаковка команды Штрих-М
		data.unshift(data.length);
		data.push(crc(data));
		data.unshift(STX);

		log("packed for sending "+data.map(hx));
		return data;
	}

	var lastcommand = 0;

	/*
	* с конца буфера ищем STX, если нашли, смотрим что получили пакет нужной длины
	* целиком плюс crc, если получили - проверяем сrc
	* если не нашли пакет в буфере - возвращаем false
	*/
	function packet(b, l) {
		function part(b, from, to) {
			const res = [];
			for (var i=from; i<to; i++)
				res.push(b[i]);
			return res;
		}

		for (var i=l-3; i>=0; i--) {
			if (STX == b[i]) {
				const blen = b[i+1]
				if (blen && l-i >= blen+3) {
					if (b[i+2] != lastcommand) continue;
					const bcrc = b[i+blen+2];
					const bdata = part(b, i+1, i+blen+2);
					if (bcrc == crc(bdata))
						return part(b, i, i+blen+3);
				}
			}
		}

		return false;
	}

	function unpack(data) { // распаковка ответа от Штрих-М
		return packet(data, data.length).slice(2, -1);
	}

	var lastsend = function () { log("last send can not be repeated, nothing been sent yet"); };
	function send (data, sentlink) { // отправка команды Штрих-М
		var attempt = 0;
		(lastsend = function () {
			if (++attempt == config.retries) {
				log("send failed in "+config.retries+" attempts");
				close();
				return;
			}

			log("sending command "+data+", attempt "+attempt);
			lastcommand = data[0];
			send1(conn, pack(data), function() {
				if (chrome.runtime.lastError) {
					log("failed sending data, "+chrome.runtime.lastError.message);
					setTimeout(lastsend, config.porttimeout);
				} else {
					log("data sent!");
					sentlink(send, receive);
				}
			});
		})();
	};

	function receive(receivedlink) { // получение ответа сообщения Штрих-М
		var receivetimeout = false;
		(function retry(attempt) {
			if (config.retries == attempt) {
				log("receive failed in "+config.retries+" attempts");
				return;
			}
			
			log("receiveing response, attempt "+attempt+" of "+config.retries);
			receivetimeout = setTimeout(function () {
				log("receive timed out");
				retry(attempt+1);
				send1(conn, [NAK, ENQ], function () {
					log("rerequesting receive");
				});
			}, config.porttimeout);
		})(0);
		
		receive1(conn, packet, function (d) {
			if (receivetimeout)
				clearTimeout(receivetimeout);

			const data = unpack(d);
			const succ = !!data;
			send1(conn, (succ ? [ACK] : [NAK]), function () {
				log((succ ? "confirmed" : "declined")+" data receive");
				if (succ) flush(conn, () => receivedlink(data, send, receive));
			});
		});
	}

	function initcheck() { // проверка состояния ФР перед отправкой команды
		function confirmprev() {
			log("confirming previous command");
			receive1(conn, function(d, l) { return !!packet(d, l); }, function(d) { 
				log("previous command confirmed, processing")
				client(send, receive, close, state);
			});
			send1(conn, [ACK], function() {});
		}

		receive1(conn, bytechecker, function (d) {
			log("on initial received ["+d.map(hx)+"]");
			switch (d[0]) { // обработка текущего состояния фр
				case NAK: // ФР ждет команды, начинаем работу
					log("init successfull");
					client(send, receive, close, state);
					break;
				case ACK:
					log("need to confirm prev command");
					confirmprev();
					break;
				default: // пришел как-то мусор, ждем таймаута, и снова начинаем сначала
					send1(conn, [ACK], function () {
						setTimeout(function (){
							initcheck();
						}, config.porttimeout);
					});
			}
		});
		log("sending initial check");
		send1(conn, [ACK,ENQ], function (si) {
			if (si.error) return err("Ошибка инициализации устройства(возможно некорректная скорость COM-порта): "+si.error);
			log("initial check sent");
		});
	}

	initcheck();
}