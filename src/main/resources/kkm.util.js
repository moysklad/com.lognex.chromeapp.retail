if (!kkm.util)
	kkm.util = {};

kkm.util.incl = function(module) {
	for (k in module)
		this[k] = module[k];
};

kkm.util.empty = function(f) {
	f();
};

kkm.util.crc = function (data) {
	if (2 > data.length) return 0;
	return data.reduce((a, c) => a ^ c);
};

kkm.util.bytecheck = function (vals) {
	if (!vals || 0 == vals.length)
		return function (b, l) {
			return (0 < l);
		}
	else
		return function (b, l) {
			for (var i=0; i<vals.length; i++) 
				if (vals[i] == b[0])
					return true;
			return false;				
		}
};

kkm.util.assertmaker = function (errtable, err) {
	const e = (
		errtable ? 
		function (c) { return errtable[c]; }: 
		function (c) { return c; }
	);

	return function (msg, close, succ) {
		return function (data) {
			const errid = 0xff == data[0] ? 2 : 1;
			if (data[errid]) 
				close(function () {
					err(msg+" ["+e(data[errid])+"]");
				});
			else if (succ)
				succ(data);
		};
	}
};

kkm.util.map = function (list, proc, res) {
	function iter(num) {
		if (num == list.length) 
			return res();
		proc(list[num], function() { iter(num+1); });
	} 
	iter(0);
};

kkm.util.ofd = function (enc, line, header, sep) {
	return function(ofdcode, kktnumber, time, offline) {
		if (!ofdcode && !kktnumber) return [];

		var autonom = enc(" (автономный)");
		var tab = enc(fillarr(" ", ((line.length - 10) / 2)));

		return [line, tab.concat(header), line].
			concat(splitbytes(enc("Фиск. чек: "+ofdcode+(offline ? autonom : "")), line.length)).
			concat([enc("Код ККТ ФНС: "+kktnumber)]).
			concat(splitbytes(enc("Время: "+time), line.length)).
			concat(splitbytes(enc("Для проверки Вашего чека зайдите на сайт: http://consumer.ofd-gnivc.ru"), line.length)).
			concat([line]);
	};
};

kkm.util.pempty = function () {
	return Promise.resolve();
}

kkm.util.pass = function(v) {
	return function () {
		return Promise.resolve(v);
	}
}

kkm.util.cbpass = function(data, onfinish) {
	onfinish(data);
}

kkm.util.talkmaker = function(send, receive, log) {
	return function (data) {
		return new Promise(function(res, rej) {
			receive(res);
			send(data, function(){
				log("...sent to fr: ["+data.map(hx)+"]");
			});
		});
	};
}

kkm.util.safetalkmaker = function (send, receive, close, log, assert) {
	return function(data, assertmsg) {
		return new Promise(function (res, rej) {
			receive(assert(assertmsg, close, res));
			send(data, function () {
				log("...safely sent to fr: ["+data.map(hx)+"]");
			});
		});
	};
}

kkm.util.waiter = function (checker, timeout) {
	return new Promise(function (res, rej) {
		function retry() {
			checker().then(function (complete) {
				if (complete) res();
				else setTimeout(retry, timeout);
			});
		}
		retry();
	});
}

kkm.util.modlog = function(logging, prefix)  {
	const l = logging.log;
	return {
		log: function(m) { l(prefix+m); },
		err: logging.err
	};
};

kkm.util.fromstorage = function() {
	return new Promise(function (res) {
		chrome.storage.local.get(res);
	});
};

kkm.util.tostorage = function(items) {
	return new Promise(function (res) {
		chrome.storage.local.set(items, res);
	});
};

// если название товара в байтах b не помещается в tl символов команды регистрации, при печати на ФР
// с длиной строки ll, сперва печатать его строками, хвост длинной до tl отдавать в регистрацию
kkm.util.tail = function(b, ll, tl) {
	const l = b.length % ll;
	if (l > tl) 
		return [splitbytes(b.slice(0, b.length - l), ll), b.slice(l)];

	const el = tl - l;
	const c = el/ll|0;

	const rtl = c*ll + l;

	const s = b.length - rtl;
	return [splitbytes(b.slice(0, s), ll), b.slice(s)];
}

kkm.util.bytes2str = data => "[" + data.map(hx).join(", ") + "]";

kkm.util.chunk = (arr, size) => {
	const copy = arr.slice();
	const chunks = [];

	while (copy.length > 0) {
		chunks.push(copy.slice(0, size));
	}

	return chunks;
};

// возвращает Promise, который останавливает выполенние цепочки промисов
kkm.util.pjam = function() {
	return new Promise(function(){});
}

kkm.util.fslogging = function(logging, fserr) {
	const L = logging();
	return function () {
		return {
			log: L.log,
			err: fserr(L.log)
		}
	}
}

kkm.util.id = function(v) { return v; }