kkm.atol54fz.ext = function (logging, config, executor, proc) {
	/*
	* переиспользуем из старого АПИ не изменившиеся функции
	*/
	kkm.atol.ext(logging, config, executor, function (beep, demo, info, type, check, print, z, x, change, cashin, cashout, ofdreport, corrprintpass, hackfr) {
		const U = kkm.util;

		function app(chain) {
			executor(logging, chain);
		}

		function fsapp(chain) {
			executor(U.fslogging(logging, U.id), chain);
		}


		const L = logging();
		const log = L.log;
		const err = L.err;
		const assert = U.assertmaker(config.errtable, err);
	    const assert0 = U.assertmaker(config.errtable, function(msg) { err(msg, 001); });

		function p(cmd) {
			return ((config.password || [0,0]).concat(cmd));
		}

		function talkmaker(send, receive, log) {
			const talk = U.talkmaker(send, receive, log);
			return function(data) {
				return talk(p(data));
			}
		};

		function safetalkmaker(send, receive, close, log, assert) {
			const stalk = U.safetalkmaker(send, receive, close, log, assert);
			return function(data, assertmsg) {
				return stalk(p(data), assertmsg);
			}
		};

		function check54fz(res, checked) {
			check(res, function(res, datas) {
				res.fz54 = true;
				res.vendor = "atol54fz";
				res.qrsupported = true;
				checked(res, datas);
			});
		}

		function print(data, onfinish) {
			data.discount = 0;
			kkm.atol.print.print4pos(config, data, kkm.atol.print.pos54fz, onfinish, app, log, err, assert, talkmaker, safetalkmaker);
		}

		function corrprint(data, onfinish) {
			kkm.atol.print.print4corr(config, data, onfinish, app, log, err, assert, talkmaker, safetalkmaker);
		}

		function reportmaker(cmd, mode, modename) {
			return function(cashier, onfinish) {
				function reportmode(talk, stalk) {
					function modeswitch() {
						log("переход в режим отчетов "+modename)
						return stalk([0x56, mode].concat(config.adminpass), "Ошибка перехода в режим отчетов "+modename);
					}

					function modeexit() {
						log("переход в режим выбора");
						return stalk([0x48], "Ошибка выхода в режим выбора").
						then(function () { return modeswitch(); });
					}

					return talk([0x3f]).then(function(data) {
						const m = data[17];
						log("перед печатью отчета ФР в режиме "+m);

						var proc = modeexit;

						if (0 == m) // в режиме выбора
							proc = modeswitch;
						if (mode == m) // в режиме печати отчетов c/без гашения
							proc = U.pass();

						return proc();
					});
				}

				function wait4finish(talk) {
					log("проверяем завершена ли печать отчета")
					function checker() {
						log("отправляем запрос состояния устройства");
						return talk([0x45]).then(function (data) {
							const res = !(0x17 == data[1] || [0x22] == data[1] || [0x23] == data[1]);
							log(res ? "отчет напечатан" : "отчет все еще печатается");
							return res;
						});
					}

					return U.waiter(checker, 500);
				}

				app(
					function(send, receive, close) {
						const talk = talkmaker(send, receive, log);
						const stalk = safetalkmaker(send, receive, close, log, assert);
						const P = kkm.atol.print;
						const setreq = P.setreqmaker(stalk, log);

						reportmode(talk, stalk).
						then(function () { // устанавливаем кассира для печати в отчете
							return (!!cashier) ?
								setreq(1021, cashier) :
								Promise.resolve();
						}).
						then(function () { return stalk(cmd, "Ошибка запроса отчета"); }).
						then(function () { return wait4finish(talk); }).
						then(function () { close(onfinish); });
					}
				);
			}
		}

		function moderegister(stalk, talk) { // переход в режим регистрации
			return talk([0x3f]).
			then(function (data) {
				function reg() {
					return stalk([0x56, 0x01].concat(config.adminpass), "Ошибка при переходе в режим регистрации");
				}

				if (0 != (data[9] & 2)) {// смена уже открыта
					return Promise.reject(new Error("Смена уже открыта"));
				}

				if (1 == data[17])
					return Promise.resolve();

				if (0 == data[17])
					return reg();

				return stalk([0x48], "Ошибка при переходе в режим выбора").then(reg);
			});
		}

		function changeext() {
			const l = arguments.length;
			const cb = arguments[l-1];
			const cashier = 1<l ? arguments[0] : false;

			app(function(send, receive, close) {
				const stalk = safetalkmaker(send, receive, close, log, assert0);
				const talk = talkmaker(send, receive, log);
				const P = kkm.atol.print;
				const setreq = P.setreqmaker(stalk, log);

				return moderegister(stalk, talk).
				then(function () { // устанавливаем кассира для печати в отчете
					return (!!cashier) ?
						setreq(1021, cashier) :
						Promise.resolve();
				}).
				then(function() {
					return stalk([0x9a, 0x00], "Ошибка при открытии смены");
				}).
				then(function() { close(cb); }).
				catch(function(e) { close(function() { err(e.message ? e.message : e); }); });
			});
		}

		function hackfr(data, onfinish) {
			const AP = kkm.atol.print;
			// fsapp insteed of app for non-critical operations
			fsapp(function(send, receive, close) {
				const talk = talkmaker(send, receive, log);
				const fsassert = U.assertmaker(config.errtable, log);
				const fstalk = safetalkmaker(send, receive, close, log, fsassert);

				const setfield = AP.setfieldmaker(log, log, talk, fstalk, config);

				setfield(2, 1, 115, [0x01]).
				then(function () {
					close(onfinish);
				}).
				catch(function(e) {
					// log insteed of err coz it's not fatal to fail hackfr
					close(function() { log(e.message || e); });
				}) ;
			});
		}

		const zext = reportmaker([0x5a], 0x03, "с гашением");
		const xext = reportmaker([0x67,0x01], 0x02, "без гашения");
		const ofdext = reportmaker([0x67,0x09], 0x02, "без гашения");
		proc(beep, demo, info, type, check54fz, print, zext, xext, changeext, cashin, cashout, ofdext, corrprint, hackfr);
	});
}
