const retail = document.getElementById("retail");
function post(data) {
    retail.contentWindow.postMessage(JSON.stringify(data), "*");
}

(resize = function () {
    retail.style.height = ""+innerHeight+"px";
})();

retail.addEventListener('permissionrequest', function(e) {
    console.log("permission request for "+e.permission);
    e.request.allow();
});
retail.addEventListener("loadcommit", function () {
    post({"kkm":"init"});
    chrome.storage.local.get("inst", function (res) {
        if (res.inst) return;
        retail.clearData({}, {appcache:true}, function () { console.log("appcache cleared"); });
        chrome.storage.local.set({"inst":new Date().getTime()});
    })
});
retail.addEventListener("newwindow", function(e) {
    e.preventDefault();
    chrome.app.window.create("external.html?"+escape(e.targetUrl), {'state':"maximized"});
});

window.onresize = function () {
    resize();
};