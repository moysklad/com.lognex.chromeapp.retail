if (!kkm.pirit.utils)
    kkm.pirit.utils = {};

kkm.pirit.utils.esc = function (data) {
    var i = -1;
    while (++i < data.length)
        if (ETX == data[i] || DLE == data[i])
            data.splice(i++, 0, DLE);
}

kkm.pirit.utils.unesc = function (data) {
    var i = 0;
    while(i++<data.length)
        if (data[i] == ACK)
            data.splice(i,1);
    return data;
}

kkm.pirit.utils.piritcrc = function (crc) {
    i1 = Math.floor(crc / 16);
    if(i1 > 9) i1 = i1 + 0x37;
    else i1 = i1 | 0x30;
    i2 = (crc % 16);
    if(i2 > 9) i2 = i2 + 0x37;
    else i2 = i2 | 0x30;

    return [i1, i2];
}

kkm.pirit.utils.pack = function (data) { // запаковка команды Pirit
    const PU = kkm.pirit.utils;
    const crc = kkm.util.crc;
    const piritcrc = PU.piritcrc;

    var i = 0;
    var i1, i2;
    i1 = 0; i2 = 0;

    PU.esc(data);
    data.push(ETX);
    i = piritcrc(crc(data));
    data.push(i[0]);
    data.push(i[1]);
    data.unshift(STX);
    log("Data to FR:"+data);
    return data;
}

kkm.pirit.utils.unpack = function (d) { // распаковка ответа от Pirit
    const PU = kkm.pirit.utils;

    const shft = firstIndex(d, d.length, function(i, v) {
      if (STX != v) return false;
      if (d.length -i < 8) return false;
      if (PU.sentid != d[i+1]) return false;

      return true;
    });
    const limit = shft + firstIndex(d.slice(shft), d.length-shft, function(i, v) { return ETX == v; });

    const data = d.slice(shft, limit + 3);

    if (STX != data[0])
        throw new Error ("no STX found");
    data.shift();
    if (ETX != data[data.length - 3])
        throw new Error("no ETX found");
    data.pop();
    data.pop();
    data.pop();
    return PU.unesc(data);
}

kkm.pirit.utils.receivedpacket = function (b, l) {
    const PU = kkm.pirit.utils;

    const crc = kkm.util.crc;
    const piritcrc = PU.piritcrc;

    if (ENQ == b[l-1]) return true;

    const shft = firstIndex(b, l, function(i, v) {
      if (STX != v) return false;
      if (l -i < 8) return false;
      if (PU.sentid != b[i+1]) return false;

      return true;
    });

    if (-1 == shft) return false;

    const lmt = firstIndex(b, l, function(i, v ){
        if (i < shft+5) return false; // ищем ETX после STX
        if (v != ETX) return false; // получили ETX
        if ((i + 2) >= l) return false; // получили после ETX 2 байта CRC

        const сcrc = piritcrc(crc(b.slice(shft+1, i+1)));  // CRC полученного пакета
        return (сcrc[0] == b[i+1] && сcrc[1] == b[i+2]); // полученный CRC пакета совпадает с рассчетным
    });

    return (-1 < lmt);
}

kkm.pirit.utils.sentid = false;

kkm.pirit.command = function (config, client, logging, conn, send1, receive1, close, flush) {
    kkm.util.incl(kkm.util.modlog(logging(), '......'));
    const bytechecker = kkm.util.bytecheck;
    const crc = kkm.util.crc;
    const PU = kkm.pirit.utils;

    PU.sentid = false;

    function r(conn, check, callback) {
        receive1(conn, check, function (d) {
            flush(conn, function (){
                callback(d);
            });
        });
    }

    var lastsend = function() { log("nothing been sent yet to pirit"); };
    function send (d, sentlink) {
      const data = PU.pack(d);
        (lastsend = function() {
            log("Function Send Start");
//            r(conn, bytechecker(), function(d) {
                send1(conn, data, function(si) {
                  PU.sentid = data[5];
                  const le = chrome.runtime.lastError;
                  var msg = "";
                  if((!si || si.error))
                    if(le && le.message) msg = le.message;
                    else msg = "data sent failed!";
                  else msg = "data sent!";  
                  log(msg);
                  if (sentlink)
                    sentlink(send, receive);
                });
                log("Function Send End");
//            });
//            send1(conn, [ENQ], function (si) {
//                if (!si || si.error)
//                    log("failed complete sending data, "+chrome.runtime.lastError.messsage);
//            });
        })();
    };

    function receive(receivedlink) { // получение сообщение АТОЛ
        log("Function Recieve Start");
        var timeout = false;
        function retry(attempt) {
            timeout = setTimeout(function() {
                if (config.retries < attempt) log("command response receive failed in "+config.retries+" attemts");
                else {
                    log("resending last sent command, attempt "+attempt+" of "+config.retries)
                    lastsend();
                    retry(attempt + 1);
                }
            }, 2000);
        }

        r(conn, PU.receivedpacket, function(d) {
            clearTimeout(timeout);
            if (ENQ == d[d.length - 1]) send1(conn, [ACK], function() { receive(receivedlink); });
            else receivedlink(PU.unpack(d));
            log("Function Recieve END");
        });
        log("receive initted");

        retry(1);
    }

    client(send, receive, close);
}
