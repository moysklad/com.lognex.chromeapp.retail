if (!kkm.pirit.utils)
    kkm.pirit.utils = {};

kkm.pirit.utils.errcode = function (data) {
    if (!data || !data.length || data.length < 5) return 0;
    return 10 * (data[3]-0x30) + (data[4]-0x30);
}

kkm.pirit.utils.statusmaker = function (log) {
    return function (data) {
        var mas = new Array();
        var o1 = 0;
        var f1 = 0;
        var f2 = 0;
        var f3 = 0;
        log("Fuction getPiritStatus Start");
        o1 = data.indexOf(28);
        log("O = "+o1);

        mas = data.slice(5,o1);
        for(i=0;i<mas.length; i++)
        {
            f1 *= 10;
            f1 += mas[i] - 0x30;
        }

        o2 = data.indexOf(28,o1+1);
        mas = data.slice(o1+1,o2);
        for(i=0;i<mas.length; i++)
        {
            f2 *= 10;
            f2 += mas[i] - 0x30;
        }
        for(i=o2+1;i<data.length-1; i++)
        {
            f3 *= 10;
            f3 += data[i] - 0x30;
        }
        log("f1 = "+f1);
        log("f2 = "+f2);
        log("f3 = "+f3);

        mas[0] = f1;
        mas[1] = f2;
        mas[2] = f3;
        log("Fuction getPiritStatus End");
        return mas;
    }
};
kkm.pirit.utils.link = function (send, receive, config) {
    function p(cmd) {
        return ((config.password).concat(cmd));
    }

    return function (data, save) {
        return function () {
            return new Promise(function (next) {
                receive(function (d) {
                    save(d);
                    next(d);
                });
                send(p(data), function () {});
            });
        };
    }
};
kkm.pirit.utils.commandStart = function (send, receive, close, config, func1) {
    var  CurrTime = new Date();
    ctday = CurrTime.getDate();
    ctday = (ctday>9)?ctday:"0"+ctday;

    ctmon = CurrTime.getMonth()+1;
    ctmon = (ctmon>9)?ctmon:"0"+ctmon;

    ctyear = CurrTime.getYear();
    ctyear = (ctyear%100);

    cthour = CurrTime.getHours();
    cthour = (cthour>9)?cthour:"0"+cthour;

    ctmin = CurrTime.getMinutes();
    ctmin = (ctmin>9)?ctmin:"0"+ctmin;

    ctsec = CurrTime.getSeconds();
    ctsec = (ctsec>9)?ctsec:"0"+ctsec;

    str = ""+ctday+ctmon+ctyear+String.fromCharCode(0x1C)+cthour+ctmin+ctsec;

    const PU = kkm.pirit.utils;
    const l = PU.link(send, receive, config);

    l([0x24, 0x31, 0x30].concat(charencode(str)), func1)();
};
kkm.pirit.utils.frerrormaker = function(log, err) {
    return function(f1) {
        if (f1 == 0) return;
        switch(true) {
            case ((f1 &   1) != 0): err("Неверная контрольная сумма NVR");break;
            case ((f1 &   2) != 0): err("Неверная контрольная сумма в конфигурации");break;
            case ((f1 &   4) != 0): err("Ошибка интерфейса с ФП");break;
            case ((f1 &   8) != 0): err("Неверная контрольная сумма фискальной памяти");break;
            case ((f1 &  16) != 0): err("Ошибка записи в фискальную память");break;
            case ((f1 &  32) != 0): err("Фискальный модуль не авторизован");break;
            case ((f1 &  64) != 0): err("Фатальная ошибка ЭКЛЗ");break;
            case ((f1 & 128) != 0): err("Расхождение данных ФП и ЭКЛЗ");break;
        }
        log("Error from FR:"+f1);
    }
};
kkm.pirit.utils.convIntToInt = function (intNum) {
    const res = [0];
    str = intNum.toString();
    len = str.length;
    for (var i=0; i<len; i++)
    {
        if((str[i] == ".")||(str[i]==",")) res[i]=0x2E;
        else if((str[i] >= "0")&&(str[i]<="9")) res[i]= 0x30+parseInt(str[i],10);;
    }
    return res;
}

kkm.pirit.utils.version = 0;

kkm.pirit.api = function (logging, config, executor, proc) {
    const U = kkm.util;

    const empty = U.empty;
    const map = U.map;
    const crc = U.crc;

    const L = U.modlog(logging(), "...");

    const log = L.log;
    const err = L.err;

    const assert = U.assertmaker(config.errtable, err);

    const PU = kkm.pirit.utils;
    const getPiritStatus = PU.statusmaker(log);
    const frerror = PU.frerrormaker(log, err);

    function app(chain) {
        executor(logging, chain);
    }

    function p(cmd) {
        return ((config.password).concat(cmd));
    }

    const talkmaker = function(send, receive, log) {
        const talk = talkmaker(send, receive, log);
        return function(data) {
            talk(p(data));
        }
    };

    const safetalkmaker = function(send, receive, close, log, assert) {
        const stalk = safetalkmaker(send, receive, close, log, assert);
        return function(data, assertmsg) {
            return stalk(p(data), assertmsg);
        }
    };


    function beep(data, onfinish) {
        const PU = kkm.pirit.utils;
        function doBeep(send, receive, close)
        {

            send(p([0x81, 0x30, 0x30]), function() {
                log("beeeeep.............................................Start");
                receive(function (data){
                    log("Hello World!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    for(i=0;i<data.length; i++)
                    {
                        log("D["+i+"]:"+data[i]);
                    }
                    var m10 = new Array();
                    m10 = getPiritStatus(data);

                    log("1="+m10[0]+"  2="+m10[1]+ "   3="+m10[2]);
                    f1 = m10[0];
                    f2 = m10[1];
                    f3 = m10[2];

                    var errNum = PU.errcode(data);

                    if (errNum != 0) log("Error from FR B1:"+errNum);
                    if (f1 != 0) log("Error from FR:"+f1);
                    if ((f2 & 1) != 0){
                        PU.commandStart(send, receive, close, config, function (){doBeep(send, receive, close);});
                    }else{
                        send(p([0x82, 0x38, 0x32, 0x37, 0x38]), function() {
                            receive(function (data){
                                close();
                            });
                        });
                    }
                    if (f3 != 0) log("Error from FR B3:"+f3);

                });
                log("beeeeep..............................................End");
            });

        }
        app(
            function (send, receive, close) {
                doBeep(send, receive, close);
            }
        );
    }

    function demo(data, onfinish) {
        app(function (send, receive, close) {
            const PU = kkm.pirit.utils;
            const l = PU.link(send, receive, config);

            l([0x94, 0x39, 0x34], function () {
                close();
                if (onfinish && onfinish instanceof Function)
                    onfinish();
            })();
        });
    }

    function demotext(data, onfinish) {
      log("demo print");
      app(function(send, receive, close) {

        function demoprint() {
          const text = cutbytes(cp866encode(kkm.config.default.demotext), 72);
          const opendoc= cp866encode("30")                                                    // открытие докумениа
          .concat(cp866encode((0b00000001).toString(16)))                                     // сервисный документ
          .concat([0x1C, 0x31, 0x1C, 0x8A, 0xA0, 0xE1, 0xE1, 0xA8, 0xE0, 0x1C, 0x30, 0x1C]);
          const printtextcom = cp866encode("40");                                             // печать текста
          const closedoc = cp866encode("31");                                                 // закрытие документа
          return talk(p([0x50].concat(opendoc))).
          then(function() { return talk(p([0x51].concat(printtextcom).concat(text).concat([0x1C]).concat(cp866encode((0b00000001).toString(16))))); }).
          then(function() { return talk(p([0x51].concat(printtextcom).concat(cutbytes(cp866encode(""), 72)).concat([0x1C]).concat(cp866encode((0b00000001).toString(16))))); }).
          then(function() { return talk(p([0x52].concat(closedoc))); }).
          then(
            function ()
                    {
                        close();
                        if (onfinish && onfinish instanceof Function)
                            onfinish();
                    }
          );
        }

        function check(data) {
            const PU = kkm.pirit.utils;
            const retry = checkFR;
            const m10 = getPiritStatus(data); // получаем флаги состояния из запроса
            frerror(m10[0]); // проверяем на фатальные ошибки
            if ((m10[1] & 1) != 0) PU.commandStart(send, receive, close, config, retry); // начать обмен данными с ФР, если еще не начат
            else if (m10[2] != 0) talk(p([0x32, 0x33, 0x32])).then(retry); // аннулируем предыдущий документ, если он открыт
            else return demoprint();
        }
        const talk = U.talkmaker(send, receive, log);

        function checkFR()
        {
          log("проверяем состояние ФР");
                talk(p([0x81, 0x30, 0x30])).
                then(check);
        }

        checkFR();

      });
    }

    function info(proc) {
        app(function (send, receive, close) {
            inforeq(send, receive, close, function(data) {
                close(function () { proc(data); });
            });
        });
    }

    function inforeq(send, receive, proc) {
        const res = {};
        function s(path) {
            return function (data) {
                res[path] = data;
            }
        }

        const PU = kkm.pirit.utils;
        const l = PU.link(send, receive, config);

        l([0x25, 0x30, 0x32, 0x34], s("num"))().
        then(l([0x27, 0x30, 0x30], s("state"))).
        then(l([0x28, 0x30, 0x31, 0x31], s("shift"))).
        then(l([0x29, 0x30, 0x31, 0x32], s("receipt"))).
        catch(function(e) { err(e); }).
        then(function () {
            proc(res);
        });
    }

    function type(proc) {
        app(function (send, receive, close) {
            typereq(send, receive, function(data) {
                close(function() { proc(data); });
            });;
        });
    }

    function typereq(send, receive, proc) {
        const PU = kkm.pirit.utils;
        const l = PU.link(send, receive, config);
        l([0x26, 0x30, 0x32, 0x32], function (data) { proc(data); })().
        then(function () { log("type info got"); }).
        catch(function (e) { err("error requesting type info: "+e); });
    }

    function model54fz(fw) {
        switch (true) {
            case (fw >=  10 && fw <  200): return ["Пирит ФР-01К ЕНВД", false];
            case (fw >= 200 && fw <= 250): return ["Pirit-K", false];
            case (fw >= 400 && fw <= 450): return ["Pirit ЕНВД", false];
            case (fw >= 500 && fw <= 549): return ["Pirit 01Ф", true];
            case (fw >= 550 && fw <= 599): return ["Pirit 02Ф", true];
            case (fw >= 600 && fw <= 650): return ["VikiPrint", false];
            case (fw >= 650 && fw <= 699): return ["VikiPrint Ф", true];
            default:
                log("Unknown firmware: " + fw + ". Setting default Pirit")
                return ["Pirit", false];
        }
    }

    function check(res, checked) {
        const datas = [];
        app(
            function (send, receive, close) {
                typereq(send, receive, function (data) {
                    log(data[3] === 48 && data[4] === 48 ? "type data received" : "error " + win1251decode([data[3], data[4]]) );
                    datas.push(data);

                    const firmware = win1251decode(data.slice(data.indexOf(28, 5) + 1, data.indexOf(28, data.indexOf(28, 5) + 1)));
                    log("firmware :"+firmware);
                    const mAndFz = model54fz(firmware);
                    res.ver = " v. "+firmware;
                    kkm.pirit.utils.version = firmware;
                    res.model = mAndFz[0];
                    res.fz54 = mAndFz[1];
                    res.vendor = res.fz54 ? "pirit54fz" : "pirit";

                    inforeq(send, receive, function(data) {
                        log("info data received! ["+data+"]");
                        datas.push(data);

                        res.fiscal = !(data.state.slice(3, data.state.indexOf(28))[0] & 1);
                        res.doc = 0;
                        res.receipt = chardecode(data.receipt.slice(3, data.receipt.indexOf(28)));
                        res.change = chardecode(data.shift.slice(3, data.shift.indexOf(28)));
                        res.serial = chardecode(data.num.slice(7,18));

                        close(function () { checked(res, datas); });
                    });
                });
            }
        );
    }


    function convIntToInt(intNum)
    {
        const res = [0];
        str = intNum.toString();
        len = str.length;
        for (var i=0; i<len; i++)
        {
            if((str[i] == ".")||(str[i]==",")) res[i]=0x2E;
            else if((str[i] >= "0")&&(str[i]<="9")) res[i]= 0x30+parseInt(str[i],10);;
        }
        return res;
    }

    function vat(v) {
        switch(v) {
            case (18): return 0x30;
            case (10): return 0x31;
            case (0): return 0x32;
            default: return 0x33;
        }
    }

    function print(receipt, onfinish) {
        function rtype() {
            return receipt.return ? 0x33: 0x32;
        }


        function sellt(send, receive, close, rl, link, maxlength)
        {
            log("Function Sellt");
            log("печать строки "+JSON.stringify(rl));

            var length = rl.name.length > maxlength ? maxlength : rl.name.length;

            const name = splitbytes(cp866encode(rl.name.substring(0, length)), 224);
            const nameEx = cp866encode_pirit(rl.name.substring(0,length));

            strName = nameEx;
            str1C = [0x1C];
            strQ = convIntToInt(rl.quantity);
            strP = convIntToInt(rl.price/100);
            strVat = vat(rl.vat);


            strM = [0x42, 0x34, 0x32].concat(strName).concat(str1C).concat(str1C).concat(strQ).concat(str1C).concat(strP).concat(str1C).concat(strVat).concat(str1C).concat(str1C).concat(0x31).concat(str1C);

            map(name, function (v, f) {
                send(p(strM), function() {
                    receive(function (data){
                        log("позиция добавлена");
                        f();
                    });
                });
            }, link);
        }

        const emsg = function(code) {
            return config.errtable[code] || "Ошибка при открытии смены ("+code+")";
        };

        function printReceipt(send, receive, close, receipt, onfinish) {
            const PU = kkm.pirit.utils;
            const l = PU.link(send, receive, config);
            function e(m) {
                throw new Error(m);
            }
            var toprint;

            return new Promise(function (f) { log("starting print receipt..."); f(); }).
            then(l([0x81, 0x30, 0x30], function (data) { // проверка состояния фр перед печатью
                var m10 = new Array();
                m10 = getPiritStatus(data);

                f1 = m10[0];
                f2 = m10[1];
                f3 = m10[2];

                var errNum = PU.errcode(data)

                if (errNum != 0) log("Error from FR err:"+errNum);
                if (f1 != 0) {
                    log("Error from FR B1:"+f1);
                    if ((f1 &   1) != 0) e("Неверная контрольная сумма NVR");
                    if ((f1 &   2) != 0) e("Неверная контрольная сумма в конфигурации");
                    if ((f1 &   4) != 0) e("Ошибка интерфейса с ФП");
                    if ((f1 &   8) != 0) e("Неверная контрольная сумма фискальной памяти");
                    if ((f1 &  16) != 0) e("Ошибка записи в фискальную память");
                    if ((f1 &  32) != 0) e("Фискальный модуль не авторизован");
                    if ((f1 &  64) != 0) e("Фатальная ошибка ЭКЛЗ");
                    if ((f1 & 128) != 0) e("Расхождение данных ФП и ЭКЛЗ");
                } else if ((f2 & 1) != 0) {
                    toprint = function (onfinish) { PU.commandStart(send, receive, close, config, function () { printReceipt(send, receive, close, receipt, onfinish); }); };
                } else if ((f2 & 8) != 0) {
                    err("Смена больше 24 часов");
                    close();
                } else if (f3 != 0){
                    toprint = function (onfinish) {
                        receive(function (data){
                            printReceipt(send, receive, close, receipt, onfinish);
                        });
                        send(p([0x32, 0x33, 0x32]), function() {});
                    };
                } else {
                    toprint = function (onfinish) {
                        receive(function (data) {
                            const ec = PU.errcode(data)
                            if (ec) return close(()=>err(emsg(ec, 001)));

                            masPos = receipt.positions;
                            map(receipt.positions, function(v, f) {
                                l([0x26, 0x30, 0x32, 0x32], function (data) {
                                    var firmware = win1251decode(data.slice(data.indexOf(28, 5) + 1, data.indexOf(28, data.indexOf(28, 5) + 1)));
                                    var maxlength = 224;
                                    if (firmware >= 10 && firmware < 200) {
                                        maxlength = 150;
                                    }
                                    sellt(send, receive, close, v, f, maxlength);
                                })()

                            }, function() { // печатаем строки чека
                                log("печать строк завершена");
                                receive(function (){
                                    const t = U.talkmaker(send, receive, log);
                                    const exec = (strM) => t(p(strM));

                                    function printSum(summ, type) {
                                        log("печатаем сумму");

                                        const strD = convIntToInt(summ/100);
                                        const strM = [0x47, 0x34, 0x37].concat(type).concat(0x1C).concat(strD).concat(0x1C).concat(0x1C);

                                        return exec(strM);
                                    }

                                    function closeReceipt() {
                                        log("закрытие чека");
                                        const enccounterparty = !!receipt.counterparty ? cp866encode(receipt.counterparty).concat([0x1C]) : [];

                                        const strM = [0x31, 0x33, 0x31, 0x30, 0x1C].concat(enccounterparty);

                                        return exec(strM);
                                    }

                                    function egaisprint() {
                                        function opendoc() {
                                            log("Открываем сервисный документ");

                                            const strM = [0x28]
                                                .concat(cp866encode("30"))                                                          // открытие докумениа
                                                .concat(cp866encode((0b00000001).toString(16)))                                     // сервисный документ
                                                .concat([0x1C, 0x31, 0x1C, 0x8A, 0xA0, 0xE1, 0xE1, 0xA8, 0xE0, 0x1C, 0x30, 0x1C]);  // it's a kind of magic

                                            return exec(strM);
                                        }

                                        function readcashbox() {
                                            log("Читаем номер кассы");

                                            const strM = [0x27]
                                                .concat(cp866encode("11"))                  // чтение таблицы настроек
                                                .concat(cp866encode("10"))                  // логический номер кассы
                                                .concat([0x1c])
                                                .concat(cp866encode("0"))
                                                .concat([0x1c]);

                                            return exec(strM).then((data) => {
                                                let cashbox = cp866decode(data.slice(5, data.indexOf(0x1c)));
                                                log("Номер кассы: " + cashbox);
                                            });
                                        }

                                        function print(text) {
                                            log("Печать строки: " + JSON.stringify(text));

                                            const strM = [0x29]                                     // ID пакета
                                                .concat(cp866encode("40"))                          // печать текста
                                                .concat(text)                                       // текст
                                                .concat([0x1c])
                                                .concat(cp866encode((0b00000001).toString(16)));    // шрифт 13х24

                                            return exec(strM);
                                        }

                                        function printarr(arr) {
                                            return arr.reduce((prev, e, index) => {
                                                return prev.then(() => print(e));
                                            }, Promise.resolve());
                                        }

                                        function qr() {
                                            log("Печать QR-кода");

                                            const zero = cp866encode("0");
                                            const strM = [0x30]                             // ID пакета
                                                .concat(cp866encode("41"))                  // печать штрих-кода
                                                .concat(zero)                               // опции вывода, для QR-кода не используется
                                                .concat([0x1c])
                                                .concat(cp866encode("6"))                   // ширина штрих-кода (так получается 30мм)
                                                .concat([0x1c])
                                                .concat(zero)                               // высота штрих-кода, для QR-кода не используется
                                                .concat([0x1c])
                                                .concat(cp866encode("8"))                   // тип штрих-кода
                                                .concat([0x1c])
                                                .concat(cp866encode(receipt.egais1.url))
                                                .concat([0x1c]);

                                            return exec(strM);
                                        }

                                        function closedoc() {
                                            log("Закрываем документ");

                                            const strM = [0x31]
                                                .concat(cp866encode("31"));                 // закрытие документа

                                            return exec(strM)
                                        }

                                        function doPrint() {
                                            log("печать чека УТМ");

                                            const ll = 40;
                                            const tail = kkm.egais.tail(cp866encode, receipt.egais1, ll);

                                            return opendoc().
                                                then(() => readcashbox()).
                                                then((cashbox) => kkm.egais.head(cp866encode, receipt.egais1, 1, ll)).
                                                then((head) => printarr(head)).
                                                then(() => qr()).
                                                then(() => printarr(tail)).
                                                then(closedoc);
                                        }

                                        return doPrint();
                                    }

                                    function regdiscount() {
                                        log("скидка");

                                        const strD = convIntToInt(receipt.discount / 100);
                                        const strM = [0x45, 0x34, 0x35, 0x31, 0x1C, 0x1C].concat(strD);

                                        return exec(strM);
                                    }


                                    (typeof receipt.discount != "undefined" ? regdiscount() : Promise.resolve()).
                                    then(receipt.noCashSum ? () => printSum(receipt.noCashSum, 0x31) : Promise.resolve()).
                                    then(receipt.paidSum ? () => printSum(receipt.paidSum, 0x30) : Promise.resolve()).
                                    // если нет суммы ни налом ни безналом - отправляем 0 налом в ФР
                                    then((!receipt.paidSum && !receipt.noCashSum)? () => printSum(0, 0x30) : Promise.resolve()).
                                    then(() => closeReceipt()).
                                    then(receipt.egais1 ? () => egaisprint() : Promise.resolve()).
                                    then(() => {
                                            close();
                                            if (onfinish)
                                                onfinish();
                                        });
                                });
                                send(p([0x44, 0x34, 0x34]), function() {
                                    log("предварительный итог");
                                });
                            });
                        });
                        const enccashier = !!receipt.cashier ? cp866encode(receipt.cashier) : [0x8A, 0xA0, 0xE1, 0xE1, 0xA8, 0xE0];
                        send(p([0x30, 0x33, 0x30, rtype(), 0x1C, 0x31, 0x1C].concat(enccashier).concat([0x1C, 0x30, 0x1C])), function() {}); // открыть чек продажи/возврата
                    };
                }
            })).
            then(function () { return new Promise(function (next) { if (toprint instanceof Function) toprint(next); else next(); }); }).
            then(function () { onfinish(); log("...print receipt finished"); }).
            catch(function (e) { close (function () { err(e.message || e); }); }); // обработка ошибок при печати чека
        }

        app(function (send, receive, close) {
            printReceipt(send, receive, close, receipt, onfinish);
        });
    }

    function printReport(send, receive, close, onfinish, repcode, cashier) {
        const PU = kkm.pirit.utils;

        receive(function (data) {
            var m10 = new Array();
            m10 = getPiritStatus(data);

            log("1="+m10[0]+"  2="+m10[1]+ "   3="+m10[2]);
            f1 = m10[0];
            f2 = m10[1];
            f3 = m10[2];

            var errNum = PU.errcode(data);

            if (errNum != 0) log("Error from FR err:"+errNum);
            if (f1 != 0) {
                frerror(f1);
            } else if ((f2 & 1) != 0) {
                PU.commandStart(send,receive, close, config, function(){printReport(send, receive, close, onfinish, repcode, cashier);});
            } else if ((f2 & 4) == 0){
                err("Смена не открыта");
                close();
            } else if (f3 != 0){
                receive(function (data){
                    printReport(send, receive, close, onfinish, repcode, cashier);
                });
                send(p([0x32, 0x33, 0x32]), function() {
                    log("Error from FR B3:"+f3);
                });
            } else {
                receive(function (data) {
                    close();
                    if (onfinish)
                        onfinish();
                });
                const enccashier = !!cashier ? cp866encode(cashier) : [0x8A, 0xA0, 0xE1, 0xE1, 0xA8, 0xE0];
                send(p([0x21, 0x32, repcode].concat(enccashier)), function() {});
            }
            log("report...........................................END");
        });
        send(p([0x81, 0x30, 0x30]), function() {
            log("report.............................................Start");
        });

    }

    function z(data, onfinish) {
        app(function(send, receive, close) {
            printReport(send, receive, close, onfinish, 0x031, data);
        });
    }

    function x(data, onfinish) {
        app(function(send, receive, close) {
            printReport(send, receive, close, onfinish, 0x030, data);
        });
    }

    function change(data, onfinish) {
        if (onfinish && onfinish instanceof Function) onfinish();
    }

    function cashopmaker(opcode, data) {
        return function (datas, onfinish) {
            function printCashop(send, receive, close, onfinish) {
                const PU = kkm.pirit.utils;
                const l = PU.link(send, receive, config);

                log("проверка состояния ФР перед "+ (opcode == 0x34 ? "внесением" : "выплатой"));
                receive(function (data) { // подтверждение проверки состояния фр перед внесением
                    log("получили состояние");
                    var m10 = new Array();
                    m10 = getPiritStatus(data);

                    f1 = m10[0];
                    f2 = m10[1];
                    f3 = m10[2];

                    var errNum = PU.errcode(data);

                    if (errNum != 0) log("Error from FR err:"+errNum);
                    if (f1 != 0) {
                        log("Error from FR B1:"+f1);
                        if ((f1 &   1) != 0) e("Неверная контрольная сумма NVR");
                        if ((f1 &   2) != 0) e("Неверная контрольная сумма в конфигурации");
                        if ((f1 &   4) != 0) e("Ошибка интерфейса с ФП");
                        if ((f1 &   8) != 0) e("Неверная контрольная сумма фискальной памяти");
                        if ((f1 &  16) != 0) e("Ошибка записи в фискальную память");
                        if ((f1 &  32) != 0) e("Фискальный модуль не авторизован");
                        if ((f1 &  64) != 0) e("Фатальная ошибка ЭКЛЗ");
                        if ((f1 & 128) != 0) e("Расхождение данных ФП и ЭКЛЗ");
                    } else if ((f2 & 8) != 0) {
                        err("Смена больше 24 часов");
                        close();
                    }
                    else if (f3 != 0){
                       err("В ККТ есть открытый документ");
                       close();
                    }
                    else {
                        log("открываем документ "+ (opcode == 0x34 ? "внесения" : "выплаты"));
                        receive(function (data) { // подтвержение открытия документа внесения
                            log("отправляем сумму "+ (opcode == 0x34 ? "внесения" : "выплаты"));
                            receive(function (data) { // подтвержение отправки внесения
                                log("закрываем документ "+ (opcode == 0x34 ? "внесения" : "выплаты"));
                                receive(function (data) { // подтверждение закрытия документа внесения
                                    log(0x34 == opcode ? "внесение завершено": "выплата завершена");
                                    close(onfinish);
                                });
                                send(p([0x31, 0x33, 0x31, 0x30, 0x1C])); // закрытие документа внесения/выплаты
                            });
                            const sum = !!datas.sum ? datas.sum : datas;
                            send(p([0x48, 0x34, 0x38, 0x1C].concat(convIntToInt(sum/100.0)))); // отправка внесения/выплаты
                        });
                        const enccashier = !!datas.cashier ? cp866encode(datas.cashier) : [0x8A, 0xA0, 0xE1, 0xE1, 0xA8, 0xE0];
                        send(p([0x30, 0x33, 0x30, opcode, 0x1C, 0x31, 0x1C].concat(enccashier).concat([0x1C, 0x30, 0x1C]))); // открытие документа внесения/выплаты
                    }
                });
                send(p([0x81, 0x30, 0x30])); // проверка состояния фр перед внесением/выплатой
            }

            app(function (send, receive, close) {
                printCashop(send, receive, close, onfinish);
            });
        }
    }

    function cashin(data, onfinish)
    {
      cashopmaker(0x34, data)(data, onfinish);
    }

    function cashout(data, onfinish)
    {
      cashopmaker(0x35,data)(data, onfinish);
    }

    function ofdreport(data, onfinish) {
        // pirit not supported yet
        onfinish();
    }

    proc(beep, demotext, info, type, check, print, z, x, change, cashin, cashout, ofdreport);
 }
