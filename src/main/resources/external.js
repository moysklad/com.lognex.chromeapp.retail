window.onload = () => {
	const ol = document.getElementById("outer-link");
	h = window.location.href;
	const src = unescape(h.substr(h.indexOf('?')+1))
	ol.src = src;


	resize = (function () {
        ol.style.height = "100%";
	})();

	window.onresize = function () {
	    resize();
	};

	ol.addEventListener('permissionrequest', function(e) {
	    e.request.allow();
	});
};