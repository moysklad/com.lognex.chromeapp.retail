kkm.serial.listener = {};
kkm.serial.err = {};

kkm.serial.request = function (logging, config, client, confail, oncomplete) {
	//kkm.util.incl(kkm.util.modlog(logging(), "........."));
	const U = kkm.util;
	const L = U.modlog(logging(), ".........");

	const log = L.log;
	const err = L.err;
	
	function logconn(ci) {
		log("-----------------------");
		log("connection id: "+ci.connectionId);
		log("bufferSize: "+ci.bufferSize);
		log("receiveTimeout: "+ci.receiveTimeout);
		log("sendTimeout: "+ci.sendTimeout);
		log("bitrate: "+ci.bitrate);
		log("-----------------------");
	}

	function connect (config, connected, reqfail) {
		function c(cd) {
			chrome.serial.connect(config.id, config.comprefs(), function (ci) {
				const le = chrome.runtime.lastError;
				if (le || (!ci || !ci.connectionId)) {
					log("connection to port "+config.id+" failed with "+le.message);
					if (0 == cd) {
						confail();
						reqfail();
						throw new Error("Ошибка при подключении к порту "+config.id);
					}
					log("retrying");
					setTimeout(function() { c(cd - 1); }, 2000);			

				} else {
					logconn(ci);
					connected(ci.connectionId);
				}
			});
		}
		c(3);
	}

	function send(conn, data, sent) {
		chrome.serial.send(conn, data, sent);
	}

	function flush(conn, flushed) {
		chrome.serial.flush(conn, flushed);
	}

	const id = function (info) { return info.connectionId; };

	function listener_add(conn, listener, adder) {
		adder(
			kkm.serial.listener,  // listeners
			function (l) { chrome.serial.onReceive.addListener(l); }, // addlistener()
			id, // id()
			function(info, listener) { listener(info.connectionId, info.data); }, // caller()
			"no serial listener for connection " // errmesg
		)(conn, listener);
	}

	function err_add(conn, listener, adder) {
		adder(
			kkm.serial.err,
			function (l) { chrome.serial.onReceiveError.addListener(l) },
			id,
			function (info, listener) { listener(info); },
			"no serial error listener for connection "
		)(conn, listener);
	}

	function close(conn, closed) {
		chrome.serial.disconnect(conn, closed);
	}

	function net(proc) {
		proc(connect, send, flush, listener_add, err_add, close);
	}

	kkm.request(logging, net, config, client, oncomplete);
 }

/**
*
* обход всех COM-портов в системе
*
* util - вспомогательные функции
*
* config - базовая конфигурация обращения к COM-порту
*
* proc - функция, вызываемая для каждого порта, вида proc(confchange),
* где confchange - модификатор общей конфигурации под конкретный порт
**/
kkm.serial.enum = function (logging, config, count, proc) {
	const L = kkm.util.modlog(logging(), ".........");
	const log = L.log;
	const err = L.err;

	try {
		chrome.serial.getDevices(function (ports) {
			count(ports.length);
			for (var i=0; i<ports.length; i++) 
				try {
					proc(function (config) {
						const conf = config();
						conf.id = ports[i].path;
						conf.name = ports[i].displayName;
						conf.vendor = ports[i].vendorId;
						conf.product = ports[i].product;
						return conf;
					});
				} catch (e) { err(e.message); }
		});
	} catch (e) { err(e.message); }
};