const ENQ = 0x05; // запрос
const ACK = 0x06; // подтверждение
const STX = 0x02; // флаг начала блока команды или ответа
const ETX = 0x03; // флаг конца блока команды или ответа
const EOT = 0x04; // конец передачи
const DLE = 0x10; // \кранирование управляющих символов
const NAK = 0x15; // отрицание

Uint8ClampedArray.prototype.join = function(sep) { // добавляем к Unit8ClampedArray родную для обычных Array функцию join
	if (! length) return "empty";
	if (1 == this.length) return this[0];

	if (undefined == sep) sep = ",";

	var res = this[0];
	for (var i=1; i<this.length; i++)
		res += sep + this[i];
	return res;
};

function hx(i) {
	return (i||0).toString(16);
}

function int2bytes (i, arr) { // заполняем младшие 4 значения массива байтами из значения int
	const ab = new ArrayBuffer(4);
	const int32view = new Uint32Array(ab);
	const byteview = new Uint8ClampedArray(ab);

	int32view[0] = i;

	var res = arr ? arr : [];
	res[0] = byteview[0];
	res[1] = byteview[1];
	res[2] = byteview[2];
	res[3] = byteview[3];

	return res.reverse();
}

function bytes2int (bytes) { // из 4 младших значений массива формируем значение int
	var res = bytes[3];
	res += bytes[2] << 8;
	res += bytes[1] << 16;
	res += bytes[0] << 24;

	return res;
}

function short2bytes(s) {
	const a = int2bytes(s, [0,0,0,0]);
	return [a[3], a[2]];
}

function int2BCDbytes (val, arr) { // преобразуем значение int в двоично-десятичное в байтовом массиве
	var b = val, i=0;
	while(0 < b) {
		var c = b % 100;
		arr[i++] = (c%10) + (16*(c/10|0));
		b = (b/100)|0;
	}
	return arr.reverse();
}

function fromBCD (bytes) { // преобразуем двоично-десятичное значение в байтовом массиве в обычный int
	var res = 0;
	for (var i=0; i<4; i++) {
		res += (bytes[3-i]%16) * Math.pow(100, i);
		res += (((bytes[3-i]/16)|0)*10) * Math.pow(100, i);
	}

	return res;
}

function encode(cp, str) { // кодируем строку в байтовый массив по таблице перекодировки
	function code(char) {
		const res = cp.indexOf(char);
		return (-1 == res ? 0x20 : res);
	}

	const bytes = [];
	for (var i=0;i<str.length;i++)
		bytes[i] = code(str[i]);

	return bytes;
}

// кодировка DOS в виде строки, позиция символа в строке == код символа
function cp866() {
	return (
		"␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏"+//0 ASCII
		"␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟"+//1 ASCII
		" !\" №%&'()*+,-./"+//2 ASCII
		"0123456789:;<=>?"+//3 ASCII
		"@ABCDEFGHIJKLMNO"+//4 ASCII
		"PQRSTUVWXYZ    -"+//5 ASCII
		" abcdefghijklmno"+//6 ASCII
		"pqrstuvwxyz{|}~ "+//7 ASCII
		"АБВГДЕЖЗИЙКЛМНОП"+//8 CP-866
		"РСТУФХЦЧШЩЪЫЬЭЮЯ"+//9 CP-866
		"абвгдежзийклмноп"+//A CP-866
		"                "+//B CP-866
		"                "+//C CP-866
		"                "+//D CP-866
		"рстуфхцчшщъыьэюя"+//E CP-866
		"Ёё              " //F CP-866
	);
}

// кодировка DOS в виде строки, позиция символа в строке == код символа
function cp866_pirit() {
	return (
		"␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏"+//0 ASCII
		"␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟"+//1 ASCII
		" !\" $%&'()*+,-./"+//2 ASCII
		"0123456789:;<=>?"+//3 ASCII
		"@ABCDEFGHIJKLMNO"+//4 ASCII
		"PQRSTUVWXYZ    -"+//5 ASCII
		" abcdefghijklmno"+//6 ASCII
		"pqrstuvwxyz{|}~ "+//7 ASCII
		"АБВГДЕЖЗИЙКЛМНОП"+//8 CP-866
		"РСТУФХЦЧШЩЪЫЬЭЮЯ"+//9 CP-866
		"абвгдежзийклмноп"+//A CP-866
		"                "+//B CP-866
		"                "+//C CP-866
		"                "+//D CP-866
		"рстуфхцчшщъыьэюя"+//E CP-866
		"Ёё          №   " //F CP-866
	);
}

// кодировка WIN в виде строки, позиция в строке == код символа
function win1251() {
	return (
		"␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏"+//0 ASCII
		"␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟"+//1 ASCII
		" !\"  %&'()*+,-./"+//2 ASCII
		"0123456789:;<=>?"+//3 ASCII
		"@ABCDEFGHIJKLMNO"+//4 ASCII
		"PQRSTUVWXYZ    -"+//5 ASCII
		" abcdefghijklmno"+//6 ASCII
		"pqrstuvwxyz{|}~ "+//7 ASCII
		"                "+//8 WIN 1251
		"                "+//9 WIN 1251
		"        Ё       "+//A WIN 1251
		"        ё№      "+//B WIN 1251
		"АБВГДЕЖЗИЙКЛМНОП"+//C WIN 1251
		"РСТУФХЦЧШЩЪЫЬЭЮЯ"+//D WIN 1251
		"абвгдежзийклмноп"+//E WIN 1251
		"рстуфхцчшщъыьэюя" //F WIN 1251
	);
}


function cp866encode (str) { // кодируем строку в байтовый массив со значениями в DOS-кодировке
	return encode(cp866(), str);
}

function cp866encode_pirit (str) { // кодируем строку в байтовый массив со значениями в DOS-кодировке, в пиритах таблица кодировки отличается от атола
	return encode(cp866_pirit(), str);
}

function win1251encode(str) { // кодируем строку в байтовый массив со значениями в WIN-кодировке
	return encode(win1251(), str);
}

function decode(cp, bytes) { // из байтового массива раскодируем строку по таблице кодировки
	const res = [];
	for (var i=0; i<bytes.length; i++)
		res[i] = cp[bytes[i]];
	return res.join("");
}

function cp866decode (bytes) { // раскодируем байтовый массив в символами в кодировке DOS в строку
	return decode(cp866(), bytes);
}

function win1251decode (bytes) { // раскодируем байтовый массив с символами в кодировке WIN в строку
	return decode(win1251(), bytes);
}

function charencode(str) {
    const res = [];
    for (var i=0; i<str.length;i++)
        res[i] = str.charCodeAt(i);
    return res;
}

function chardecode(arr) {
    const res = [];
    for (var i=0; i<arr.length; i++)
        res[i] = String.fromCharCode(arr[i]);
    return res.join("");
}

function fillarr(ch, len) { // создаем массив длины len заполненый символами ch
	const res = new Array(len);
	for (var i=0;i<len;i++)
		res[i] = ch;
	return res;
}

function zeroarr(len) { // создаем массив длины len заполненый нулями
	return fillarr(0, len);
}

function cutbytes(bytes, cut) { // приводим массив к заданой длине, обрезаем если он длиннее, дополняем нулями если короче
	if (cut <= bytes.length)
		return bytes.slice(0,cut);
	return bytes.concat(zeroarr(cut-bytes.length));
}

function splitbytes(bytes, len) { // разбиваем массив на массивы длиной не больше len
	const res = [];
	for (var i=0; i*len+len<bytes.length; i++)
		res[i] = bytes.slice(i*len, i*len+len);
	res.push(bytes.slice(i*len, bytes.length));
	return res;
}

function squinter(left, right, len) { // растягивает строки left и right по краям объединяющей строки длинны len, в середине пробелы
	const lr = left.length  + right.length;
	return lr > len ? left + right.substr(lr - len) : left + fillarr(" ", len - lr).join("") + right;
}

function center(str, ll, ident) { // помещает строку str в центр строки длинной ll, заливая края символом ident
	const cut = str.substr(0, ll);
	const i = fillarr(ident, ((ll - cut.length) / 2)|0).join('');
	const res = i+cut+i;
	return res + (ll > res.length ? ident : '');
}

// первый встречный элемент e в массиве b длиной l
function firstIndex(b, l, p) {
	for (var i=0; i<l; i++)
		if (p(i, b[i])) return i;
	return -1;
}

// последний встречный элемент e в массиве b длинной l
function lastIndex(b, l, p) {
	var i=l; while(--i>=0)
		if (p(i, b[i]))
			return i;
	return -1;
}

kkm.convert = true; // mark of availability
