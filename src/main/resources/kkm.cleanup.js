kkm.cleanup.clean = (function () { const selfclean = function () { kkm.cleanup.clean = selfclean; }; return selfclean; })();

kkm.cleanup.add = function (clean) {
	const preclean = kkm.cleanup.clean;
	kkm.cleanup.clean = function() {
		preclean();
		clean();
	};
}