kkm.atol.command = function (config, client, logging, conn, send1, receive1, close, flush) {
	const L = kkm.util.modlog(logging(), '...');
	const log = L.log;
	const err = L.err;
	const bytechecker = kkm.util.bytecheck;

	function esc(data) {
		var i = -1;
		while (++i < data.length)
			if (ETX == data[i] || DLE == data[i])
				data.splice(i++, 0, DLE);
	}

	function unesc (data) {
		var i = 0;
		while(i++<data.length)
			if (data[i] == DLE)
				data.splice(i,1);
		return data;
	}

	function pack(data) { // запаковка команды АТОЛ
		esc(data);
		data.push(ETX);
		data.push(kkm.util.crc(data));
		data.unshift(STX);

		return data;
	}

	function shft(b, l) {
		for (var i=0; i<l; i++) {
			if (STX == b[i])
				if (0 == i || DLE != b[i-1])
					return i;
		}
		return -1;
	}

	function unpack(data) { // распаковка ответа от АТОЛ
		const sh = shft(data, data.length);
		if (-1 == sh)
			throw new Error ("no STX found");
		const d = data.slice(sh);
		d.shift();
		if (ETX != d[d.length - 2])
			throw new Error("no ETX found");
		d.pop();
		d.pop();
		return unesc(d);
	}

	function receivedpacket(b, l) {
		log("checking for packet data");
		const sh = shft(b, l);
		log("package shift is "+sh);
		// начинаться пакет должен с STX, длиной быть не меньше 3 символов, завершаться должен символом ETX, при этом этот ETX не должен быть экранирован символом DLE
		return (0 <= sh) && (3 < (l-sh) && STX == b[sh] && (ETX == b[l-2] && DLE != b[l-3]));
	}

	function r(conn, check, callback) {
		receive1(conn, check, function (d) {
			flush(conn, function (){
				callback(d);
			});
		}) 
	}
    
    function sendN(conn, bytes, times, timeout, check, callback) {
        var done = false;
        r(conn, check, function(d) {
            done = true;
            callback(d);
        });
        function attempt(num) {
            if (done) return;
            if (num > times) {
                err("failed sending ["+bytes.map(hx)+"] in "+times+" attempts");
                return;
            }
            log("sending ["+bytes.map(hx)+"], attempt "+num);
            send1(conn, bytes, function() {
                log("attempt "+num+" sent");
                setTimeout(function() { attempt(num + 1); }, timeout);
            });
        }
        
        attempt(1);
    }

	function send (sdata, sentlink) { // отправка команды АТОЛ
		function checkinit(b) {
			if (ACK == b) {
				log("send init confirmed");
				return true;
			}
			if (ENQ == b || NAK == b) 
				send(sdata, sentlink);
			else
				log("incorrect send init, "+b+" insteed of "+ACK);
			return false;
		}

		function checkconfirm(b) {
			if (ACK == b) {
				log("send confirmed");
				return true;
			}
			if (EOT == b || NAK == b)
				send(sdata, sentlink);
			else
				log("incorrect send confirmation, "+b+" insteed of "+ACK);
			return false;
		}

		sendN(conn, [ENQ], 5, 500, bytechecker(), function(d) {
			if (!checkinit(d[0]))
				return;

			log("send pre-confirmed");
			r(conn, bytechecker(), function(d) {
				if (!checkconfirm(d[0]))
					return;

				send1(conn, [EOT], function (si) {
					if (!si || si.error)
						log("failed complete sending data, "+chrome.runtime.lastError.messsage);
					else {
						log("sending completed");
						sentlink(send, receive);
					}
				});
			});
			send1(conn, pack(sdata), function(si) {
				log((!si || si.error) ? "failed sending data, "+chrome.runtime.lastError.message : "data sent!");
			});
		});
	};

	function receive(receivedlink) { // получение сообщение АТОЛ
		function check(d) {
			if (ENQ == d[0]) return true;
			if (ACK == d[0]) {
				send1(conn, [ACK], function () {
					receive(receivedlink);
				});
				return false;
			}
			log("incorrect recieve init, "+d[0]+" insteed of "+ENQ);
			return false;
		}

		function checksend(si, fail, ok) {
			if (!si || si.error)
				log(fail+", "+chrome.runtime.lastError.message);
			else
				log(ok);
		}

		r(conn, bytechecker(), function(d) {
			if (!check(d))
				return;

			log("receive initted");
			r(conn, receivedpacket, function(d) {
				const data = unpack(d);
				log("data received! received data size: "+data.length);

				(function finish() {
					r(conn, bytechecker(), function(d) {
						if (EOT != d[0])
							finish();
						else {
							log("receive finished");
							receivedlink(data, send, receive);
						}
					});
				})();
				send1(conn, [ACK], function(si) {
					checksend(si, "failed sending post-receive confirmation", "data receive confirmed");
				});
			});
			send1(conn, [ACK], function (si) {
				checksend(si, "failed sending pre-receive confirmation", "receive pre-confirmation sent");
			});
		});
	}

	client(send, receive, close);
}