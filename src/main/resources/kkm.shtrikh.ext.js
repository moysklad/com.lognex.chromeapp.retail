kkm.shtrikh.ext = function (logging, config, executor, proc) {
	kkm.shtrikh.api(logging, config, executor, function (beep, demo, info, type, check, print, z, x, change, cashin, cashout, wait4finish) {
		const U = kkm.util;

		const L = U.modlog(logging(), "...");

		const log = L.log;
		const err = L.err;

		function app(chain) {
			executor(logging, chain);
		}

		const checkext = function (res, checked) {
			const datas = [];
			app(
				function (send, receive, close) {
					function t() {
						receive(function (data) {
							log(data[1] ? "error "+conf.errtable[data[1]] : "type data received");
							datas.push(data);
							res.model = win1251decode(data.slice(8, data.length));
							i();
						});
						send([0xfc], function() {
							log("type request sent!");
						});
					}

					function i() {
						receive(function (data) {
							log("info data received!");
  						kkm.shtrikh.cashbox = data[10];
              log("Cashbox is: " + kkm.shtrikh.cashbox);
							datas.push(data);
							if(data[3] && data[4]) res.ver = " v. "+win1251decode([data[3], data[4]]).split("").join(".");
							else res.ver = "";		
							res.fiscal = (data[2] & 32);
				      res.doc = bytes2int([0, 0, data[11], data[12]]);
				      res.receipt = 0;
				      res.change = bytes2int([0, 0, data[36], data[37]])+1;
				      res.serial = bytes2int([data[35], data[34], data[33], data[32]]);
							q();
						});
						send([0x11].concat(config.operpass), function() {
							log("info request sent!");
						});
					}

					function q() {
						receive(data => {
							// проверяем возможность печати QR-кода на устройстве
							// шлем команду печати кода, размер точки указываем 0
							// если код ошибки 0x33 (51, Некорректные параметры в команде)
							// то считаем, что устройство команду поддерживает
                            res.qrsupported = (data[1] == 0x33);
							log("QR code test ended, result: " + res.qrsupported);
                            fz();
						});

						send([0xde].concat(config.operpass).concat([
                            0x03,						// тип штрихкода (QR)
                            0x00,						// длина данных
                            0x00,						// номер начального блока данных
                            0x00,						// версия (авто)
                            0x00,
                            0x00,
                            0x00,						// размер точки (недопустимое значение)
                            0x00,
                            0x02,						// уровень коррекции ошибок
                            0x01						// выравнивание (по центру)
						]), () => log("QR code test started"));
					}

					// определяем ФЗшный ли Штрих, отправляя запрос состояния ФН, если ответ без ошибок - значит Штрих ФЗшный
					function fz() {
						receive((data) => {
							res.fz54 = data.length > 2 && (0xff == data[0] && 0x01 == data[1]);
							res.vendor = res.fz54 ? "shtrikh54fz" : "shtrikh";
							datas.push(data);
							log("Получен ответ на запрос статуса ФН, ФР Штрих "+(res.fz54?"":"не ")+"поддерживает ФЗ-54");
							close(function () { checked(res, datas); });
						});
						send([0xff, 0x01].concat(config.adminpass), function() {
							log("Запрос статуса ФН отправлен");
						})
					}

					t();
				}
			);
		}

		proc(beep, demo, info, type, checkext, print, z, x, change, cashin, cashout, wait4finish, /*ofdreport*/U.cbpass, /*corrprint*/U.cbpass, /*hackfr*/U.cbpass);
	});
}
