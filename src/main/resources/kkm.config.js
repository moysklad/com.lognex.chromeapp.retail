kkm.config.default = function () {
	function comprefs() {
		return {
			"persistent":false,
			"name":"KKM",
			"bufferSize": 512,
			"bitrate": kkm.config.default.bitrate,
			"dataBits": "eight",
			"parityBit": "no",
			"stopBits": "one",
			"ctsFlowControl": false,
			"sendTimeout": 60000
		};
	}

	function atol() {
		return {
			"vendor": "atol",
			"id": this.id,
			"comprefs": this.comprefs,
			"password": [0, 0],
			"operpass": [0x00, 0x00, 0x00, 0x04],
			"adminpass": [0x00, 0x00, 0x00, 0x30],
			"timeout": this.timeout,
			"cmdtimeout": this.cmdtimeout,
			"errtable": kkm.atol.errors()
		};

	}

	function atol54fz() {
		return {
			"vendor": "atol54fz",
			"id": this.id,
			"comprefs": this.comprefs,
			"password": [0, 0],
			"operpass": [0x00, 0x00, 0x00, 0x04],
			"adminpass": [0x00, 0x00, 0x00, 0x30],
			"timeout": this.timeout,
			"cmdtimeout": this.cmdtimeout,
			"retries":5,
			"errtable": kkm.atol54fz.errors()
		};

	}

	function shtrikh() {
		return {
			"vendor": "shtrikh",
			"id": this.id,
			"comprefs": this.comprefs,
			"password": [0x00, 0x00],
			"operpass": [0x01, 0x00, 0x00, 0x00],
			"adminpass": [0x1e, 0x00, 0x00, 0x00],
			"timeout": this.timeout,
			"cmdtimeout": this.cmdtimeout,
			"porttimeout": this.porttimeout,
			"retries": this.retries,
			"errtable": kkm.shtrikh.errors(),
			// модели не поддерживающие отрезку чека
			"nocutmodels":[
					0, // "ШТРИХ-500"
					6, // "ЭЛЕВС-ФР-К", "АСПД ЭЛВЕС-ПРИНТ"
					19 // "Штрих-Mobile-ПТК"
				]
		};
	}

	function shtrikh54fz() {
	    const conf = shtrikh.apply(this);
	    conf.vendor = "shtrikh54fz";
	    return conf;
	}

	function atol_bluetooth() {
		return {
			"vendor": "atol_bluetooth",
			"id": this.id,
			"name": this.name,
			"password": [0, 0],
			"operpass": [0x00, 0x00, 0x00, 0x04],
			"adminpass": [0x00, 0x00, 0x00, 0x30],
			"timeout": this.timeout,
			"cmdtimeout": this.cmdtimeout,
			"porttimeout": this.porttimeout,
			"retries": this.retries,
			"errtable": kkm.atol.errors()
		};
	}

    function pirit() {
        return {
            "vendor": "pirit",
            "id": this.id,
            "comprefs": comprefs,
            "password": [0x50, 0x49, 0x52, 0x49],
            "operpass": [0x50, 0x49, 0x52, 0x49],
            "adminpass": [0x50, 0x49, 0x52, 0x49],
            "timeout": this.timeout,
            "cmdtimeout": this.cmdtimeout,
            "porttimeout": this.porttimeout,
            "receiveTimeout": 505,
            "sendTimeout": 500,
            "retries": 5,
            "errtable": kkm.pirit.errors()
        };
    }

    function pirit54fz() {
        const conf = pirit.apply(this);
        conf.vendor = "pirit54fz";
        return conf;
    }


    const res = {
        "id": false,
        "comprefs": comprefs,
        "timeout": 80000,
        "cmdtimeout":15000,
        "porttimeout":500,
        "retries":7,
    };

   	function own(f) {
   		const r = res;
   		return function () {
   			return f.apply(r);
   		}
   	}

    res.atol = own(atol);
    res.atol54fz = own(atol54fz);
    res.shtrikh = own(shtrikh);
    res.shtrikh54fz = own(shtrikh54fz);
    res.pirit = own(pirit);
    res.pirit54fz = own(pirit54fz);
    res.atol_bluetooth = own(atol_bluetooth);

    return res;
};
kkm.config.default.bitrate = 9600;
kkm.config.default.demotext ="Тестовая печать!";
