kkm.shtrikh54fz.ext = function (logging, config, executor, proc) {
	kkm.shtrikh.ext(logging, config, executor, function (beep, demo, info, type, check, print, z, x, change, cashin, cashout, wait4finish, ofdreport, corrprintpass, hackfr) {
		const U = kkm.util;

		const L = U.modlog(logging(), "...");

		const log = L.log;
		const err = L.err;

		const assert = U.assertmaker(config.errtable, err);

		const X = kkm.ext;

		function app(chain) {
			executor(logging, chain);
		}

		// отрезка чека
		function cutreceipt(stalk) {
			return  stalk([0xfc], "Ошибка при получении типа устройства").
					then(function(data) {
						const model = data[6];
						return config.nocutmodels.includes(model) ?
							Promise.resolve() :
							stalk([0x25].concat(config.operpass).concat([1]), "Ошибка отрезки чека");
					});
		}

		function e0(code) {
		    const msg = config.errtable[code] || "Ошибка при открытии смены ("+code+")";
		    const e = new Error(msg);
		    e.code = 001;
		    return e;
		}

		function corrprint(corrreceipt, onfinish)
		{
			var model;
			function check(data, fail, drop, open) {
				const modes = {
					1: "Выдача данных",
					5: "Блокировка по неправильному паролю налогового инспектора",
					6: "Ожидание подтверждения ввода даты",
					7: "Разрешение изменения положения десятичной точки",
					9: "Режим разрешения технологического обнуления",
					10: "Тестовый прогон.",
					11: "Печать полного фис. отчета",
					12: "Печать отчёта ЭКЛЗ",
					13: "Работа с фискальным подкладным документом",
					14: "Печать подкладного документа"
				};

				switch (true) {
/*					case (!(data[2] & 2)):
						return fail("Закончилась бумага");*/
					case (4 == data[5]): // смена закрыта, открываем
						return open();
					case (8 == data[5] || 40 == data[5]): // открыт чек, закрываем
						return drop();
					case (3 == data[5]):
						return fail("Ошибка при печати кассового чека. Смена превысила 24 часа, для продолжения работы необходимо снять Z-отчет");
					case (0 == data[5] || 2 == data[5] || 15 == data[5]):
						return Promise.resolve();
					default: return Promise.reject("Печать чека невозможна в режиме(подрежиме): "+(modes[data[5]] || data[5])+"("+data[6]+")");
				}
			}

			app(
				function (send, receive, close) {
					const talk = U.talkmaker(send, receive, log);
					const stalk = U.safetalkmaker(send, receive, close, log, assert);

					const setreq = setreqmaker(stalk);


					function fillmodel(withmodel) {
						log("получаем модель");
						return stalk([0xfc], "Ошибка при получении типа устройства").
						then(function (data) {
							model = data[6];
							log("получили модель Штриха ("+model+")");
							return Promise.resolve();
						});
					}

					function open() {
						return talk([0xE0].concat(config.operpass)).
						then(function (data) {
						    if (data[1]) throw e0(data[1]);
						    else {
							    log("ждем когда смена откроется");
							    return wait4finish(talk);
							}
						}).then(function () {
							log("Смена открыта");
							return Promise.resolve();
						}).
						catch (function(e) {
						    close(function() { err(e.message || e, e.code); });
						});
					}

					function drop() {
						return stalk([0x88].concat(config.operpass), "Ошибка аннулирования незакрытого чека").
						then(function () {
							log("предыдущий не закрытый чек аннулирован");
							return new Promise(function(r) {
								setTimeout(r, 200);
							});
						});
					}

					function fail(msg) {
						return Promise.reject(new Error(msg));;
					}

					fillmodel(). // выясняем с какой моделью ФР работаем
					then(function () { // запрашиваем информацию о состоянии ФР
						log("получаем состояние ФР");
						return talk([0x10].concat(config.operpass));
					}).
					then(function (data) { // проверяем, готов ли ФР к печати
						log("проверяем готовность ФР")
						return check(data, fail, drop, open);
					}).
					// устанавливаем имя кассира в таблице в ФР для печати на чеке и отправки в ОФД
					then(setcashiermaker(corrreceipt.cashier, stalk, log, 30)).
					then(function () { // печатаем чек коррекции
						log("печатаем чек коррекции");
						const cashsum = int2bytes(corrreceipt.cashsum, [0,0,0,0,0]).reverse();
						const nocashsum = int2bytes(corrreceipt.nocashsum, [0,0,0,0,0]).reverse();
						const sum = int2bytes(corrreceipt.nocashsum+corrreceipt.cashsum, [0,0,0,0,0]).reverse();
						return stalk([0xff,0x4a]
						.concat(config.adminpass).concat(corrreceipt.type).concat(corrreceipt.op)
						.concat(sum).concat(cashsum).concat(nocashsum).concat(zeroarr(46)),
						"Ошибка при печати чека коррекции");
					}).
					/*then(function () { // отрезаем чек
						log("отрезка чека");
						return cutreceipt(stalk);
					}).*/
					then(function () { // закрываем соединение
						log("отключаемся");
						return new Promise(function(r) { close(r); });
					}).
					then(onfinish). // передаем управление наружу
                    catch(e => {
                        close(function () { err(e.message || e); });
                    }); // обработка ошибок внутри цикла печати
				}
			);
		}

		function printext(receipt, onfinish) {
			const linelengths = {
				250:57, // Штрих-М-ФР-К
				7:50, // Штрих-Мини-ФР-К
				4:36, // Штрих-ФР-К
				252:32, // Штрих-Лайт-ФР-К
				240:32, // Штрих-Лайт-ПТК
			};

			const vats = {
				18.0: 1,
				10.0: 2,
				0: 3
			};

			var model;
			function check(data, fail, drop, open) {
				const modes = {
					1: "Выдача данных",
					5: "Блокировка по неправильному паролю налогового инспектора",
					6: "Ожидание подтверждения ввода даты",
					7: "Разрешение изменения положения десятичной точки",
					9: "Режим разрешения технологического обнуления",
					10: "Тестовый прогон.",
					11: "Печать полного фис. отчета",
					12: "Печать отчёта ЭКЛЗ",
					13: "Работа с фискальным подкладным документом",
					14: "Печать подкладного документа"
				};

				switch (true) {
/*					case (!(data[2] & 2)):
						return fail("Закончилась бумага");*/
					case (4 == data[5]): // смена закрыта, открываем
						return open();
					case (8 == data[5] || 40 == data[5]): // открыт чек, закрываем
						return drop();
					case (3 == data[5]):
						return fail("Ошибка при печати кассового чека. Смена превысила 24 часа, для продолжения работы необходимо снять Z-отчет");
					case (0 == data[5] || 2 == data[5] || 15 == data[5]):
						return Promise.resolve();
					default: return Promise.reject("Печать чека невозможна в режиме(подрежиме): "+(modes[data[5]] || data[5])+"("+data[6]+")");
				}
			}

			const ll = function () { return (linelengths[model] || 40); };

			const ofdblock = U.ofd(
				win1251encode,
				fillarr(0x2d, ll()),
				[0x2a, 0x2a, 0x2a, 0x20, 0xd4, 0xd0, 0x20, 0x2a, 0x2a, 0x2a],
				[0x3a, 0x20]
			)

			log("печатаем чек "+(receipt.return?"возврата":"продажи"));
			const rtype = receipt.return ? [0x02] : [0x00];
			const stype = receipt.return ? [0x82] : [0x80];

			app(
				function (send, receive, close, state) {
					const talk = U.talkmaker(send, receive, log);
					const stalk = U.safetalkmaker(send, receive, close, log, assert);

					const setreq = setreqmaker(stalk);

					function printline(prev, line) {
						return prev.then(function () {
							return stalk([0x17].concat(config.operpass).concat([3]).concat(cutbytes(line, 40)), "Ошибка при печати строки (" + line + ")");
						});
					}

					function sell(prev, rl) {
						return prev.
							then(function () {
								log("печать строки "+JSON.stringify(rl));

								const quantity = int2bytes(Math.round(rl.quantity*1000), [0,0,0,0,0]).reverse();
								const amount = int2bytes(rl.price, [0,0,0,0,0]).reverse();
								const name = cutbytes(win1251encode(rl.name), 40);

								log("данные подготовлены, amount "+amount.map(hx).join()+", quantity: "+quantity.map(hx).join()+", name: "+name.map(hx).join());

								const p = stype.concat(config.operpass).concat(quantity).concat(amount).concat([0, (vats[rl.vat] || 4), 0, 0, 0]).concat(name);
								return stalk(p, "Ошибка печати строки чека");
							});
					}

					function fillmodel(withmodel) {
						log("получаем модель");
						return stalk([0xfc], "Ошибка при получении типа устройства").
						then(function (data) {
							model = data[6];
							log("получили модель Штриха ("+model+")");
							return Promise.resolve();
						});
					}

					function open() {
						return talk([0xE0].concat(config.operpass)).
						then(function (data) {
						    if (data[1]) throw e0(data[1]);
						    else {
							    log("ждем когда смена откроется");
							    return wait4finish(talk);
							}
						}).then(function () {
							log("Смена открыта");
							return Promise.resolve();
						}).
						catch (function(e) {
						    close(function() { err(e.message || e, e.code); });
						});
					}

					function drop() {
						return stalk([0x88].concat(config.operpass), "Ошибка аннулирования незакрытого чека").
						then(function () {
							log("предыдущий не закрытый чек аннулирован");
							return new Promise(function(r) {
								setTimeout(r, 200);
							});
						});
					}

					function fail(msg) {
						return Promise.reject(new Error(msg));;
					}

					function upload(data) {
						function upload(data, block) {
							log("Uploading " + (data.map(hx)) + " into block " + block);
							return stalk(
								[0xdd].							// загрузка данных
								concat(config.operpass).		// пароль
								concat([0x00]).					// тип данных (0 для двухмерного штрихкода)
								concat(block).					// порядковый номер блока данных
								concat(data)					// данные (64 байта)
								, "Ошибка загрузки данных")
						}

						const blocks = splitbytes(data, 64);
						return blocks.reduce(
							(p, block, index) => {
								log("Uploading " + JSON.stringify(block) + " into block " + index);
								return p.then(() => upload(block, index));
							}, Promise.resolve());
					}

					function egaisprint(receipt) {
						function qr() {
							if (kkm.shtrikh.qr_supported) {
	                            log("Printing QR code");
	                            const data = win1251encode(receipt.url);
	                            return upload(data).then(() => stalk(
	                                [0xde].// печать многомерного штрих-кода
	                                concat(config.operpass).// пароль
	                                concat([
	                                    0x03,						// тип штрихкода (QR)
	                                    data.length,				// длина данных
	                                    0x00,						// номер начального блока данных
	                                    0x00,						// версия (авто)
	                                    0x00,
	                                    0x00,
	                                    0x06,						// размер точки
	                                    0x00,
	                                    0x02,						// уровень коррекции ошибок
	                                    0x01						// выравнивание (по центру)
	                                ])
	                            ));
	                        } else {
								log("Skipping QR code print");
								return Promise.resolve();
							}
						}

						const head = kkm.egais.head(win1251encode, receipt, kkm.shtrikh.cashbox, ll());
						const tail = kkm.egais.tail(win1251encode, receipt, ll());

						return head.reduce(printline, Promise.resolve())
							.then(qr)
							.then(() => tail.reduce(printline, Promise.resolve()));
					}

					fillmodel(). // выясняем с какой моделью ФР работаем
					then(function () { // запрашиваем информацию о состоянии ФР
						log("получаем состояние ФР");
						return talk([0x10].concat(config.operpass));
					}).
					then(function (data) { // проверяем, готов ли ФР к печати
						log("проверяем готовность ФР")
						return check(data, fail, drop, open);
					}).
					// устанавливаем имя кассира в таблице в ФР для печати на чеке и отправки в ОФД
					then(setcashiermaker(receipt.cashier, stalk, log, 1)).
					then(function () { // открываем чек
						log("открываем чек");
						return stalk([0x8d].concat(config.operpass).concat(rtype), "Ошибка открытия чека");
					}).
					then(function() { // устанавливаем почту|телефон покупателя в реквизитах чека
						log("устанавливаем почту/телефон покупателя ("+receipt.counterparty+")");
						return (!!receipt.counterparty) ?
							setreq(1008, receipt.counterparty) :
							Promise.resolve();
					}).
					then(function () { // печатаем строки чека
						log("печать позиций");
						return receipt.positions.reduce(sell, Promise.resolve());
					}).
					then(function () { // печатаем подытог чека
						log("подытог чека");
						return stalk([0x89].concat(config.operpass), "Ошибка печати подытога чека");
					}).
					then(function () { // закрываем чек
						log("закрываем чек");
						const cash = int2bytes(receipt.paidSum, [0,0,0,0,0]).reverse();
						const nocash = int2bytes(receipt.noCashSum, [0,0,0,0,0]).reverse();
						const summ =  cash.concat(zeroarr(10)).concat(nocash.concat(zeroarr(46)));

						return stalk([0x85].concat(config.operpass).concat(summ), "Ошибка закрытия чека").
						then(function () { return new Promise(function (r) { setTimeout(r, 500); }) });
					}).
					then(function() { state.stagecritical = false; }). // с этого момента разрыв подключения к ФР не приводит к ошибке в продаже
					then(function () { // ждем окончания закрытия чека
						log("ждем когда чек закроется");
						return wait4finish(talk);
					}).
					then(function() { // печать чека УТМ, state[8] - номер в зале
						log("печать чека ЕГАИС");
						return receipt.egais1 ? egaisprint(receipt.egais1) : Promise.resolve();
					}).
					then(function () { // отрезаем чек
						log("отрезка чека");
						return cutreceipt(stalk);
					}).
					then(function () { // закрываем соединение
						log("отключаемся");
						return new Promise(function(r) { close(r); });
					}).
					then(onfinish). // передаем управление наружу
                    catch(e => {
                        close(function () { err(e.message || e); });
                    }); // обработка ошибок внутри цикла печати
				}
			);
		}

		function z(cashier, onfinish) {
			app(function(send, receive, close) {
				const talk = U.talkmaker(send, receive, log);
				const stalk = U.safetalkmaker(send, receive, close, log, assert);

				setcashiermaker(cashier, stalk, log, 30)().
				then(function () { return stalk([0x41].concat(config.adminpass), "Ошибка печати Z-отчета"); }). // печатаем отчет
				then(function () { return wait4finish(talk).then(function () { return new Promise(function (r) { setTimeout(r, 200); }) }); }). // ждем окончания печати
				then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
				then(onfinish); // передаем управление наружу
			});
		}

		function x(cashier, onfinish) {
			app(function(send, receive, close) {
				const talk = U.talkmaker(send, receive, log);
				const stalk = U.safetalkmaker(send, receive, close, log, assert);

				setcashiermaker(cashier, stalk, log, 30)().
				then(function () { return stalk([0x40].concat(config.adminpass), "Ошибка печати X-отчета"); }). // печатаем отчет
				then(function () { return wait4finish(talk).then(function () { return new Promise(function (r) { setTimeout(r, 200); }) }); }). // ждем окончания печати
				then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
				then(onfinish); // передаем управление наружу
			});
		}

		function cashin(data, onfinish) {
			const summ = data.sum || data;
			const cashier = data.cashier;

			app(function(send, receive, close) {
				const talk = U.talkmaker(send, receive, log);
				const stalk = U.safetalkmaker(send, receive, close, log, assert);

				setcashiermaker(cashier, stalk, log, 1)().
				then(function () { return stalk([0x50].concat(config.operpass).concat(int2bytes(summ, [0,0,0,0,0]).reverse()), "Ошибка при внесении наличных"); }).
				then(function() { return wait4finish(talk) }). // ждем окончания печати внесения
				then(function () { return cutreceipt(stalk); }). // отрезаем чек
				then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
				then(onfinish);
			});
		}

		function cashout(data, onfinish) {
			const summ = data.sum || data;
			const cashier = data.cashier;

			app(function(send, receive, close) {
				const talk = U.talkmaker(send, receive, log);
				const stalk = U.safetalkmaker(send, receive, close, log, assert);

				setcashiermaker(cashier, stalk, log, 1)().
				then(function() { return stalk([0x51].concat(config.operpass).concat(int2bytes(summ, [0,0,0,0,0]).reverse()), "Ошибка при выплате наличных"); }).
				then(function() { return wait4finish(talk); }). // ждем окончания печати выплаты
				then(function () { return cutreceipt(stalk); }). // отрезаем чек
				then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
				then(onfinish);
			});
		}

		function changext(cashier, onfinish) {
			app(
				function (send, receive, close) {
					const talk = U.talkmaker(send, receive, log);
					const stalk = U.safetalkmaker(send, receive, close, log, assert);

					setcashiermaker(cashier, stalk, log, 1)().
					then(function () { return talk([0xE0].concat(config.operpass)); }).
					then(function (data) {
					    if (data[1]) throw e0(data[1]);
					    else {
						    log("ждем когда смена откроется");
						    return wait4finish(talk);
						}
					}).then(function () {
						return new Promise(function (r) {
							log("Смена открыта");
							setTimeout(r, 200);
						});
					}).then(function () { close(onfinish); }).
					catch(function(e) {
					    close(function() { err(e.message || e, e.code); });
					});
				}
			);
		}

		function reportCalcStatusext(cashier, onfinish) {
			app(function(send, receive, close) {
				const talk = U.talkmaker(send, receive, log);
				const stalk = U.safetalkmaker(send, receive, close, log, assert);
				const settable = settablemaker(stalk);

				setcashiermaker(cashier, stalk, log, 30)().
				then(function () { return stalk([0xFF,0x38].concat(config.adminpass), "Ошибка печати отчета о состоянии рассчетов"); }). // печатаем отчет
				then(function () { return wait4finish(talk).then(function () { return new Promise(function (r) { setTimeout(r, 200); }) }); }). // ждем окончания печати
				then(function () { return new Promise(function(r) { close(r); }); }). // закрываем соединение
				then(onfinish); // передаем управление наружу
			});
		}

		function setreqmaker(stalk) {
			return function (req, text) {
				const textbytes = win1251encode(text);
				const reqbytes = short2bytes(req);
				const lenbytes = short2bytes(textbytes.length);

				return stalk(
					[0xff, 0x0c].
					concat(config.operpass).
					concat(reqbytes).
					concat(lenbytes).
					concat(textbytes),
					"Ошибка при установке значения ("+text+") реквизита "+req);
			}
		}

		function settablemaker(stalk) {
			return function (tab, row, field, val) {
				const rowbytes = short2bytes(row);
				const valbytes = cutbytes(win1251encode(val), 40);
				return stalk(
					[0x1e].
					concat(config.adminpass).
					concat(tab).
					concat(rowbytes).
					concat(field).
					concat(valbytes),
					"Ошибка при записи значения в таблицу ФР "+tab+", ряд "+row+", поле "+field);
			}
		}

		function setcashiermaker(cashier, stalk, log, num) {
			const settable = settablemaker(stalk);
			return (!!cashier) ?
				function () {
					log("устанавливаем имя кассира в таблица ФР для печати в чеке");
					return settable(2, num, 2, cashier);
				} :
				U.pass();
		}

		proc(beep, demo, info, type, check, printext, z, x, changext, cashin, cashout, reportCalcStatusext, corrprint, hackfr);
	});
}
