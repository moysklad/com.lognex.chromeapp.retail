if (!kkm.egais)
	kkm.egais = {};

kkm.egais.date = function (d) {
	function f(n) {
		const r = ""+n; 
		return 2==r.length?r:"0"+r;
	};

	return d.getFullYear()+"."+f(1+d.getMonth())+"."+f(d.getDate())+" "+f(d.getHours())+":"+f(d.getMinutes()); 
}

kkm.egais.head = function (e, rec, cashbox, ll) {
	var res = [];
	res.push(e(" "));
	res.push(e(center(rec.head, ll, ' ')));
	res.push(e(" "));
	res.push(e(squinter("ИНН:"+rec.inn, "КПП:"+rec.kpp, ll)));
	res.push(e(squinter("КАССА:"+cashbox, "СМЕНА:"+rec.shift, ll)));
	res.push(e(squinter("ЧЕК:"+rec.doc, "ДАТА:"+kkm.egais.date(new Date()), ll)));
	res.push(e(" "));

	return res;
}

kkm.egais.tail = function (e, rec, ll) {
	var res = [e(" ")];
	res = res.concat(splitbytes(e(rec.url), ll));
	res.push(e(" "));
	res = res.concat(splitbytes(e(rec.sign), ll));
	res.push(e(" "));
	res.push(e(" "));
	res.push(e(" "));
	res.push(e(" "));
	res.push(e(" "));
	res.push(e(" "));

	return res;				
}

kkm.egais.parseinfo = function(data) {
	const res = {};
	res.cashbox = fromBCD([0x00, 0x00, data[2], data[3]]);
	res.receipt = fromBCD([0x00, 0x00, data[18], data[19]]);
	res.change = fromBCD([0x00, 0x00, data[20], data[21]]);

	return res;
}