kkm.pirit54fz.ext = function (logging, config, executor, proc) {
	kkm.pirit.ext(logging, config, executor, function (beep, demo, info, type, check, print, z, x, change, cashin, cashout, ofdreport, corrprint, hackfr) {
	    const U = kkm.util;
	    const L = U.modlog(logging(), "...");

	    const log = L.log;
	    const err = L.err;

	    const X = kkm.ext;
	    const PU = kkm.pirit.utils;

	    const status = PU.statusmaker(log);
	    const frerror = PU.frerrormaker(log, err);

	    const e0 = function(code) {
	        const msg = config.errtable[code] || "Ошибка при открытии смены ("+code+")";
	        const e = new Error(msg);
	        e.code = 001;
	        return e;
	    };

	    function app(chain) {
	        executor(logging, chain);
	    }

	    const talkmaker = function(send, receive, log) {
	    	const t = U.talkmaker(send, receive, log);
	    	return function(data) {
	    		return t(config.password.concat(data));
	    	}
	    }

	    const talkfunctionmaker = function(log, talk) {
	    	return function(msg, data) {
	    		return function () {
		    		log(msg);
		    		return talk(data);
	    		}
	    	}
	    }

	    function getcashier(data) {
	    	return (!!data ? cp866encode(data) : [0x8A, 0xA0, 0xE1, 0xE1, 0xA8, 0xE0]).concat(0x1c);
	    }

	    function changeext(data, onfinish) {
	    	const PU = kkm.pirit.utils;
	    	app(function(send, receive, close){

				function changeprint() {
					return talk([0x29, 0x32, 0x33].concat(getcashier(!!data && !!data.cashier ? data.cashier : false))).// открываем смену
					then(function (data) {
						const ec = PU.errcode(data);
					    if (ec) throw e0(ec);
					    else close(onfinish);
					}). // передаем управление наружу
	    			catch(function(e) { close(err(e.message || e, e.code)); }); // обрабатываем ошибки
				}

		    	function check(data) {
		    		const retry = checkFR;

		    		const m10 = status(data); // получаем флаги состояния из запроса
		    		frerror(m10[0]); // проверяем на фатальные ошибки
		    		if ((m10[1] & 1) != 0) PU.commandStart(send, receive, close, config, retry); // начать обмен данными с ФР, если еще не начат
		    		else if ((m10[1] & 4) != 0) close(err("Смена уже открыта")); // проверяем, не открыта ли уже смена
		    		else if (m10[2] != 0) talk([0x32, 0x33, 0x32]).then(retry); // аннулируем предыдущий документ, если он открыт
		    		else return changeprint();
		    	}

	    		const talk = talkmaker(send, receive, log);
	    		const talkf = talkfunctionmaker(log, talk);

				function checkFR() {
		          log("проверяем состояние ФР");
		        		talk([0x81, 0x30, 0x30]).
		        		then(check);
		        }
		        checkFR();
	    	});
	    }

	    function ofdreportext(data, onfinish) {
	    	const PU = kkm.pirit.utils;
	    	app(function(send, receive, close) {

				function ofdreportprint() {
					return talk([0x30, 0x35, 0x39].concat(cashier)). // печатаем отчет о состоянии рассчетов
					then(function() { close(onfinish); }). // передаем управление наружу
					catch(function(e) { close(err(e.message || e)); }); // обрабатываем ошибки
				}

	    		const retry = checkFR;

		    	function check(data) {
		    		const m10 = status(data); // получаем флаги состояния из запроса
		    		frerror(m10[0]); // проверяем на фатальные ошибки

		    		if ((m10[1] & 1) != 0) PU.commandStart(send, receive, close, config, retry); // начать обмен данными с ФР, если еще не начат
		    		else if ((m10[1] & 4) != 0) close(err("Смена открыта")); // проверяем, закрыта ли уже смена
		    		else return ofdreportprint();
		    	}

	    		const talk = talkmaker(send, receive, log);
	    		const talkf = talkfunctionmaker(log, talk);

	    		const cashier = getcashier(data);

				function checkFR() {
			      log("проверяем состояние ФР");
			    		talk([0x81, 0x30, 0x30]).
			    		then(check);
			    }

			    checkFR();
	    	});
	    }

		function corrprint(prms, onfinish) {
			const PU = kkm.pirit.utils;
			/*
			Прошивка с добавлением СНО и типа в коррекцию:
			659 прошивка для Вики Принтов Ф,
			для Пирита 1Ф это 158 прошивка,
			для Пирита 2Ф - 559.
			*/
			if(!PU.version || PU.version < 158 || (PU.version>=550 && PU.version<559) || (PU.version>=650 && PU.version<659))
			{
				return err("Печать чека коррекции для прошивки "+PU.version+" невозможна. Обновите прошивку на последнюю актуальную версию.");
			}
			app(function(send, receive, close) {
				const talk = talkmaker(send, receive, log);
				const commandStartPromise = function() {
					return new Promise(function(res) {
						PU.commandStart(send, receive, close, config, res);
					});
				}

				talk([0x81, 0x30, 0x30]). // запрашиваем состояние устройства
				then(function(data) { // проверяем на фатальные ошибки
					const m10 = status(data);
					frerror(m10[0]);
					return Promise.resolve(m10);
				}).then(function(m10) { // начинаем обмен данными с ФР, если еще не начат
					if ((m10[1] & 1) == 0) return Promise.resolve(m10);
					return commandStartPromise().
					then(function() { return Promise.resolve(m10); });
				}).then(function (m10) { // открываем смену, если нужно
					if ((m10[1] & 4) != 0) {
						if ((m10[1] & 8) != 0) throw "Смена больше 24 часов";
						return Promise.resolve(m10);
					}
					return talk([0x29, 0x32, 0x33].concat(getcashier(!!prms && !!prms.cashier ? prms.cashier : false))).
					then(function (data) {
						  const ec = PU.errcode(data);
					    if (ec) throw e0(ec);
						else return Promise.resolve(m10);
					});
				}).then(function(m10) { // аннулируем предыдущий документ, если он открыт
					if (m10[2] == 0) return Promise.resolve();
					return talk([0x32, 0x33, 0x32]).then(function() { Promise.resolve(); });
				}).then(function() { // печатаем чек коррекции
					log("prms.cashsum: "+prms.cashsum);
					const cashsum = PU.convIntToInt(prms.cashsum/100);
					log("cashsum: "+cashsum);
					log("prms.nocashsum: "+prms.nocashsum);
					const nocashsum = PU.convIntToInt(prms.nocashsum/100);
					log("nocashsum: "+nocashsum);
					const enccashier = getcashier(!!prms && !!prms.cashier ? prms.cashier: false);
					log("enccashier: "+enccashier);
					const corrcheckcmd = [0x23].concat(cp866encode("58")).concat(enccashier).concat(cashsum).concat(0x1C).concat(nocashsum).concat(0x1C).
						concat([0x30, 0x1c, 0x30, 0x1c, 0x30, 0x1c, 0x30, PU.convIntToInt(prms.op), 0x1c]);
					return talk(corrcheckcmd);
				}).then(function(data) { // передаем управление наружу
					close(onfinish);
				}).catch(function(e) { // обрабатываем ошибки
					close(err(e.message || e, e.code));
				});
			});

		}

		proc(beep, demo, info, type, check, print, z, x, changeext, cashin, cashout, ofdreportext, corrprint, hackfr);
	});
}
