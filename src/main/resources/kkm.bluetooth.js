kkm.bluetooth.uuid = "00001101-0000-1000-8000-00805f9b34fb"; // АТОЛ FPrint-11 ПТК
kkm.bluetooth.listener = {};
kkm.bluetooth.err = {};

kkm.bluetooth.request = function (logging, config, client, confail) {
	kkm.util.incl(kkm.util.modlog(logging(), "........."));

	function connect (config, connected) {
		chrome.bluetoothSocket.create(function(ci) {
			if (!ci || !ci.socketId) {
				err("Невозможно подключиться к bluetooth-устройству: "+chrome.runtime.lastError.message);
				confail();
			}
			chrome.bluetoothSocket.connect(ci.socketId, config.id, kkm.bluetooth.uuid, function () {
				log("connected to bluetooth device "+config.address+" ("+kkm.bluetooth.uuid+")");
				connected(ci.socketId);
			});
		});
	}

	function send(conn, data, sent) {
		chrome.bluetoothSocket.send(conn, data, sent);
	}

	function flush(conn, flushed) { // bluetooth need no flush
		flushed(true);
	}

	const id = function (info) { return info.socketId; };

	function listener_add(conn, listener, adder) {
		adder(
			kkm.bluetooth.listener,  // listeners
			function (l) { chrome.bluetoothSocket.onReceive.addListener(l); },  // addlistener()
			id, // id()
			function(info, listener) { listener(info.socketId, info.data); }, // caller()
			"no bluetooth listener for connection " // errmesg
		)(conn, listener);
	}

	function err_add(conn, listener, adder) {
		adder(
			kkm.bluetooth.err,
			function (l) { chrome.bluetoothSocket.onReceiveError.addListener(l); },
			id,
			function (info, listener) { listener(info); },
			"no bluetooth error listener for connection "
		)(conn, listener);
	}

	function close(conn, closed) {
		chrome.bluetoothSocket.disconnect(conn, closed);
		delete kkm.bluetooth.listener[conn];
		delete kkm.bluetooth.err[conn];
	}

	function net(proc) {
		proc(connect, send, flush, listener_add, err_add, close);
	}

	kkm.request(logging, net, config, client);
}

/**
*
* обход всех подключенных bluetooth-устройств
*
* logging - конструктор набора функций логирования
*
* config - базовая конфигурация обращения к bluetooth-устройству
*
* proc - функция, вызываемая для каждого порта, вида proc(confchange),
* где confchange - модификатор общей конфигурации под конкретный порт
**/
kkm.bluetooth.enum = function (logging, config, count, proc) {
	kkm.util.incl(kkm.util.modlog(logging(), "........."));
	
	try {
		log("enumerating bluetooth devices");
		if (!chrome.bluetooth) {
			err("bluetooth not supported");
			count(0);
			return;
		}
		chrome.bluetooth.getDevices(function (devs) {
			log("found "+devs.length+" bluetooth devices");
			count(devs.length);
			for (var i=0; i<devs.length; i++) {
				if (-1 != devs[i].uuids.indexOf(kkm.bluetooth.uuid)) {
					log("found known bluetooth kkm");
					try {
						proc(function (config) {
							const conf = config();
							conf.id = devs[i].address;
							log("found kkm address: "+conf.id);
							conf.name = devs[i].name;
							log("found kkm name: "+conf.name);
							return conf;
						});
					} catch (e) { err(e.message); }
				}
			}
		});
	} catch (e) { err(e.message); }
};