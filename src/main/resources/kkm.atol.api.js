kkm.atol.api = function (logging, config, executor, proc) {
	const U = kkm.util;
	const L = logging();
	const log = L.log;
	const err = L.err;
	const assert = U.assertmaker(config.errtable, err);
	const assert0 = U.assertmaker(config.errtable, function(msg) { err(msg, 001); });

	function p(cmd) {
		return ((config.password || [0,0]).concat(cmd));
	}

	function talkmaker(send, receive, log) {
		const talk = U.talkmaker(send, receive, log);
		return function(data) {
			return talk(p(data));
		}
	};

	function safetalkmaker(send, receive, close, log, assert) {
		const stalk = U.safetalkmaker(send, receive, close, log, assert);
		return function(data, assertmsg) {
			return stalk(p(data), assertmsg);
		}
	};


	function app(chain) {
		executor(logging, chain);
	}

	function beep() {
		app(
			function (send, receive, close) {
				send(p([0x47]), function() {
					log("beeeeep!");
					close();
				});
			}
		);
	}

	function demotext(data, onfinish) {
		log("demo print");
		app(
			function (send, receive, close) {
				log("sending demoprint command");
				function printline(p, l) {
					return p.then(function () {
						return stalk([0x4c].concat(l), "Ошибка при печати строки ("+l+")");
					});
				}
				const stalk = safetalkmaker(send, receive, close, log, assert);
				log("sending demoprint command");
				const mas = [kkm.config.default.demotext,"","","","",""];
				const text = mas.map(s => cutbytes(cp866encode(s),20));
				stalk([0x048],"ошибка при переходе в режим выбора").then
				(
					function()
					{
						return text.reduce(printline, Promise.resolve());
					}
				)
				.then
				(
					function ()
					{
						close();
						if (onfinish && onfinish instanceof Function)
							onfinish();
					}
				);
			}
		);
	}

	function demo(data, onfinish) {
		log("demo print");
		app(
			function (send, receive, close) {
				log("sending demoprint command");
				receive(assert("ошибка при переходе в режим выбора", close, function (data) {
					log("перешли в режим выбора");
					receive(assert("Ошибка при тестовом прогоне", close, function (data) {
						log("got response: "+data.join());
						close();
						if (onfinish && onfinish instanceof Function)
							onfinish();
					}));
					send(p([0x82, 0x01, 0x00, 0x00]), function(send, receive) {
						log("demo print processing!");
					});
				}));
				send(p([0x048]), function () {
					log("переходим в режим выбора");
				});
			}
		);
	}

	function info(proc) {
		app(
			function (send, receive, close) {
				send(p([0x3f]), function() {
					log("info request sent!");
					receive(function (data) {
						close(function () { proc(data); });
					});
				});
			}
		);
	}

	function type(proc) {
		app(
			function (send, receive, close) {
				send(p([0xa5]), function() {
					log("type request sent!");
					receive(function (data) {
						log(data[0] ? "error "+data[0] : "type data received");
	                    close(function () { proc(data); });
					});
				});
			}
		);
	}

	function check(type, info) {
		app(
			function (send, receive, close) {
				send(p([0xa5]), function() {
					log("type request sent!");
					receive(function (data) {
						log(data[0] ? "error "+data[0] : "type data received");
	                    type(data);
						send(p([0x3f]), function() {
							log("info request sent!");
							receive(function (data) {
								log("info data received! ["+data+"]");
								close(function () { info(data); });
							});
						});
					});
				});
			}
		);
	}

	function print(receipt, onfinish) {
		var model;

		function prepare(send, receive, close, data) {
			const talk = talkmaker(send, receive, log);
			const stalk = safetalkmaker(send, receive, close, log, assert);
			const stalk0 = safetalkmaker(send, receive, close, log, assert0);

			model = data[14];
			const mode = data[17];
			const change = data[9];
			const receiptnum = data[22];

			function modesuper() {
				if (1 < mode) {
					log("переходим в режим выбора");
					return stalk([0x48], "Ошибка выхода в режим выбора");
				}
				log("уже в режиме выбора или регистрации");
				return Promise.resolve()
			}

			function moderegister() {
				if (1 == mode) {
					log("уже в режиме регистрации");
					return Promise.resolve();
				}

				log("переходим в режим регистрации");
				return stalk([0x56, 0x01].concat(config.operpass), "Ошибка при входе в режим регистрации").
				then(function() {
					return talk([0x3f]);
				}).
				then(function (data) {
					if (1 != data[17])
						throw "Устройство не может войти в режим регистрации";
					else
						return Promise.resolve();
				});
			}

			function openchange() {
				if (0 == (change & 2)) {
					log("открываем смену");
					return stalk0([0x9a, 0x00], "Ошибка открытия смены");
				} else {
					log("смена уже открыта");
					return Promise.resolve();
				}
			}

			function openreceipt() {
				log("открываем чек"+(receipt.return ? " возврата":""));
				return stalk([0x92, 0x00, receipttype()], "Ошибка открытия чека");
			}

			function cancelreceipt(f) {
				if (0 == receiptnum) {
					log("предыдущий чек закрыт");
					Promise.resolve();
				} else {
					log("аннулируем не закрытый чек");
					return stalk([0x59], "Ошибка аннулирования чека");
				}
			}

			function receipttype() { // чек продажи (1) или чек возврата (2)
				return receipt.return ? 0x02 : 0x01;
			}

			return modesuper(). // если не в режимах выбора или регистрации - переходим в выбор
			then(moderegister). // переходим в режим регистрации если еще не в нем
			then(openchange). // если смена закрыта - открываем
			then(cancelreceipt). // если не закрыт предыдущий чек - аннулируем его
			then(openreceipt); // открываем чек
		}


		const linelengths = {
			13:40, 14:20, 15:20, 16:24,
			20:48, 23:39, 24:38, 27:38,
			30:56, 31:32, 32:56, 35:36,
			41:56, 45:56, 46:72, 47:25,
			51:32, 52:48, 53:57
		};

        const cutEgaisCheckModels = [52];

		const ofdblock = U.ofd(
			cp866encode,
			fillarr(0x2d, (linelengths[model] || 20)),
			[0x2a, 0x2a, 0x2a, 0x20, 0x94, 0x8f, 0x20, 0x2a, 0x2a, 0x2a],
			[0x24, 0x20]
		);

		function paytype() {
			return ("cash" == receipt.type ? [0x01] : [0x04]);
		}

		function parseinfo(data) {
			const res = {};
			res.cashbox = fromBCD([0x00, 0x00, data[2], data[3]]);
			res.receipt = fromBCD([0x00, 0x00, data[18], data[19]]);
			res.change = fromBCD([0x00, 0x00, data[20], data[21]]);

			return res;
		}

        function canCutEgaisCheck(infodata) {
            const model = infodata[14];
            return cutEgaisCheckModels.includes(model);
        }

		app(
			function (send, receive, close) {
				const talk = talkmaker(send, receive, log);
				const stalk = safetalkmaker(send, receive, close, log, assert);

				function printline(p, l) {
					return p.then(function () {
						return stalk([0x4c].concat(l), "Ошибка при печати строки ("+l+")");
					});
				}

				function pos(rl) {
					function sell(a, q) {
						return [0x52, 0x00].concat(a).concat(q).concat([0x01]);
					}

					function ret(a, q) {
						return [0x57, 0x00].concat(a).concat(q);
					}

					log("печать строки "+JSON.stringify(rl));

					const amount = int2BCDbytes(rl.price, [0, 0, 0, 0, 0]);
					const quantity = int2BCDbytes(Math.round(rl.quantity*1000), [0, 0, 0, 0, 0]);
					const name = splitbytes(cp866encode(rl.name), (linelengths[model] || 20));

					log("данные подготовлены, amount "+amount.join()+", quantity: "+quantity.join()+", name: "+name.join());

					var s = (receipt.return ? ret: sell)(amount, quantity);

					return name.reduce(printline, Promise.resolve()).
					then(function () { return stalk(s, "Ошибка при регистрации позиции чека"); });
				}

				function egaisprint(er, infodata) {
					function qr() {
                        if (kkm.atol.qr_supported) {
                            log("printing qr code, text "+er.url);
                            return stalk(
                                [0xC1, // <C1h> печать QR кода
                                    0,     // <Тип штрихкода(1)>, qr
                                    2,     // <Выравнивание (1)>, по центру
                                    8,     // <Ширина (1)>
                                    0, 0,  // <Версия(2)>, автоматический подбор
                                    0, 1,  // <Опции (2)>
                                    0,     // <Уровень коррекции(1)>, по настройке Т2Р1П93
                                    0,     // <Количество строк(1)> (не исп.)
                                    0,     // <Количество столбцов(1)> (не исп.)
                                    0, 0,  // <Пропорции штрихкода(2)> (не исп.)
                                    0, 0]. // <Пропорции пикселя(2)> (не исп.)
                                    concat(cutbytes(win1251encode(er.url), 100)), // <Строка данных(100)>
                                "Ошибка печати QR-кода"
                            );
                        } else {
                            log("fr is not able to print qr code");
                            return Promise.resolve();
                        }
					}

                    function cutEgaisCheck () {
                        if (!canCutEgaisCheck(infodata)) {
                            log("fr is not able to cut egais check");
                            return Promise.resolve();
                        }

                        log("cut egais check");
                        return talk([0x75, 0x00]); // отправляем команду отрезки чека
                    }

					return function() {
						const E = kkm.egais;
						const head = E.head(cp866encode, er, parseinfo(infodata).cashbox, linelengths[model] || 20);
						const tail = E.tail(cp866encode, er, linelengths[model] || 20);

						log("printing egais receipt head, "+head.length+" lines");

						return head.reduce(printline, Promise.resolve()). // начало чека егаис (до qr-кода)
						then(qr).
						then(function () {
							return tail.reduce(printline, Promise.resolve()); // конец чека егаис (после qr-кода)
						}).
                        then(function () {
                            return stalk([0x6C]); // отправляем команду печати клише
                        }).
                        then(cutEgaisCheck); // отрезаем чек, если нужно
					}
				}

				const block = ofdblock(receipt.ofdCode, receipt.ofdKKT, receipt.ofdTime, false);

				function closereceipt() {
					function closepromise() {
						return new Promise(function(res, rej) { close(res); });
					}

					log("закрываем чек");

					return stalk([0x4a, 0x00].concat(paytype()).concat([0, 0, 0, 0, 0]), "Ошибка закрытия чека"). // закрываем чек
					then(function() { return talk([0x3f]); }). // снова проверяем состояние, чек должен быть закрыт
					then(function(data) {
						if (1 == data[22]) {
							return stalk([0x59], "Ошибка аннулирования чека").then(closepromise);
						} else {
							const egais1 = receipt.egais1 ? egaisprint(receipt.egais1, data) : U.pempty;
							const egais2 = receipt.egais2 ? egaisprint(receipt.egais2, data) : U.pempty;

							return egais1().then(egais2).then(closepromise);
						}
					})
				}

				function printsum(sum, paytype) {
					if (!sum) {
						log("суммы по типу оплаты "+paytype+" нет");
						return Promise.resolve();
					}
					log("печать суммы по типу оплаты "+paytype);
					const sumbytes = int2BCDbytes(sum|0, [0, 0, 0, 0, 0]);
					return stalk([0x99, 0x00].concat(paytype).concat(sumbytes), "Ошибка при печати суммы");
				}

				function regdiscount() {
					if (!receipt.discount) {
						log("скидки нет");
						return Promise.resolve();
					}
					log("регистрируем скидку")
					const discount = int2BCDbytes((receipt.discount)|0, [0, 0, 0, 0, 0]);
					return stalk([0x43, 0x00, 0x00, 0x01, 0x00].concat(discount), "Ошибка регистрации скидки");
				}

				return talk([[0x3f]]). // получаем состояние фискальника
				then(function (data) { return prepare(send, receive, close, data); }). // по данным состояния фискальника готовим его к печати строк чека
				then(function() { return block.reduce(printline, Promise.resolve()); }). // печатаем шапку онлайновой фискальнизации, если таковая была
				then(function() { // печатаем строки чека
					function sellchain(prev, line) {
						return prev.then(function() { return pos(line); });
					}

					return receipt.positions.reduce(sellchain, Promise.resolve());
				}).
				then(function() { return regdiscount(); }). // регистрируем скидку на чек, если она есть
				then(function() { return printsum(receipt.paidSum, 1); }). // печатаем сумму оплаты налом, если она есть
				then(function() { return printsum(receipt.noCashSum, 4); }). // печатаем сумму оплаты безналом, если она есть
				then(closereceipt). // закрываем чек
				then(onfinish); // отдаем управление наружу
			}
		);
	}

	function z(data, onfinish) {
		function reportmode(send, receive, close, succ) {
			function modeswitch() {
				log("переход в режим отчетов с гашением")
				receive(assert("Ошибка перехода в режим отчетов с гашением", close, succ));
				send(p([0x56, 0x03]).concat(config.adminpass), function () {});
			}

			function modeexit() {
				log("Ошибка при переходе в режим выбора");
				receive(assert("Ошибка выхода в режим выбора", close, function () {
					modeswitch(succ);
				}));
				send(p([0x48]), function(){});
			}

			receive(function (data) {
				var proc = modeexit;
				if (0 == data[17])
					proc = modeswitch;
				if (3 == data[17])
					proc = U.empty;

				if (0 == (data[9] & 2)) {
					err("Невозможно повторное снятие Z-отчета");
					close(function () {
						if (onfinish)
							onfinish();
					});
				}
				else
					proc(succ);
			});
			send(p([0x3f]), function () {});
		}

		function wait4finish(send, receive, onfinish) {
			log("проверяем завершена ли печать отчета");
			receive(function (data) {
				log("состояние полученo");
				log("в режиме "+data[1]);
				if (0x17 == data[1] || [0x23] == data[1]) // z-отчет все еще печатается
					setTimeout(function () { wait4finish(send, receive, onfinish); }, 2000);
				else
					onfinish(); // z-отчет печать окончил, едем дальше
			});
			send(p([0x45]), function () { // получаем состояние устройства
				log("отправлен запрос состояния устройства");
			});
		}

		app(
			function(send, receive, close) {
				reportmode(send, receive, close, function() {
					receive(assert("Ошибка запроса Z-отчета", close, function () {
						wait4finish(send, receive, function () {
							close(function () {
								if (onfinish)
									onfinish();
							});
						});
					}));
					send(p([0x5a]), function () { log("Z-отчет запрошен"); });
				})
			}
		);
	}


	function x(data, onfinish) {
		function reportmode(send, receive, close, succ) {
			function modeswitch(succ) {
				log("переход в режим отчетов без гашения")
				receive(assert("Ошибка перехода в режим отчетов без гашения", close, succ));
				send(p([0x56, 0x02]).concat(config.adminpass), function () {});
			}

			function modeexit(succ) {
				log("Ошибка при переходе в режим выбора");
				receive(assert("Ошибка выхода в режим выбора", close, function () {
					modeswitch(succ);
				}));
				send(p([0x48]), function(){
				});
			}

			receive(function (data) {
				var proc = modeexit;
				if (0 == data[17])
					proc = modeswitch;
				if (3 == data[17])
					proc = U.empty;

				proc(succ);
			});
			send(p([0x3f]), function () {
			});
		}

		function wait4finish(send, receive, onfinish) {
			log("проверяем завершена ли печать отчета")
			send(p([0x45]), function () { // получаем состояние устройства
				log("отправлен запрос состояния устройства");
				receive(function (data) {
					log("состояние полученo");
					log("в режиме "+data[1]);
					if (0x17 == data[1] || [0x22] == data[1]) {// x-отчет все еще печатается
						log("x-отчет все еще печатается")
						setTimeout(function () { wait4finish(send, receive, onfinish); }, 2000);
					} else {
						onfinish(); // x-отчет печать окончил, едем дальше
					}
				});
			});
		}

		app(
			function(send, receive, close) {
				reportmode(send, receive, close, function() {
					receive(assert("Ошибка запроса X-отчета", close, function () {
						log("х-отчет начал печататься");
						wait4finish(send, receive, function () {
							close(function () {
								if (onfinish)
									onfinish();
							});
						});
					}));
					send(p([0x67, 0x01]), function () { log("X-отчет запрошен"); });
				})
			}
		);
	}

	function moderegister(stalk, talk, checkchange) { // переход в режим регистрации
		return talk([0x3f]).
		then(function (data) {
			function reg() {
				return stalk([0x56, 0x01].concat(config.adminpass), "Ошибка при переходе в режим регистрации");
			}

			if (!(checkchange ^ (0 != (data[9] & 2)))) {// смена уже открыта при открытии или закрыта при внесении/выплате
				return Promise.reject(new Error(checkchange ? "Смена уже открыта" : "Смена закрыта. Операция невозможна."));
			}

			if (1 == data[17])
				return Promise.resolve();

			if (0 == data[17])
				return reg();

			return stalk([0x48], "Ошибка при переходе в режим выбора").then(reg);
		});
	}

	function change(data, onfinish) {
		app(function(send, receive, close) {
			const stalk0 = safetalkmaker(send, receive, close, log, assert0);
			const talk = talkmaker(send, receive, log);

			moderegister(stalk0, talk, true).
			then(function() {
				return stalk0([0x9a, 0x00], "Ошибка при открытии смены");
			}).
			then(function() { close(onfinish);}, function() { close(onfinish);}).
			catch(function(e) { close(function () { err(e.message || e, 001); }); });
		});
	}

	function cashin(data, onfinish) {
		const sum = data.sum || data;
		app(function(send, receive, close) {
			const stalk = safetalkmaker(send, receive, close, log, assert);
			const talk = talkmaker(send, receive, log);

			moderegister(stalk, talk, false).
			then(function () {
				const sumbytes = int2BCDbytes(sum|0, [0,0,0,0,0]);
				return stalk([0x49, 0x00].concat(sumbytes), "Ошибка при внесении наличных");
			}).
			then(function() { close(onfinish); }).
			catch(function(e) { close(err(e.message ? e.message : e)); });
		});
	}

	function cashout(data, onfinish) {
		const sum = data.sum || data;
		app(function(send, receive, close) {
			const stalk = safetalkmaker(send, receive, close, log, assert);
			const talk = talkmaker(send, receive, log);

			moderegister(stalk, talk, false).
			then(function () {
				const sumbytes = int2BCDbytes(sum|0, [0,0,0,0,0]);
				return stalk([0x4f, 0x00].concat(sumbytes), "Ошибка при выплате наличных");
			}).
			then(function() { close(onfinish); }).
			catch(function(e) { close(err(e.message ? e.message : e)); });
		});
	}

	proc(beep, demotext, info, type, check, print, z, x, change, cashin, cashout);
}
