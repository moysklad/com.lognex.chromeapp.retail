kkm.request = function (logging, net, config, client, oncomplete) {
	net(function (n_connect, n_send, n_flush, n_listener_add, n_err_add, n_close) {
		var connected = false;

		const l = logging();
		const log = function (msg) { l.log("......"+msg) };
		const err = l.err;

		const bin = function(data) {
			var buf = new ArrayBuffer(data.length)
			var view = new Uint8ClampedArray(buf);
			view.set(data);
			return buf;
		}

		const flush = function(conn, onflush) {
			n_flush(conn, function (flushed) {
				log("serial is "+(flushed ? "":"not ")+"flushed"+(flushed ? "" : ", "+(chrome.runtime.lastError || {message:"error unknown"}).message));
				onflush();
			});
		}

		const send1 = function (conn, data, onsend) {
			if (!connected) return;

			log("sending ["+data.map(hx)+"]");

			n_send(conn, bin(data), onsend);
		}

		const receive1 = (function () { // receive data from port, buffer it and process it
			var conn = false; // all responses should pass from single connection
			const handlers = []; // handlers to process received data, [check, handler] records, LIFO logic
			const buff = new Uint8ClampedArray(new ArrayBuffer(512)); // no response should be bigger then specified size
			var shift = 0; // amount of data already in buffer
			
			function store(ab) {
				const v = new Uint8ClampedArray(ab);
				for (var i=0; i<v.length; i++)
					buff[shift+i] = v[i];
				shift += v.length;
				log("shift is "+shift);
			}

			function restore() {
				const res = [];
				for (var i=0; i<shift; i++)
					res[i] = buff[i];
				return res;
			}

			function handle() {
				log("searching for appropriate handler for conn "+conn+", data ["+restore().map(hx)+"]");
				const h = handlers[handlers.length-1];
				if (!h) {
					log("no handlers specified for connection "+conn);
					return;
				}

				const matches = h[0](buff, shift);
				log("handler "+(matches ? "": "not ")+"match");
				if (! matches) return;

				handlers.length--;
				const data = restore();
				log("handling "+data.map(n=>(n).toString(16)));
				h[1](data);
				shift = 0;
			}

			const receivehandler = function (c, data) {
				if (! connected) return;

				log("receive");
				if (!conn) {
					log("unexpected receive, terminating it");
					return;
				}
				if (c != conn) {
					log("incorrect connection "+c+" (should be "+conn+")");
					return;
				}

				store(data);
				handle();
			}

			const receiverrhandler = function (msg) {
				log("Ошибка при получении ответа от устройства: "+(msg.error || msg));
			}

			const adder = function (listeners, addlistener, id, caller, errmsg) {
				return function (conn, listener) {
					log("adding listener");
					var virgin = true;
					for (c in listeners) {
						virgin = false;
						break;
					}
					if (virgin)
						addlistener(function (info) {
							if (listeners[id(info)])
								caller(info, listeners[id(info)]);
							else
								log(errmsg+id(info));
						});

					listeners[conn] = listener;

					return listener;
				}
			};


			return function (c, check, onreceive) {
				if (! connected) 
					return;
				if (! conn) {
					conn = c;
					n_listener_add(conn, receivehandler, adder);
					n_err_add(conn, receiverrhandler, adder);
					log("registered listeners");
				}
				if (c != conn) {
					log("registering handler for incorrect connection, "+c+" (should be "+conn+")");
					return;
				}
				handlers.push([check, onreceive]);
				log("handlers registered: "+handlers.length);
			}
		})();

		const logconn = function(ci) {
			log("-----------------------");
			log("connection id: "+ci.connectionId);
			log("bufferSize: "+ci.bufferSize);
			log("receiveTimeout: "+ci.receiveTimeout);
			log("sendTimeout: "+ci.sendTimeout);
			log("boadrate: "+ci.bitrate);
			log("-----------------------");
		}

		log("connecting to port "+config.id);
		var concanceled = false;
		const cancel = function(oncomplete) {
			concanceled = function () {
				kkm.request.break.delete(cancel);
				oncomplete();
			};
		};
		kkm.request.break.add(cancel);
		n_connect(config, function (c, logconn) {
			if (concanceled) {
				log("connection halted manualy");
				if (concanceled instanceof Function)
					concanceled();
				return;
			} else
				kkm.request.break.delete(cancel);

			connected = !!c;

			const close = function(next) {
				if (wd) clearTimeout(wd);
				if (!connected) {
					log("connection already closed");
					return;
				}

				flush(c, function () {
					n_close(c, function () {
						kkm.request.break.delete(close);
						connected = false;

						log("-----------------------");
						log("disconnected");
						log("-----------------------");
						if (next && next instanceof Function)
							next();
					});
				});
			}

			kkm.request.break.add(close);

			const state = {stagecritical:true};
			var wd = setTimeout(function () { // watch dog for connection timeout
					close(function () {
						if (state.stagecritical) err("Устройство перестало отвечать", false);
						else { 
							log("timed out on non-critical stage");
							oncomplete();
						}
					}); 
			}, config.timeout);

			try {
				client(c, send1, receive1, close, flush, state);
			} catch (e) {
				err("Ошибка при обмене данными с устройством: "+e.message);
			}
		}, function () {
			log("connection to "+config.id+" failed, cleanup");
			if (concanceled && concanceled instanceof Function)
				concanceled();
			else
				kkm.request.break.delete(cancel);

			log("kkm.request.break.size "+kkm.request.break.size);
		});
	});
};

kkm.request.break = new Set();
