kkm.find.all = function (logging, findlog, found, notfound, complete) {
	var somefound = false;
	function atolfound(config, res, datas) { // разбираем данные найденного фр АТОЛ
		somefound = true;
		kkm.util.incl(logging());
		log("Устройство АТОЛ обнаружено на COM-порту "+config.id);
		found([
			config.id,
			res.model + " (АТОЛ)"+res.ver,
			"atol",
			false // fz54
		]);
	}

	function atol54fzfound(config, res, datas) { // разбираем данные найденного фр АТОЛ
		somefound = true;
		kkm.util.incl(logging());
		log("Устройство АТОЛ (54-ФЗ) обнаружено на COM-порту "+config.id);
		found([
			config.id,
			res.model + " (АТОЛ 54-ФЗ)"+res.ver,
			"atol54fz",
			true // fz54
		]);
	}

	function shtrikhfound(config, res, datas) { // нашли Штрих, разбираем его данные
		somefound = true;
		kkm.util.incl(logging());
		log("Устройство Штрих-М обнаружено на COM-порту "+config.id);
		found([
			config.id,
			res.model + " (Штрих-М"+(res.fz54? " 54-ФЗ": "")+")"+res.ver,
			res.fz54 ? "shtrikh54fz": "shtrikh",
			res.fz54
		]);
	}

	function piritfound(config, res, datas) {
		kkm.util.incl(logging());
		log("Устройство Pirit обнаружено на COM-порту "+config.id);
		found([
			config.id,
			res.model + " (Пирит"+(res.fz54 ? " 54-ФЗ)" : ")")+res.ver,
			res.fz54 ? "pirit54fz" : "pirit",
			res.fz54
		]);
	}

	function serialenum (logging, config, count, proc) {
		function c(num) {
			findlog(0 == num ? "Не найдено ни одного COM-порта ": "Найдено COM-портов: "+num);
			count(num);
		}

		kkm.serial.enum(logging, config, c, proc);
	}

	function bluetoothenum(logging, config, count, proc) {
		function c (num) {
			findlog("Обнаружено Bluetooth устройств: "+num);
			count(num);
		}

		kkm.bluetooth.enum(logging, config, c, proc);
	}

	function reqfail(port) {
		findlog("Порт "+port+" недоступен");
	}

	kkm.util.incl(logging());
	kkm.find.find(logging, kkm.config.default, function (search) {
		search(serialenum, kkm.serial.request, function (conf, apply, nothing) { // ищем устройства на ком-портах
			findlog("Ищем устройства АТОЛ (54-ФЗ) на COM-порту "+conf.id);
			apply(conf.atol(), kkm.atol54fz.command, kkm.atol54fz.ext, atol54fzfound, // Ищем АТОЛ (54-ФЗ)
				function () { // АТОЛ (54-ФЗ) не нашли
					findlog("Ищем устройства Штрих-М на COM-порту "+conf.id);
					apply(conf.shtrikh(), kkm.shtrikh.command, kkm.shtrikh.ext, shtrikhfound, // ищем Штрихи
						function () { // Штрихи не нашли
							findlog("Ищем устройства АТОЛ на COM-порту "+conf.id);
							apply(conf.atol(), kkm.atol.command, kkm.atol.ext, atolfound, // Ищем АТОЛ
								function () { // АТОЛ тоже не нашли
					                findlog("Не удалось получить ответ от COM-порта "+conf.id);
					                nothing(); notfound(conf.id);
								}
							);
						}
					);
				}
			);
		}, reqfail);
	}, function () {
		if (somefound) return complete([]);
		setTimeout(function() {
			kkm.find.find(logging, kkm.config.default, function (search) {
				search(serialenum, kkm.serial.request, function (conf, apply, nothing) { // ищем устройства на ком-портах
					findlog("Ищем устройства Pirit на COM-порту "+conf.id); // ищем Пириты
					apply(conf.pirit(), kkm.pirit.command, kkm.pirit.ext, piritfound,
		        function () { // Пирит не нашли
		            findlog("Не удалось получить ответ от COM-порта "+conf.id);
		            nothing(); notfound(conf.id);
		        }
			    );
				}, reqfail);
			}, complete);
		}, 10000);
	});
}

/**
 * поиск ФР указанного вендора на всех портах
 * если указан АТОЛ ищем сперва ФЗшный АТОЛ (протокол 3й версии), затем, если не нашли ищем не-ФЗшный (протокол 2й версии)
**/
kkm.find.forvendor = function(vendor, logging, findlog, found, notfound, complete) {
	const cd = kkm.config.default();

	function foundmaker(vendor) {
		return function(config, res) {
			switch (vendor) {
				case "atol": found([
					config.id,
					res.model + " (АТОЛ)"+res.ver,
					"atol",
					false // fz54
				]); break;
				case "atol54fz": found([
					config.id,
					res.model + " (АТОЛ 54-ФЗ)"+res.ver,
					"atol54fz",
					true // fz54
				]); break;
				case "shtrikh": found([
					config.id,
					res.model + " (Штрих-М"+(res.fz54? " 54-ФЗ": "")+")"+res.ver,
					res.fz54 ? "shtrikh54fz": "shtrikh",
					res.fz54
				]); break;
				case "pirit": found([
					config.id,
					res.model + " (Пирит"+(res.fz54 ? " 54-ФЗ)" : ")")+res.ver,
					res.fz54 ? "pirit54fz" : "pirit",
					res.fz54
				]); break;
				default: notfound(config.id);
			}
		}
	}

    function searchpromise () {
        if ("pirit" == vendor) return kkm.find.vendoric(cd.pirit, logging, findlog, kkm.serial, kkm.pirit, foundmaker("pirit"), notfound);
        if ("shtrikh" == vendor) return kkm.find.vendoric(cd.shtrikh, logging, findlog, kkm.serial, kkm.shtrikh, foundmaker("shtrikh"), notfound);
        if ("atol" == vendor) {
            const atol54fzres = kkm.find.vendoric(cd.atol54fz, logging, findlog, kkm.serial, kkm.atol54fz, foundmaker("atol54fz"), function() {/* proceeding to search non-fz */});
            return atol54fzres.then(function (ress) {
                return ress.found ? 
                	Promise.resolve(ress) : 
                	kkm.find.vendoric(cd.atol, logging, findlog, kkm.serial, kkm.atol, foundmaker("atol"), notfound);
            });
        }
        else return Promise.reject(new Error("Выбранный вендор не поддерживается"));
    }
	function searchtimeout() {
		return new Promise(function (r) {
			setTimeout(function() {
				findlog("Ничего не найдено за отведенное на поиск время");
				r();
			}, cd.cmdtimeout*2);
		});
	}

    Promise.race([searchpromise(), searchtimeout()]).then(complete);
}