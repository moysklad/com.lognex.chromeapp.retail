kkm.atol.print = {};

kkm.atol.print.linelengths = {
	13:40, 14:20, 15:20, 16:24,
	20:48, 23:39, 24:38, 27:38,
	30:56, 31:32, 32:56, 35:36,
	41:56, 45:56, 46:72, 47:25,
	51:32, 52:32, 53:57
};


// создание функции печати строки
kkm.atol.print.printlinemaker = function(stalk) {
	return function(p, l) {
		return p.then(function () {
			return stalk([0x4c].concat(l), "Ошибка при печати строки ("+l+")");
		});
	}
}

// перефодим ФР в режим выбора, если еще не в нем
kkm.atol.print.modesuper = function (data, log, stalk) {
	const mode = data[17];
	if (1 < mode) {
		log("переходим в режим выбора");
		return stalk([0x48], "Ошибка выхода в режим выбора");
	}
	log("уже в режиме выбора или регистрации");
	return Promise.resolve()
};

kkm.atol.print.modemaker = function(tomode, modename) {
	return function(log, err, talk, stalk, config) {

		return talk([0x3f]). // выясняем, в каком мы режиме
		then(function(data) {
			const r = data[22];// номер чека

			if (!r) return Promise.resolve(data);
			return stalk([0x59], "Ошибка аннулирования чека").
			then(function() {
				return talk([0x3f]);
			});
		}).
		then(function(data) { // переходим в режим выбора, если нужно
			const m = data[17];
			if (0 == m || tomode == m) return Promise.resolve(data);

			return stalk([0x48], "Ошибка выхода в режим выбора").
			then(function() {
				return talk([0x3f]);
			});
		}).
		then(function(data) { // переходим в указанный режим, если нужно
			const m = data[17];
			if (tomode == m) return Promise.resolve(data);

			if (0 != m && tomode != m) err("Не удалось выйти в режим выбора");

			return talk([0x56, tomode].concat(config.adminpass), "Ошибка перехода в режим "+tomode).
			then(function() { 
				return talk([0x3f]); 
			});
		}).
		then(function(data) {
			const m = data[17];
			if (tomode != m) err("Не удалось перейти в режим "+modename);

			return Promise.resolve();
		});
	}
}

// переводим в режим регистрации, если уже не в нем
kkm.atol.print.moderegister = function(data, log, talk, stalk, config) {
	const mode = data[17];
	if (1 == mode) {
		log("уже в режиме регистрации");
		return Promise.resolve();
	}

	log("переходим в режим регистрации");
	return stalk([0x56, 0x01].concat(config.operpass), "Ошибка при входе в режим регистрации").
	then(function() {
		return talk([0x3f]);
	}).
	then(function (data) {
		if (1 != data[17])
			throw "Устройство не может войти в режим регистрации";
		else
			return Promise.resolve();
	});
}

// открываем смену на ФР
kkm.atol.print.openchange = function (data, log, stalk, cashier) {
	const change = data[9];
	if (0 == (change & 2)) {
		log("открываем смену");
		return ((!!cashier) ?
				kkm.atol.print.setreqmaker(stalk, log)(1021, cashier) :
				Promise.resolve()).
		then(function () { return stalk([0x9a, 0x00], "Ошибка открытия смены"); });
	} else {
		log("смена уже открыта");
		return Promise.resolve();
	}
}

// аннулируем на закрытый чек на ФР
kkm.atol.print.cancelreceipt = function (data, log, stalk) {
	const receiptnum = data[22];
	if (0 == receiptnum) {
		log("предыдущий чек закрыт");
		return Promise.resolve();
	} else {
		log("аннулируем не закрытый чек");
		return stalk([0x59], "Ошибка аннулирования чека");
	}
}

// открываем чек на ФР
kkm.atol.print.openreceipt = function (receipt, log, stalk, receipttype) {
	log("открываем чек"+(receipt.return ? " возврата":""));
	return stalk([0x92, 0x00, receipttype(receipt)], "Ошибка открытия чека");
}

// тип чека, продажа или возврат
kkm.atol.print.receipttype = function (receipt) { // чек продажи (1) или чек возврата (2)
	return receipt.return ? 0x02 : 0x01;
}

kkm.atol.print.corrreceipttype = function (corrreceipt) {
	return corrreceipt.op == 1 ? 0x07 : 0x09;
}

kkm.atol.print.parseinfo = function (data) {
	const res = {};
	res.cashbox = fromBCD([0x00, 0x00, data[2], data[3]]);
	res.receipt = fromBCD([0x00, 0x00, data[18], data[19]]);
	res.change = fromBCD([0x00, 0x00, data[20], data[21]]);

	return res;
}

kkm.atol.print.printlinemaker = function(stalk) {
	return function (p, l) {
		return p.then(function () {
			return stalk([0x4c].concat(l), "Ошибка при печати строки ("+l+")");
		});
	}
}

kkm.atol.print.pos = function (receipt, rl, log, stalk, printline, infodata, linelengths) {
	function sell(a, q) {
		return [0x52, 0x00].concat(a).concat(q).concat([0x01]);
	}

	function ret(a, q) {
		return [0x57, 0x00].concat(a).concat(q);
	}

	log("печать строки "+JSON.stringify(rl));

	const model = infodata[14];
	const amount = int2BCDbytes(rl.price, [0, 0, 0, 0, 0]);
	const quantity = int2BCDbytes(Math.round(rl.quantity*1000), [0, 0, 0, 0, 0]);
	const name = splitbytes(cp866encode(rl.name), (linelengths[model] || 20));

	log("данные подготовлены, amount "+amount.map(n=>(n).toString(16))+", quantity: "+quantity.map(n=>(n).toString(16))+", name: "+name.map(n=>(n).toString(16)));

	var s = (receipt.return ? ret: sell)(amount, quantity);

	return name.reduce(printline, Promise.resolve()).
	then(function () { return stalk(s, "Ошибка при регистрации позиции чека"); });
}

kkm.atol.print.vat = function(rl) {
	function byffd(v1, v2) {
		if ((1 < kkm.atol54fz.ffd) || kkm.atol54fz.ffd105vat) return v2;
		else return v1;
	}

	if (undefined == rl.vat) return byffd(0x04, 0x06);
	switch(rl.vat) {
		case 0: return byffd(0x01, 0x05);
		case 10: return 0x02;
		case 18: return byffd(0x03, 0x01);
		case 110: return byffd(0x05, 0x04);
		case 118: return byffd(0x06, 0x03);
		default: throw ("Неизвестная ставка НДС "+rl.vat);
	}
}

kkm.atol.print.paytype = function(ptname) {
	switch (ptname){
		case "cash":
			return 0x01; break;
		case "cashless":
			return (1 < kkm.atol54fz.ffd) ? 0x02 : 0x04; break;
		default: 0x01;
	}
}

kkm.atol.print.corrpos = function(sum, log, stalk) {
	if (!sum) {
		log("нет суммы для печати");
		return Promise.resolve();
	}
  log("печать строки чека коррекции (54-ФЗ)");
	const price = int2BCDbytes(sum, [0, 0, 0, 0, 0, 0]);
	const quantity = int2BCDbytes(Math.round(1*1000), [0, 0, 0, 0, 0]);
	const p = [0xE6]. // регистрация позиции продажи
	concat(0x00). // флаги
	concat(zeroarr(64)). // название товара
	concat(price). // цена
	concat(quantity). // количество
	concat(0x01). // тип скидки (сумма)
	concat(0x01). // скидка или надбавка
	concat(zeroarr(6)). // размер скидки
	concat(0x04). // тип НДС по таблице 13
	concat(0x00). // секция
	concat(fillarr(0x00, 16)). // штрихкод, пустой
	concat(0x00); // зарезервировано
	return stalk(p, "Ошибка при печати позиции чека коррекции (ФЗ-54)");
}

kkm.atol.print.pos54fz = function(receipt, rl, log, stalk, printline, infodata, linelengths) {
	function sumdiscount(rl) {
		if (typeof rl.discountAbs != "undefined") {
			return Math.round(rl.discountAbs);
		} else {
			return Math.round(((rl.price * rl.quantity) * rl.discount) / 100);
		}
	}
	const tail = kkm.util.tail;

	log("печать строки (54-ФЗ) "+JSON.stringify(rl));

	const namebytes = cp866encode(rl.name);
	const price = int2BCDbytes(rl.price, [0, 0, 0, 0, 0, 0]);
	const quantity = int2BCDbytes(Math.round(rl.quantity*1000), [0, 0, 0, 0, 0]);
	const d = sumdiscount(rl);
	const discount = int2BCDbytes(d, [0, 0, 0, 0, 0, 0]);
	const discountdirection = d > 0 ? 0x00: 0x01;

	const p = [0xE6]. // регистрация позиции продажи
	concat(0x00). // флаги
	concat(cutbytes(namebytes, 64)). // название товара
	concat(price). // цена
	concat(quantity). // количество
	concat(0x01). // тип скидки (сумма)
	concat(discountdirection). // скидка или надбавка
	concat(discount). // размер скидки
	concat(kkm.atol.print.vat(rl)). // тип НДС по таблице 13
	concat(0x00). // секция
	concat(fillarr(0x00, 16)). // штрихкод, пустой
	concat(0x00); // зарезервировано

	return stalk(p, "Ошибка при печати позиции чека (ФЗ-54)");
}

kkm.atol.print.print4corr = function (config, corrreceipt, onfinish, app, log, err, assert, talkmaker, safetalkmaker) {
	app(
		function (send, receive, close) {
			const U = kkm.util;
			const talk = talkmaker(send, receive, log);
			const stalk = safetalkmaker(send, receive, close, log, assert);

            const assert0 = U.assertmaker(config.errtable, function(msg) { err(msg, 001); });
            const stalk0 = safetalkmaker(send, receive, close, log, assert0);

			const P = kkm.atol.print;
			const setreq = P.setreqmaker(stalk, log);

			return talk([0x3f]). // получаем состояние фискальника
			then(function(infodata) { // если не в режимах выбора или регистрации - переходим в выбор
				return P.modesuper(infodata, log, stalk).then(U.pass(infodata));
			}).
			then(function(infodata) { // переходим в режим регистрации если еще не в нем
				return P.moderegister(infodata, log, talk, stalk, config).then(U.pass(infodata));
			}).
			then(function(infodata) { // если смена закрыта - открываем
				return P.openchange(infodata, log, stalk0, corrreceipt.cashier).then(U.pass(infodata));
			}).
			then(function(infodata) { // если не закрыт предыдущий чек - аннулируем его
				return P.cancelreceipt(infodata, log, stalk).then(U.pass(infodata));
			}).
			then(function(infodata) { // открываем чек
				log("Открываем чек коррекции");
				//return P.openreceipt(receipt, log, stalk, corrreceipt.op).then(U.pass(infodata));
				return stalk([0x92, 0x00, P.corrreceipttype(corrreceipt)], "Ошибка открытия чека коррекции");
			}).
			then(function(infodata) { // устанавливаем имя кассира
				return (!!corrreceipt.cashier) ?
					setreq(1021, corrreceipt.cashier).then(U.pass(infodata)) :
					Promise.resolve(infodata);
			}).
			then(function(infodata) {
				return P.corrpos(corrreceipt.cashsum+corrreceipt.nocashsum,log,stalk);
			}).
			then(function() { // печатаем сумму оплаты налом, если она есть
				return P.printsum(corrreceipt.cashsum, P.paytype("cash"), log, stalk);
			}).
			then(function() { // печатаем сумму оплаты безналом, если она есть
				return P.printsum(corrreceipt.nocashsum, P.paytype("cashless"), log, stalk);
			}).
			then(function(){log("Закрываем чек коррекции");return stalk([0x4a, 0x00].concat(P.corrreceipttype(corrreceipt)).concat([0, 0, 0, 0, 0]), "Ошибка закрытия чека");}). // закрываем чек
			then(function(){log("Проверяем состояния ус-ва");return talk([0x3f]);}).
			then(function(data) {
				if (1 == data[22]) {
					return stalk([0x59], "Ошибка аннулирования чека").then(function(){return new Promise(function(res) { close(res); });});
				}
				else {
					return new Promise(function(res) { close(res); });
				}
			}).
			then(onfinish). // отдаем управление наружу*/
			catch(function(e) { close(function() { err(e.message || e); }); });
		}
	);
}

kkm.atol.print.print4pos = function(config, receipt, pos, onfinish, app, log, err, assert, talkmaker, safetalkmaker) {
	app(
		function (send, receive, close) {
			const U = kkm.util;
			const talk = talkmaker(send, receive, log);
			const stalk = safetalkmaker(send, receive, close, log, assert);

			const assert0 = U.assertmaker(config.errtable, function(msg) { err(msg, 001); });
			const stalk0 = safetalkmaker(send, receive, close, log, assert0);

			const P = kkm.atol.print;
			const printline = P.printlinemaker(stalk);
			const egaisprint = P.egaisprintmaker(log, stalk, kkm.egais, P.linelengths, printline);
			const setreq = P.setreqmaker(stalk, log);

			return talk([0x3f]). // получаем состояние фискальника
			then(function(infodata) { // если не в режимах выбора или регистрации - переходим в выбор
				return P.modesuper(infodata, log, stalk).then(U.pass(infodata));
			}).
			then(function(infodata) { // переходим в режим регистрации если еще не в нем
				return P.moderegister(infodata, log, talk, stalk, config).then(U.pass(infodata));
			}).
			then(function(infodata) { // если смена закрыта - открываем
				return P.openchange(infodata, log, stalk0, receipt.cashier).then(U.pass(infodata));
			}).
			then(function(infodata) { // если не закрыт предыдущий чек - аннулируем его
				return P.cancelreceipt(infodata, log, stalk).then(U.pass(infodata));
			}).
			then(function(infodata) { // открываем чек
				return P.openreceipt(receipt, log, stalk, P.receipttype).then(U.pass(infodata));
			}).
			then(function(infodata) { // устанавливаем имя кассира
				return (!!receipt.cashier) ?
					setreq(1021, receipt.cashier).then(U.pass(infodata)) :
					Promise.resolve(infodata);
			}).
			then(function(infodata) { // устанавливаем почту/телефон получателя электронного чека
				return (!!receipt.counterparty) ?
					setreq(1008, receipt.counterparty).then(U.pass(infodata)) :
					Promise.resolve(infodata);
			}).
			then(function(infodata) { // печатаем шапку онлайновой фискальнизации, если таковая была
				const model = infodata[17];
				const ofdblock = U.ofd(
					cp866encode,
					fillarr(0x2d, (P.linelengths[model] || 20)),
					[0x2a, 0x2a, 0x2a, 0x20, 0x94, 0x8f, 0x20, 0x2a, 0x2a, 0x2a],
					[0x24, 0x20]
				);

				const block = ofdblock(receipt.ofdCode, receipt.ofdKKT, receipt.ofdTime, false);
				return block.reduce(printline, Promise.resolve()).then(U.pass(infodata));
			}).
			then(function(infodata) { // печатаем строки чека
				function sellchain(prev, line) {
					return prev.then(function() { return pos(receipt, line, log, stalk, printline, infodata, P.linelengths); });
				}

				return receipt.positions.reduce(sellchain, Promise.resolve());
			}).
			then(function() { // регистрируем скидку на чек, если она есть
				return P.regdiscount(receipt, log, stalk);
			}).
			then(function() { // печатаем сумму оплаты налом, если она есть
				return P.printsum(receipt.paidSum, P.paytype("cash"), log, stalk);
			}).
			then(function() { // печатаем сумму оплаты безналом, если она есть
				return P.printsum(receipt.noCashSum, P.paytype("cashless"), log, stalk);
			}).
			then(P.closereceiptmaker(close, talk, stalk, log, receipt, egaisprint, P.pempty)). // закрываем чек
			then(onfinish). // отдаем управление наружу*/
			catch(function(e) { close(function() { err(e.message || e); }); });
		}
	);
}

kkm.atol.print.egaisprintmaker = function (log, stalk, E, linelengths, printline) {
	const P = kkm.atol.print;

	return function (er, infodata) {
		function qr() {
			if (!kkm.atol54fz.qr_supported) {
				log("fr is not able to print qr code");
				return Promise.resolve();
			}

			log("printing qr code, text "+er.url);
			return stalk(
						[0xC1, // <C1h> печать QR кода
						0,     // <Тип штрихкода(1)>, qr
						2,     // <Выравнивание (1)>, по центру
						8,     // <Ширина (1)>
						0, 0,  // <Версия(2)>, автоматический подбор
						0, 1,  // <Опции (2)>
						0,     // <Уровень коррекции(1)>, по настройке Т2Р1П93
						0,     // <Количество строк(1)> (не исп.)
						0,     // <Количество столбцов(1)> (не исп.)
						0, 0,  // <Пропорции штрихкода(2)> (не исп.)
						0, 0]. // <Пропорции пикселя(2)> (не исп.)
						concat(cutbytes(win1251encode(er.url), 100)), // <Строка данных(100)>
						"Ошибка печати QR-кода"
				);
		}

		return function() {
			const model = infodata[14];
			const head = E.head(cp866encode, er, E.parseinfo(infodata).cashbox, linelengths[model] || 20);
			const tail = E.tail(cp866encode, er, linelengths[model] || 20);

			log("printing egais receipt head, "+head.length+" lines");

			return head.reduce(printline, Promise.resolve()).
			then(qr).
			then(function () {
				return tail.reduce(printline, Promise.resolve());
			});
		}
	}
}

kkm.atol.print.printsum = function(sum, paytype, log, stalk) {
	if (!sum) {
		log("суммы по типу оплаты "+paytype+" нет");
		return Promise.resolve();
	}
	log("печать суммы по типу оплаты "+paytype);
	const sumbytes = int2BCDbytes(sum|0, [0, 0, 0, 0, 0]);
	return stalk([0x99, 0x00].concat(paytype).concat(sumbytes), "Ошибка при печати суммы");
}

kkm.atol.print.regdiscount = function(receipt, log, stalk) {
	if (!receipt.discount) {
		log("скидки нет");
		return Promise.resolve();
	}
	log("регистрируем скидку")
	const discount = int2BCDbytes((receipt.discount)|0, [0, 0, 0, 0, 0]);
	return stalk([0x43, 0x00, 0x00, 0x01, 0x00].concat(discount), "Ошибка регистрации скидки");
}

kkm.atol.print.closereceiptmaker = function (close, talk, stalk, log, receipt, egaisprint, pempty) {
	return function () {
		function closepromise() {
			return new Promise(function(res, rej) { close(res); });
		}

		function paytype() {
			return ("cash" == receipt.type ? [0x01] : [0x04]);
		}

		log("закрываем чек");

		const U = kkm.util;
		return stalk([0x4a, 0x00].concat(paytype()).concat([0, 0, 0, 0, 0]), "Ошибка закрытия чека"). // закрываем чек
		then(function() { return talk([0x3f]); }). // снова проверяем состояние, чек должен быть закрыт
		then(function(data) {
			if (1 == data[22]) {
				return stalk([0x59], "Ошибка аннулирования чека").then(closepromise);
			} else {
				const egais1 = receipt.egais1 ? egaisprint(receipt.egais1, data) : U.pempty;
				const egais2 = receipt.egais2 ? egaisprint(receipt.egais2, data) : U.pempty;

				return egais1().then(egais2).then(closepromise);
			}
		})
	}
}

kkm.atol.print.setreqmaker = function(stalk, log) {
	function bytes(i) {
		const a = int2bytes(i, [0,0,0,0]);
		return [a[3], a[2]];
	}

	return function(req, text) {
		log("setting requisite "+req+" ("+text+")");
		const textbytes = cp866encode(text);
		const reqbytes = bytes(req);
		const lenbytes = bytes(textbytes.length);
		return stalk([
			0xe8, // команда установки значения реквизита в ФН
			0x01, // флаги (выводить на печать)
			0x01, // количество блоков под данные
			0x00  // номер блока в памяти ФН
			].concat(reqbytes). // номер реквизита
			concat(lenbytes). // длина значения реквизита
			concat(textbytes),  // значение реквизита
			"Ошибка при установке значения реквизита "+req);
	}
}

kkm.atol.print.setfieldmaker = function (log, err, talk, stalk, config) {
	const AP = kkm.atol.print;
	const modeprog = AP.modemaker(4, "программирования");

	return function(table, row, field, bytes) {
		const rb = int2BCDbytes(row, [0,0,0,0]); // [0, 0, row high byte, row low byte]
		const progbytes = [0x50, table, rb[2], rb[3], field].concat(bytes);

		return modeprog(log, err, talk, stalk, config).
		then(function() {
			return talk(progbytes);
		});
	}
}
