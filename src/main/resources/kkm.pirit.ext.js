kkm.pirit.ext = function (logging, config, executor, proc) {
	kkm.pirit.api(logging, config, executor, function (beep, demo, info, type, check, print, z, x, change, cashin, cashout) {
	    const U = kkm.util;
	    const L = U.modlog(logging(), "...");

	    const log = L.log;
	    const err = L.err;

	    const X = kkm.ext;

		proc(beep, demo, info, type, check, print, z, x, change, cashin, cashout, /*ofdreport*/U.cbpass, /*corrprint*/U.cbpass, /*hackfr*/U.cbpass);
	});
}