/**
*
* поиск подключенных регистраторов
*
* logging - функция, формирующая набор функций логирования, log - сообщение в лог, err - сообщение об ошибке
*
* proc - функция вида proc(search), в которой делегат search должен быть
* вызван для всех типов подключений, по которым может искаться регистратор
*
* complete - функция вида complete(foundsomething), вызываемая по завершении
* поиска, foundsomething boolean, true если что-то нашлось
*
*
**/
kkm.find.find = function (logging, config, proc, complete) {
	kkm.util.incl(logging());

	var types = 0;
	var ids = [];
 	var searches = 0;
 	const wd = setTimeout(function () { complete(ids); }, config().timeout); // на случай если ни одного подключения не найдено

 	function done() {
 		if (0 == --searches) {
 			clearTimeout(wd);
 			complete(ids);
 		}
 	}

	/**
	*
	* поиск устройств, подключенных определенным способом, через
	* COM-порт, USB или Bluetooth
	*
	* enum - перечислитель аппаратных подключений определенного типа,
	* функция enum(found), где found вызывается при нахождении
	* подключения как found(config)
	*
	* request - функция реализующая низкоуровневый запрос к устройствам
	* перечисляемого enum типа, вида request(logging, client, config, confail)
	*
	* protocols - функция вида protocols(conf, apply, nothing), где conf -
	* конфигурация текущего подключения, apply функция вида
	* apply(command, api, found, notfound) ко всем протоколам, реализованным
	* в command, через которую вызывается проверка регистратора из api,
	* данные по регистратору передаются в метод
	* found(config, typedata, infodata) или  по окончании
	* (не успешном) вызывается notfound
	* а nothing - функция без параметров, вызываемая когда на текущем подключении
	* ничего не найдено
	*
	**/
	function search(enm, req, protocols, reqfail) {
		types++;
		enm(logging, config, function (n) { searches+=n; types--; if (0 == types && 0 == searches) complete(ids); }, function (confchange) {
			const conf = confchange(config);
			ids.push(conf.id);
			protocols(conf, function (conf, command, api, found, notfound) {
				var timer;
				function exec(logging, chain) {
					req(logging, conf, function (conn, send1, receive1, close, flush) {
						timer = setTimeout(function () {
							log("device not found on port "+conf.id);
							timer = false;
							close(notfound);
						}, conf.cmdtimeout);
						command(conf, chain, logging, conn, send1, receive1, close, flush);
					}, function () { done(); reqfail(conf.id); });
				}

				api(logging, conf, exec, function (beep, demo, info, type, check, print, z) {
					var typedata;
					check({}, function (res, datas) {
							if (timer)
								clearTimeout(timer);
							found(conf, res, datas);
							done();
					});
				});
			}, done);
		});
	}

	proc(search);
};

/**
 * поиск подключения ФР определенного производителя
 * config - настройки для данного производителя
 * logging - логирование событий поиска (no images here!)
 * connector - подключение, kkm.serial например, используются connector.enum - для перечисления списка возможных подключений, и connector.request - для подключения
 * vendor - вендорный код, kkm.atol или kkm.shtrikh, например, используется vendor.command как реализация низкоуровневого протокола, и vendor.ext делегат check для проверки подключения
 * found - обработчик события нахождения ФР на определенном подклчении
 * notfound - обработчик таймаута поиска ФР на подключении
**/
kkm.find.vendoric = function (config, logging, findlog, connector, vendor, found, notfound) {
    const L = logging();
    const log = L.log;

    return new Promise(function (res) {
    	var conns;
	    const reportcount = function(count) {
	    	conns = count;
	        log(0 == count ? "Не найдено подключений": "Найдено подключений: "+count);
	    };

    	const searches = [];
	    connector.enum(logging, config, reportcount, function (confchange) {
	    	const conconfig = confchange(config);

	    	var timeouts = {};

	        const executormaker = function(presolve) {
	        	return function(logging, check) {
		            connector.request(logging, conconfig, function(conn, send1, receive1, close, flush) {
	                    timeouts[conconfig.id] = setTimeout(function () {
	                    	close(function() {
		                        notfound(conconfig.id);
		                        findlog("На подключении "+conconfig.id+" ФР не найден");
		                        presolve({"found":false, "id":conconfig.id}); // не скидываем reject потому что он завершает Promise.all
	                    	});
	                    }, conconfig.cmdtimeout);
		                vendor.command(conconfig, check, logging, conn, send1, receive1, close, flush);
		            }, function() { log(conconfig.id+" not available"); });
		        };
		    };

		    const checkconn = function(conconfig) {
		        try {
		            return new Promise(function(res) {
		                vendor.ext(logging, conconfig, executormaker(res), function(beep, demo, info, type, check) {
		                    findlog("Ищем ФР на подключении "+conconfig.id);
		                    check({}, function (fr) {
		                        clearTimeout(timeouts[conconfig.id]);
		                        found(conconfig, fr);
		                        findlog("На подключении "+conconfig.id+" обнаружен ФР "+fr.model)
		                        res({"found":true, "fr":fr});
		                    });
		                });
		            });
		        } catch (e) {
		            log("error checking port "+conconfig.id+" "+(e.message ? e.message : e));
		            findlog("Ошибка при проверке подключения "+conconfig.id);
		            return Promise.resolve({"found":false, "id":conconfig.id})
		        }
		    }

	        searches.push(checkconn(conconfig));
	        if (searches.length == conns) res(searches);
	    });
    }).then(function(searches) {
    	return Promise.race(searches);
    });
}